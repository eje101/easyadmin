/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : easyadmin

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 28/09/2022 17:52:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ea_mall_cate
-- ----------------------------
DROP TABLE IF EXISTS `ea_mall_cate`;
CREATE TABLE `ea_mall_cate`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名',
  `image` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类图片',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_mall_cate
-- ----------------------------
INSERT INTO `ea_mall_cate` VALUES (9, '手机', 'http://admin.host/upload/20200514/98fc09b0c4ad4d793a6f04bef79a0edc.jpg', 0, 1, '', 1589440437, 1589440437, NULL);

-- ----------------------------
-- Table structure for ea_mall_goods
-- ----------------------------
DROP TABLE IF EXISTS `ea_mall_goods`;
CREATE TABLE `ea_mall_goods`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `logo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品logo',
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品图片 以 | 做分割符号',
  `describe` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品描述',
  `market_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '市场价',
  `discount_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '折扣价',
  `sales` int(11) NULL DEFAULT 0 COMMENT '销量',
  `virtual_sales` int(11) NULL DEFAULT 0 COMMENT '虚拟销量',
  `stock` int(11) NULL DEFAULT 0 COMMENT '库存',
  `total_stock` int(11) NULL DEFAULT 0 COMMENT '总库存',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cate_id`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品列表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_mall_goods
-- ----------------------------
INSERT INTO `ea_mall_goods` VALUES (8, 10, '落地-风扇', 'http://admin.host/upload/20200514/a0f7fe9637abd219f7e93ceb2820df9b.jpg', 'http://admin.host/upload/20200514/95496713918290f6315ea3f87efa6bf2.jpg|http://admin.host/upload/20200514/ae29fa9cba4fc02defb7daed41cb2b13.jpg|http://admin.host/upload/20200514/f0a104d88ec7dc6fb42d2f87cbc71b76.jpg|http://admin.host/upload/20200514/3b88be4b1934690e5c1bd6b54b9ab5c8.jpg', '<p>76654757</p>\n\n<p><img alt=\"\" src=\"http://admin.host/upload/20200515/198070421110fa01f2c2ac2f52481647.jpg\" style=\"height:689px; width:790px\" /></p>\n\n<p><img alt=\"\" src=\"http://admin.host/upload/20200515/a07a742c15a78781e79f8a3317006c1d.jpg\" style=\"height:877px; width:790px\" /></p>\n', 599.00, 368.00, 0, 594, 0, 0, 675, 1, '', 1589454309, 1589567016, NULL);
INSERT INTO `ea_mall_goods` VALUES (9, 9, '电脑', 'http://admin.host/upload/20200514/bbf858d469dec2e12a89460110068d3d.jpg', 'http://admin.host/upload/20200514/f0a104d88ec7dc6fb42d2f87cbc71b76.jpg', '<p>477</p>\n', 0.00, 0.00, 0, 0, 115, 320, 0, 1, '', 1589465215, 1589476345, NULL);
INSERT INTO `ea_mall_goods` VALUES (10, 9, '123', '12', '12', '&lt;p&gt;123213&lt;/p&gt;\n', 0.00, 0.00, 0, 0, 0, 0, 1, 1, '123', 1664178640, 1664178640, NULL);
INSERT INTO `ea_mall_goods` VALUES (11, 9, '1', '1', '1', '&lt;p&gt;1&lt;/p&gt;\n', 1.00, 1.00, 0, 1, 0, 0, 1, 1, '1', 1664178656, 1664178656, NULL);

-- ----------------------------
-- Table structure for ea_system_admin
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_admin`;
CREATE TABLE `ea_system_admin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色权限ID',
  `head_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户登录密码',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系手机号',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用,)',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_admin
-- ----------------------------
INSERT INTO `ea_system_admin` VALUES (1, NULL, '/static/admin/images/head.jpg', 'admin', 'ed696eb5bba1f7460585cc6975e6cf9bf24903dd', 'admin', 'admin', 16, 0, 1, 1589454169, 1664355867, NULL);

-- ----------------------------
-- Table structure for ea_system_auth
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_auth`;
CREATE TABLE `ea_system_auth`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_auth
-- ----------------------------
INSERT INTO `ea_system_auth` VALUES (1, '管理员', 1, 1, '测试管理员', 1588921753, 1589614331, NULL);
INSERT INTO `ea_system_auth` VALUES (6, '游客权限', 0, 1, '', 1588227513, 1589591751, 1589591751);

-- ----------------------------
-- Table structure for ea_system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_auth_node`;
CREATE TABLE `ea_system_auth_node`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '角色ID',
  `node_id` bigint(20) NULL DEFAULT NULL COMMENT '节点ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_system_auth_auth`(`auth_id`) USING BTREE,
  INDEX `index_system_auth_node`(`node_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与节点关系表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_auth_node
-- ----------------------------
INSERT INTO `ea_system_auth_node` VALUES (1, 6, 1);
INSERT INTO `ea_system_auth_node` VALUES (2, 6, 2);
INSERT INTO `ea_system_auth_node` VALUES (3, 6, 9);
INSERT INTO `ea_system_auth_node` VALUES (4, 6, 12);
INSERT INTO `ea_system_auth_node` VALUES (5, 6, 18);
INSERT INTO `ea_system_auth_node` VALUES (6, 6, 19);
INSERT INTO `ea_system_auth_node` VALUES (7, 6, 21);
INSERT INTO `ea_system_auth_node` VALUES (8, 6, 22);
INSERT INTO `ea_system_auth_node` VALUES (9, 6, 29);
INSERT INTO `ea_system_auth_node` VALUES (10, 6, 30);
INSERT INTO `ea_system_auth_node` VALUES (11, 6, 38);
INSERT INTO `ea_system_auth_node` VALUES (12, 6, 39);
INSERT INTO `ea_system_auth_node` VALUES (13, 6, 45);
INSERT INTO `ea_system_auth_node` VALUES (14, 6, 46);
INSERT INTO `ea_system_auth_node` VALUES (15, 6, 52);
INSERT INTO `ea_system_auth_node` VALUES (16, 6, 53);

-- ----------------------------
-- Table structure for ea_system_config
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_config`;
CREATE TABLE `ea_system_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '变量值',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `sort` int(10) NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `group`(`group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_config
-- ----------------------------
INSERT INTO `ea_system_config` VALUES (41, 'alisms_access_key_id', 'sms', '填你的', '阿里大于公钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (42, 'alisms_access_key_secret', 'sms', '填你的', '阿里大鱼私钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (55, 'upload_type', 'upload', 'local', '当前上传方式 （local,alioss,qnoss,txoss）', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (56, 'upload_allow_ext', 'upload', 'doc,gif,ico,icon,jpg,mp3,mp4,p12,pem,png,rar,jpeg', '允许上传的文件类型', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (57, 'upload_allow_size', 'upload', '1024000', '允许上传的大小', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (58, 'upload_allow_mime', 'upload', 'image/gif,image/jpeg,video/x-msvideo,text/plain,image/png', '允许上传的文件mime', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (59, 'upload_allow_type', 'upload', 'local,alioss,qnoss,txcos', '可用的上传文件方式', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (60, 'alioss_access_key_id', 'upload', '填你的', '阿里云oss公钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (61, 'alioss_access_key_secret', 'upload', '填你的', '阿里云oss私钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (62, 'alioss_endpoint', 'upload', '填你的', '阿里云oss数据中心', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (63, 'alioss_bucket', 'upload', '填你的', '阿里云oss空间名称', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (64, 'alioss_domain', 'upload', '填你的', '阿里云oss访问域名', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (65, 'logo_title', 'site', 'EasyAdmin', 'LOGO标题', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (66, 'logo_image', 'site', '/favicon.ico', 'logo图片', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (68, 'site_name', 'site', 'EasyAdmin后台系统', '站点名称', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (69, 'site_ico', 'site', '填你的', '浏览器图标', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (70, 'site_copyright', 'site', '填你的', '版权信息', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (71, 'site_beian', 'site', '填你的', '备案信息', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (72, 'site_version', 'site', '2.0.0', '版本信息', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (75, 'sms_type', 'sms', 'alisms', '短信类型', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (76, 'miniapp_appid', 'wechat', '填你的', '小程序公钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (77, 'miniapp_appsecret', 'wechat', '填你的', '小程序私钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (78, 'web_appid', 'wechat', '填你的', '公众号公钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (79, 'web_appsecret', 'wechat', '填你的', '公众号私钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (80, 'txcos_secret_id', 'upload', '填你的', '腾讯云cos密钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (81, 'txcos_secret_key', 'upload', '填你的', '腾讯云cos私钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (82, 'txcos_region', 'upload', '填你的', '存储桶地域', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (83, 'tecos_bucket', 'upload', '填你的', '存储桶名称', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (84, 'qnoss_access_key', 'upload', '填你的', '访问密钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (85, 'qnoss_secret_key', 'upload', '填你的', '安全密钥', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (86, 'qnoss_bucket', 'upload', '填你的', '存储空间', 0, NULL, NULL);
INSERT INTO `ea_system_config` VALUES (87, 'qnoss_domain', 'upload', '填你的', '访问域名', 0, NULL, NULL);

-- ----------------------------
-- Table structure for ea_system_log_202209
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_log_202209`;
CREATE TABLE `ea_system_log_202209`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '管理员ID',
  `url` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作页面',
  `method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求方法',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '日志标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'User-Agent',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 822 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台操作日志表 - 202209' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_log_202209
-- ----------------------------
INSERT INTO `ea_system_log_202209` VALUES (630, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"vve6\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1663924791);
INSERT INTO `ea_system_log_202209` VALUES (631, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"kuua\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1663924802);
INSERT INTO `ea_system_log_202209` VALUES (632, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"fbdx\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1663924849);
INSERT INTO `ea_system_log_202209` VALUES (633, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"fbdx\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1663924922);
INSERT INTO `ea_system_log_202209` VALUES (634, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"nndn\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1663924936);
INSERT INTO `ea_system_log_202209` VALUES (635, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"v2ua\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1663924961);
INSERT INTO `ea_system_log_202209` VALUES (636, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"dsmk\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1663925003);
INSERT INTO `ea_system_log_202209` VALUES (637, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"iteg\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664154581);
INSERT INTO `ea_system_log_202209` VALUES (638, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664156877);
INSERT INTO `ea_system_log_202209` VALUES (639, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"aavx\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664161035);
INSERT INTO `ea_system_log_202209` VALUES (640, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664161092);
INSERT INTO `ea_system_log_202209` VALUES (641, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664161097);
INSERT INTO `ea_system_log_202209` VALUES (642, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664161119);
INSERT INTO `ea_system_log_202209` VALUES (643, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664161125);
INSERT INTO `ea_system_log_202209` VALUES (644, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"302\",\"301\",\"300\",\"299\",\"298\",\"297\",\"296\",\"291\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164099);
INSERT INTO `ea_system_log_202209` VALUES (645, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"302\",\"301\",\"300\",\"299\",\"298\",\"297\",\"296\",\"291\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164105);
INSERT INTO `ea_system_log_202209` VALUES (646, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"302\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164150);
INSERT INTO `ea_system_log_202209` VALUES (647, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"302\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164156);
INSERT INTO `ea_system_log_202209` VALUES (648, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"302\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164166);
INSERT INTO `ea_system_log_202209` VALUES (649, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"301\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164172);
INSERT INTO `ea_system_log_202209` VALUES (650, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"301\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164172);
INSERT INTO `ea_system_log_202209` VALUES (651, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"301\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664164198);
INSERT INTO `ea_system_log_202209` VALUES (652, 1, '/admin/system.uploadfile/delete?id=301', 'post', '', '{\"id\":\"301\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664169852);
INSERT INTO `ea_system_log_202209` VALUES (653, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"dmhx\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664171326);
INSERT INTO `ea_system_log_202209` VALUES (654, 1, '/admin/system.uploadfile/delete?id=301', 'post', '', '{\"id\":\"301\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664171814);
INSERT INTO `ea_system_log_202209` VALUES (655, 1, '/admin/system.uploadfile/delete?id=300', 'post', '', '{\"id\":\"300\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664171823);
INSERT INTO `ea_system_log_202209` VALUES (656, 1, '/admin/system.uploadfile/delete?id=299', 'post', '', '{\"id\":\"299\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664171915);
INSERT INTO `ea_system_log_202209` VALUES (657, 1, '/admin/system.uploadfile/delete?id=300', 'post', '', '{\"id\":\"300\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664171987);
INSERT INTO `ea_system_log_202209` VALUES (658, 1, '/admin/system.uploadfile/delete?id=299', 'post', '', '{\"id\":\"299\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664172009);
INSERT INTO `ea_system_log_202209` VALUES (659, 1, '/admin/system.uploadfile/delete?id=299', 'post', '', '{\"id\":\"299\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664172054);
INSERT INTO `ea_system_log_202209` VALUES (660, 1, '/admin/system.uploadfile/delete?id=298', 'post', '', '{\"id\":\"298\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664172058);
INSERT INTO `ea_system_log_202209` VALUES (661, 1, '/admin/system.uploadfile/delete?id=298', 'post', '', '{\"id\":\"298\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664173457);
INSERT INTO `ea_system_log_202209` VALUES (662, 1, '/admin/system.uploadfile/delete?id=297', 'post', '', '{\"id\":\"297\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664173463);
INSERT INTO `ea_system_log_202209` VALUES (663, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664174630);
INSERT INTO `ea_system_log_202209` VALUES (664, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664174646);
INSERT INTO `ea_system_log_202209` VALUES (665, 1, '/admin/system.uploadfile/delete?id=297', 'post', '', '{\"id\":\"297\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664174646);
INSERT INTO `ea_system_log_202209` VALUES (666, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664174872);
INSERT INTO `ea_system_log_202209` VALUES (667, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664174883);
INSERT INTO `ea_system_log_202209` VALUES (668, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664174983);
INSERT INTO `ea_system_log_202209` VALUES (669, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664174996);
INSERT INTO `ea_system_log_202209` VALUES (670, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664175284);
INSERT INTO `ea_system_log_202209` VALUES (671, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664175307);
INSERT INTO `ea_system_log_202209` VALUES (672, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664175382);
INSERT INTO `ea_system_log_202209` VALUES (673, 1, '/admin/Ajax/getCsrfToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664175387);
INSERT INTO `ea_system_log_202209` VALUES (674, 1, '/admin/ajax/getToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664175588);
INSERT INTO `ea_system_log_202209` VALUES (675, 1, '/admin/ajax/getToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664175630);
INSERT INTO `ea_system_log_202209` VALUES (676, 1, '/admin/ajax/getToken.html', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664175690);
INSERT INTO `ea_system_log_202209` VALUES (677, 1, '/admin/system.uploadfile/delete?id=297', 'post', '', '{\"id\":\"297\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176173);
INSERT INTO `ea_system_log_202209` VALUES (678, 1, '/admin/system.uploadfile/delete?id=297', 'post', '', '{\"id\":\"297\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176289);
INSERT INTO `ea_system_log_202209` VALUES (679, 1, '/admin/system.uploadfile/delete?id=297', 'post', '', '{\"id\":\"297\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176297);
INSERT INTO `ea_system_log_202209` VALUES (680, 1, '/admin/system.uploadfile/delete?id=297', 'post', '', '{\"id\":\"297\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176362);
INSERT INTO `ea_system_log_202209` VALUES (681, 1, '/admin/system.uploadfile/delete?id=296', 'post', '', '{\"id\":\"296\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176366);
INSERT INTO `ea_system_log_202209` VALUES (682, 1, '/admin/system.uploadfile/delete?id=291', 'post', '', '{\"id\":\"291\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176483);
INSERT INTO `ea_system_log_202209` VALUES (683, 1, '/admin/system.uploadfile/delete?id=291', 'post', '', '{\"id\":\"291\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176483);
INSERT INTO `ea_system_log_202209` VALUES (684, 1, '/admin/system.uploadfile/delete?id=290', 'post', '', '{\"id\":\"290\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176494);
INSERT INTO `ea_system_log_202209` VALUES (685, 1, '/admin/system.uploadfile/delete?id=287', 'post', '', '{\"id\":\"287\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176513);
INSERT INTO `ea_system_log_202209` VALUES (686, 1, '/admin/system.uploadfile/delete?id=305', 'post', '', '{\"id\":\"305\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664176748);
INSERT INTO `ea_system_log_202209` VALUES (687, 1, '/admin/system.uploadfile/delete?id=290', 'post', '', '{\"id\":\"290\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664177649);
INSERT INTO `ea_system_log_202209` VALUES (688, 1, '/admin/system.uploadfile/delete?id=303', 'post', '', '{\"id\":\"303\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664177675);
INSERT INTO `ea_system_log_202209` VALUES (689, 1, '/admin/system.uploadfile/delete?id=304', 'post', '', '{\"id\":\"304\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664177689);
INSERT INTO `ea_system_log_202209` VALUES (690, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664177917);
INSERT INTO `ea_system_log_202209` VALUES (691, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664177926);
INSERT INTO `ea_system_log_202209` VALUES (692, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178234);
INSERT INTO `ea_system_log_202209` VALUES (693, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178240);
INSERT INTO `ea_system_log_202209` VALUES (694, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178352);
INSERT INTO `ea_system_log_202209` VALUES (695, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178360);
INSERT INTO `ea_system_log_202209` VALUES (696, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178596);
INSERT INTO `ea_system_log_202209` VALUES (697, 1, '/admin/mall.goods/add', 'post', '', '{\"cate_id\":\"9\",\"title\":\"123\",\"logo\":\"12\",\"file\":\"\",\"images\":\"12\",\"market_price\":\"0\",\"discount_price\":\"0\",\"virtual_sales\":\"0\",\"describe\":\"&lt;p&gt;123213&lt;\\/p&gt;\\n\",\"sort\":\"1\",\"remark\":\"123\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178639);
INSERT INTO `ea_system_log_202209` VALUES (698, 1, '/admin/mall.goods/add', 'post', '', '{\"cate_id\":\"9\",\"title\":\"1\",\"logo\":\"1\",\"file\":\"\",\"images\":\"1\",\"market_price\":\"1\",\"discount_price\":\"1\",\"virtual_sales\":\"1\",\"describe\":\"&lt;p&gt;1&lt;\\/p&gt;\\n\",\"sort\":\"1\",\"remark\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178656);
INSERT INTO `ea_system_log_202209` VALUES (699, 1, '/admin/mall.goods/add', 'post', '', '{\"cate_id\":\"9\",\"title\":\"1\",\"logo\":\"1\",\"file\":\"\",\"images\":\"1\",\"market_price\":\"1\",\"discount_price\":\"1\",\"virtual_sales\":\"1\",\"describe\":\"&lt;p&gt;1&lt;\\/p&gt;\\n\",\"sort\":\"1\",\"remark\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664178657);
INSERT INTO `ea_system_log_202209` VALUES (700, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"289\",\"288\",\"287\",\"286\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664181775);
INSERT INTO `ea_system_log_202209` VALUES (701, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"306\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664181781);
INSERT INTO `ea_system_log_202209` VALUES (702, 1, '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"308\"]}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664181788);
INSERT INTO `ea_system_log_202209` VALUES (703, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182240);
INSERT INTO `ea_system_log_202209` VALUES (704, 1, '/admin/system.uploadfile/delete?id=309', 'post', '', '{\"id\":\"309\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182254);
INSERT INTO `ea_system_log_202209` VALUES (705, 1, '/admin/system.uploadfile/delete?id=309', 'post', '', '{\"id\":\"309\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182316);
INSERT INTO `ea_system_log_202209` VALUES (706, 1, '/admin/system.uploadfile/delete?id=308', 'post', '', '{\"id\":\"308\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182327);
INSERT INTO `ea_system_log_202209` VALUES (707, 1, '/admin/system.uploadfile/delete?id=307', 'post', '', '{\"id\":\"307\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182486);
INSERT INTO `ea_system_log_202209` VALUES (708, 1, '/admin/system.uploadfile/delete?id=307', 'post', '', '{\"id\":\"307\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182495);
INSERT INTO `ea_system_log_202209` VALUES (709, 1, '/admin/system.uploadfile/delete?id=307', 'post', '', '{\"id\":\"307\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182509);
INSERT INTO `ea_system_log_202209` VALUES (710, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182529);
INSERT INTO `ea_system_log_202209` VALUES (711, 1, '/admin/system.uploadfile/delete?id=310', 'post', '', '{\"id\":\"310\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182535);
INSERT INTO `ea_system_log_202209` VALUES (712, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"227\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182729);
INSERT INTO `ea_system_log_202209` VALUES (713, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182736);
INSERT INTO `ea_system_log_202209` VALUES (714, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182737);
INSERT INTO `ea_system_log_202209` VALUES (715, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182737);
INSERT INTO `ea_system_log_202209` VALUES (716, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182739);
INSERT INTO `ea_system_log_202209` VALUES (717, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182740);
INSERT INTO `ea_system_log_202209` VALUES (718, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182740);
INSERT INTO `ea_system_log_202209` VALUES (719, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182775);
INSERT INTO `ea_system_log_202209` VALUES (720, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182775);
INSERT INTO `ea_system_log_202209` VALUES (721, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182784);
INSERT INTO `ea_system_log_202209` VALUES (722, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182788);
INSERT INTO `ea_system_log_202209` VALUES (723, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664182788);
INSERT INTO `ea_system_log_202209` VALUES (724, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"249\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183104);
INSERT INTO `ea_system_log_202209` VALUES (725, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"249\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183128);
INSERT INTO `ea_system_log_202209` VALUES (726, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"249\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183136);
INSERT INTO `ea_system_log_202209` VALUES (727, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183194);
INSERT INTO `ea_system_log_202209` VALUES (728, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183199);
INSERT INTO `ea_system_log_202209` VALUES (729, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183483);
INSERT INTO `ea_system_log_202209` VALUES (730, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183484);
INSERT INTO `ea_system_log_202209` VALUES (731, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183485);
INSERT INTO `ea_system_log_202209` VALUES (732, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183491);
INSERT INTO `ea_system_log_202209` VALUES (733, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183496);
INSERT INTO `ea_system_log_202209` VALUES (734, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183633);
INSERT INTO `ea_system_log_202209` VALUES (735, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183633);
INSERT INTO `ea_system_log_202209` VALUES (736, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183648);
INSERT INTO `ea_system_log_202209` VALUES (737, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183648);
INSERT INTO `ea_system_log_202209` VALUES (738, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183655);
INSERT INTO `ea_system_log_202209` VALUES (739, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183655);
INSERT INTO `ea_system_log_202209` VALUES (740, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183762);
INSERT INTO `ea_system_log_202209` VALUES (741, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183766);
INSERT INTO `ea_system_log_202209` VALUES (742, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183826);
INSERT INTO `ea_system_log_202209` VALUES (743, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183829);
INSERT INTO `ea_system_log_202209` VALUES (744, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183995);
INSERT INTO `ea_system_log_202209` VALUES (745, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664183995);
INSERT INTO `ea_system_log_202209` VALUES (746, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184068);
INSERT INTO `ea_system_log_202209` VALUES (747, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184068);
INSERT INTO `ea_system_log_202209` VALUES (748, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184159);
INSERT INTO `ea_system_log_202209` VALUES (749, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184159);
INSERT INTO `ea_system_log_202209` VALUES (750, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184205);
INSERT INTO `ea_system_log_202209` VALUES (751, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184221);
INSERT INTO `ea_system_log_202209` VALUES (752, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184232);
INSERT INTO `ea_system_log_202209` VALUES (753, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184305);
INSERT INTO `ea_system_log_202209` VALUES (754, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184312);
INSERT INTO `ea_system_log_202209` VALUES (755, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184324);
INSERT INTO `ea_system_log_202209` VALUES (756, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184422);
INSERT INTO `ea_system_log_202209` VALUES (757, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184455);
INSERT INTO `ea_system_log_202209` VALUES (758, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184460);
INSERT INTO `ea_system_log_202209` VALUES (759, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184618);
INSERT INTO `ea_system_log_202209` VALUES (760, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184623);
INSERT INTO `ea_system_log_202209` VALUES (761, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184655);
INSERT INTO `ea_system_log_202209` VALUES (762, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664184666);
INSERT INTO `ea_system_log_202209` VALUES (763, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185008);
INSERT INTO `ea_system_log_202209` VALUES (764, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185014);
INSERT INTO `ea_system_log_202209` VALUES (765, 1, '/admin/mall.goods/edit?id=11', 'post', '', '{\"id\":\"11\",\"cate_id\":\"9\",\"title\":\"1\",\"logo\":\"1\",\"file\":\"\",\"images\":\"1|http:\\/\\/ey.cn\\/upload\\/20220926\\/5fbda6b44d6effe26d6b5482862f1469.png|http:\\/\\/ey.cn\\/upload\\/20220926\\/48c5e6d179374e42c38af0dd0711c272.png\",\"market_price\":\"1.00\",\"discount_price\":\"1.00\",\"virtual_sales\":\"1\",\"describe\":\"&lt;p&gt;1&lt;\\/p&gt;\\n\",\"sort\":\"1\",\"remark\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185019);
INSERT INTO `ea_system_log_202209` VALUES (766, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185301);
INSERT INTO `ea_system_log_202209` VALUES (767, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185301);
INSERT INTO `ea_system_log_202209` VALUES (768, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185819);
INSERT INTO `ea_system_log_202209` VALUES (769, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185833);
INSERT INTO `ea_system_log_202209` VALUES (770, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"250\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185835);
INSERT INTO `ea_system_log_202209` VALUES (771, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"249\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185974);
INSERT INTO `ea_system_log_202209` VALUES (772, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"249\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664185975);
INSERT INTO `ea_system_log_202209` VALUES (773, 1, '/admin/system.menu/edit?id=248', 'post', '', '{\"id\":\"248\",\"pid\":\"228\",\"title\":\"上传管理\",\"href\":\"system.uploadfile\\/index\",\"icon\":\"fa fa-arrow-up\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186000);
INSERT INTO `ea_system_log_202209` VALUES (774, 1, '/admin/system.menu/edit?id=251', 'post', '', '{\"id\":\"251\",\"pid\":\"249\",\"title\":\"商品管理\",\"href\":\"mall.goods\\/index\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186009);
INSERT INTO `ea_system_log_202209` VALUES (775, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186029);
INSERT INTO `ea_system_log_202209` VALUES (776, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186043);
INSERT INTO `ea_system_log_202209` VALUES (777, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186166);
INSERT INTO `ea_system_log_202209` VALUES (778, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186170);
INSERT INTO `ea_system_log_202209` VALUES (779, 1, '/admin/system.quick/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186225);
INSERT INTO `ea_system_log_202209` VALUES (780, 1, '/admin/system.quick/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186230);
INSERT INTO `ea_system_log_202209` VALUES (781, 1, '/admin/system.quick/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186237);
INSERT INTO `ea_system_log_202209` VALUES (782, 1, '/admin/system.quick/modify', 'post', '', '{\"id\":\"6\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186509);
INSERT INTO `ea_system_log_202209` VALUES (783, 1, '/admin/system.quick/modify', 'post', '', '{\"id\":\"6\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664186510);
INSERT INTO `ea_system_log_202209` VALUES (784, NULL, '/admin/login/index.html?username=admin&password=123456', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664261178);
INSERT INTO `ea_system_log_202209` VALUES (785, NULL, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664261651);
INSERT INTO `ea_system_log_202209` VALUES (786, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664261759);
INSERT INTO `ea_system_log_202209` VALUES (787, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664261768);
INSERT INTO `ea_system_log_202209` VALUES (788, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664261845);
INSERT INTO `ea_system_log_202209` VALUES (789, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664261855);
INSERT INTO `ea_system_log_202209` VALUES (790, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664263834);
INSERT INTO `ea_system_log_202209` VALUES (791, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664263845);
INSERT INTO `ea_system_log_202209` VALUES (792, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664263857);
INSERT INTO `ea_system_log_202209` VALUES (793, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664263874);
INSERT INTO `ea_system_log_202209` VALUES (794, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664264129);
INSERT INTO `ea_system_log_202209` VALUES (795, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664264134);
INSERT INTO `ea_system_log_202209` VALUES (796, NULL, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664266349);
INSERT INTO `ea_system_log_202209` VALUES (797, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664270052);
INSERT INTO `ea_system_log_202209` VALUES (798, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664270073);
INSERT INTO `ea_system_log_202209` VALUES (799, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272792);
INSERT INTO `ea_system_log_202209` VALUES (800, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272829);
INSERT INTO `ea_system_log_202209` VALUES (801, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272829);
INSERT INTO `ea_system_log_202209` VALUES (802, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272839);
INSERT INTO `ea_system_log_202209` VALUES (803, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272839);
INSERT INTO `ea_system_log_202209` VALUES (804, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272855);
INSERT INTO `ea_system_log_202209` VALUES (805, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272855);
INSERT INTO `ea_system_log_202209` VALUES (806, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664272868);
INSERT INTO `ea_system_log_202209` VALUES (807, NULL, '/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664328358);
INSERT INTO `ea_system_log_202209` VALUES (808, NULL, '/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664343992);
INSERT INTO `ea_system_log_202209` VALUES (809, 1, '/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664345026);
INSERT INTO `ea_system_log_202209` VALUES (810, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664345096);
INSERT INTO `ea_system_log_202209` VALUES (811, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"251\",\"field\":\"status\",\"value\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664345097);
INSERT INTO `ea_system_log_202209` VALUES (812, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664345887);
INSERT INTO `ea_system_log_202209` VALUES (813, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36', 1664345960);
INSERT INTO `ea_system_log_202209` VALUES (814, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36', 1664345967);
INSERT INTO `ea_system_log_202209` VALUES (815, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664346000);
INSERT INTO `ea_system_log_202209` VALUES (816, NULL, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664347534);
INSERT INTO `ea_system_log_202209` VALUES (817, NULL, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664347888);
INSERT INTO `ea_system_log_202209` VALUES (818, NULL, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"mpff\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664347996);
INSERT INTO `ea_system_log_202209` VALUES (819, 1, '/admin/system.node/refreshNode?force=1', 'post', '', '{\"force\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664350342);
INSERT INTO `ea_system_log_202209` VALUES (820, 1, '/admin/system.menu/modify', 'post', '', '{\"id\":\"227\",\"field\":\"status\",\"value\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664353312);
INSERT INTO `ea_system_log_202209` VALUES (821, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"x22g\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36 SE 2.X MetaSr 1.0', 1664355867);

-- ----------------------------
-- Table structure for ea_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_menu`;
CREATE TABLE `ea_system_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `href` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) NULL DEFAULT 0 COMMENT '菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title`(`title`) USING BTREE,
  INDEX `href`(`href`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 254 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_menu
-- ----------------------------
INSERT INTO `ea_system_menu` VALUES (227, 99999999, '后台首页', 'fa fa-home', 'index/welcome', '', '_self', 0, 1, NULL, NULL, 1573120497, NULL);
INSERT INTO `ea_system_menu` VALUES (228, 0, '系统管理', 'fa fa-cog', '', '', '_self', 0, 1, '', NULL, 1588999529, NULL);
INSERT INTO `ea_system_menu` VALUES (234, 228, '菜单管理', 'fa fa-tree', 'system.menu/index', '', '_self', 10, 1, '', NULL, 1588228555, NULL);
INSERT INTO `ea_system_menu` VALUES (244, 228, '管理员管理', 'fa fa-user', 'system.admin/index', '', '_self', 12, 1, '', 1573185011, 1588228573, NULL);
INSERT INTO `ea_system_menu` VALUES (245, 228, '角色管理', 'fa fa-bitbucket-square', 'system.auth/index', '', '_self', 11, 1, '', 1573435877, 1588228634, NULL);
INSERT INTO `ea_system_menu` VALUES (246, 228, '节点管理', 'fa fa-list', 'system.node/index', '', '_self', 9, 1, '', 1573435919, 1588228648, NULL);
INSERT INTO `ea_system_menu` VALUES (247, 228, '配置管理', 'fa fa-asterisk', 'system.config/index', '', '_self', 8, 1, '', 1573457448, 1588228566, NULL);
INSERT INTO `ea_system_menu` VALUES (248, 228, '上传管理', 'fa fa-arrow-up', 'system.uploadfile/index', '', '_self', 0, 1, '', 1573542953, 1664186000, NULL);
INSERT INTO `ea_system_menu` VALUES (249, 0, '商城管理', 'fa fa-list', '', '', '_self', 0, 1, '', 1589439884, 1664185975, NULL);
INSERT INTO `ea_system_menu` VALUES (250, 249, '商品分类', 'fa fa-calendar-check-o', 'mall.cate/index', '', '_self', 0, 1, '', 1589439910, 1664185835, NULL);
INSERT INTO `ea_system_menu` VALUES (251, 249, '商品管理', 'fa fa-list', 'mall.goods/index', '', '_self', 0, 1, '', 1589439931, 1664345097, NULL);
INSERT INTO `ea_system_menu` VALUES (252, 228, '快捷入口', 'fa fa-list', 'system.quick/index', '', '_self', 0, 1, '', 1589623683, 1589623683, NULL);
INSERT INTO `ea_system_menu` VALUES (253, 228, '日志管理', 'fa fa-connectdevelop', 'system.log/index', '', '_self', 0, 1, '', 1589623684, 1589623684, NULL);

-- ----------------------------
-- Table structure for ea_system_node
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_node`;
CREATE TABLE `ea_system_node`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `node` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点代码',
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点标题',
  `type` tinyint(1) NULL DEFAULT 3 COMMENT '节点类型（1：控制器，2：节点）',
  `is_auth` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否启动RBAC权限控制',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `node`(`node`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统节点表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_node
-- ----------------------------
INSERT INTO `ea_system_node` VALUES (1, 'system.admin', '管理员管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (2, 'system.admin/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (3, 'system.admin/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (4, 'system.admin/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (5, 'system.admin/password', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (6, 'system.admin/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (7, 'system.admin/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (8, 'system.admin/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (9, 'system.auth', '角色权限管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (10, 'system.auth/authorize', '授权', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (11, 'system.auth/saveAuthorize', '授权保存', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (12, 'system.auth/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (13, 'system.auth/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (14, 'system.auth/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (15, 'system.auth/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (16, 'system.auth/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (17, 'system.auth/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (18, 'system.config', '系统配置管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (19, 'system.config/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (20, 'system.config/save', '保存', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (21, 'system.menu', '菜单管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (22, 'system.menu/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (23, 'system.menu/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (24, 'system.menu/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (25, 'system.menu/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (26, 'system.menu/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (27, 'system.menu/getMenuTips', '添加菜单提示', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (28, 'system.menu/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (29, 'system.node', '系统节点管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (30, 'system.node/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (31, 'system.node/refreshNode', '系统节点更新', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (32, 'system.node/clearNode', '清除失效节点', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (33, 'system.node/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (34, 'system.node/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (35, 'system.node/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (36, 'system.node/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (37, 'system.node/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (38, 'system.uploadfile', '上传文件管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (39, 'system.uploadfile/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (40, 'system.uploadfile/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (41, 'system.uploadfile/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (42, 'system.uploadfile/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (43, 'system.uploadfile/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (44, 'system.uploadfile/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (45, 'mall.cate', '商品分类管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (46, 'mall.cate/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (47, 'mall.cate/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (48, 'mall.cate/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (49, 'mall.cate/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (50, 'mall.cate/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (51, 'mall.cate/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (52, 'mall.goods', '商城商品管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (53, 'mall.goods/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (54, 'mall.goods/stock', '入库', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (55, 'mall.goods/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (56, 'mall.goods/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (57, 'mall.goods/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (58, 'mall.goods/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (59, 'mall.goods/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `ea_system_node` VALUES (60, 'system.quick', '快捷入口管理', 1, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (61, 'system.quick/index', '列表', 2, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (62, 'system.quick/add', '添加', 2, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (63, 'system.quick/edit', '编辑', 2, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (64, 'system.quick/delete', '删除', 2, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (65, 'system.quick/export', '导出', 2, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (66, 'system.quick/modify', '属性修改', 2, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (67, 'system.log', '操作日志管理', 1, 1, 1589623188, 1589623188);
INSERT INTO `ea_system_node` VALUES (68, 'system.log/index', '列表', 2, 1, 1589623188, 1589623188);

-- ----------------------------
-- Table structure for ea_system_quick
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_quick`;
CREATE TABLE `ea_system_quick`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '快捷入口名称',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '快捷链接',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统快捷入口表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_quick
-- ----------------------------
INSERT INTO `ea_system_quick` VALUES (1, '管理员管理', 'fa fa-user', 'system.admin/index', 0, 1, '', 1589624097, 1589624792, NULL);
INSERT INTO `ea_system_quick` VALUES (2, '角色管理', 'fa fa-bitbucket-square', 'system.auth/index', 0, 1, '', 1589624772, 1589624781, NULL);
INSERT INTO `ea_system_quick` VALUES (3, '菜单管理', 'fa fa-tree', 'system.menu/index', 0, 1, NULL, 1589624097, 1589624792, NULL);
INSERT INTO `ea_system_quick` VALUES (6, '节点管理', 'fa fa-list', 'system.node/index', 0, 1, NULL, 1589624772, 1664186510, NULL);
INSERT INTO `ea_system_quick` VALUES (7, '配置管理', 'fa fa-asterisk', 'system.config/index', 0, 1, NULL, 1589624097, 1589624792, NULL);
INSERT INTO `ea_system_quick` VALUES (8, '上传管理', 'fa fa-arrow-up', 'system.uploadfile/index', 0, 1, NULL, 1589624772, 1589624781, NULL);
INSERT INTO `ea_system_quick` VALUES (10, '商品分类', 'fa fa-calendar-check-o', 'mall.cate/index', 0, 1, NULL, 1589624097, 1589624792, NULL);
INSERT INTO `ea_system_quick` VALUES (11, '商品管理', 'fa fa-list', 'mall.goods/index', 0, 1, NULL, 1589624772, 1589624781, NULL);

-- ----------------------------
-- Table structure for ea_system_uploadfile
-- ----------------------------
DROP TABLE IF EXISTS `ea_system_uploadfile`;
CREATE TABLE `ea_system_uploadfile`  (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `upload_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `original_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件原名',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物理路径',
  `image_width` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '宽度',
  `image_height` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '高度',
  `image_type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片类型',
  `image_frames` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片帧数',
  `mime_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mime类型',
  `file_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `file_ext` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sha1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `upload_time` int(10) NULL DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `upload_type`(`upload_type`) USING BTREE,
  INDEX `original_name`(`original_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 354 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '上传文件表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ea_system_uploadfile
-- ----------------------------
INSERT INTO `ea_system_uploadfile` VALUES (306, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/61008f1e04969dfd76cfafc873346bad.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664177917, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (310, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/b92071e604446fe747409746ecdcc60e.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664182529, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (311, 'local', '200.png', 'http://ey.cn/upload/20220926/5d3d7c2d03038754ee7f72320062ed72.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183633, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (312, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/6972493ee8199f594d3f643202e25a1a.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183633, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (313, 'local', '200.png', 'http://ey.cn/upload/20220926/c6167f775c13e9f35bd55a822e01335a.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183648, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (314, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/5b1df482a0b1d5bac0f0fc33c6c29008.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183648, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (315, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/117c199484d164497bd9d1bbaa55c7c9.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183762, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (316, 'local', '200.png', 'http://ey.cn/upload/20220926/bdad50d80162c7c19e04f18ce3a6583e.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183826, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (317, 'local', '200.png', 'http://ey.cn/upload/20220926/f10862b7d8d5e857dc04f267cc07ff00.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183995, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (318, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/bb5215484e46201d5147e24c9469a444.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664183996, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (319, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/5c7aae33a10fdd1bc9ecbc0f22ace9bc.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184068, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (320, 'local', '200.png', 'http://ey.cn/upload/20220926/d3cfd573f45f5e7398dd19665930c1ab.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184068, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (321, 'local', '200.png', 'http://ey.cn/upload/20220926/2e8718aa3ac15fedb271cfcdf02f22b6.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184159, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (322, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/a49e4a95e11e98c98c55b7e3881dc7e4.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184159, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (323, 'local', '200.png', 'http://ey.cn/upload/20220926/5305491bbafbfaa1d906adc0a4337af7.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184205, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (324, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/f0b617e5ea45a592302ed2be3ce7f325.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184232, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (325, 'local', '200.png', 'http://ey.cn/upload/20220926/2c80af2fb86c2a773648467f66180091.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184305, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (326, 'local', '200.png', 'http://ey.cn/upload/20220926/e127c348c6da7f4de67df0fadf409eee.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184423, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (327, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/598ccb50ea0370ebc45f6a3e98782f7c.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184618, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (328, 'local', '200.png', 'http://ey.cn/upload/20220926/8051e7281d8c98c0a363ef7e0515140c.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664184623, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (329, 'local', '200.png', 'http://ey.cn/upload/20220926/5fbda6b44d6effe26d6b5482862f1469.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664185009, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (330, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/48c5e6d179374e42c38af0dd0711c272.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664185014, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (331, 'local', '200.png', 'http://ey.cn/upload/20220926/9aca1c1096153bab58c8b60199e29510.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664185301, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (332, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220926/b7077c18bc7d7b4a9fd5b8f66dcb8573.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664185301, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (333, 'local', '200.png', 'http://ey.cn/upload/20220927/f54786b89fce5fb015c62ae6486551ee.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664261759, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (334, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220927/9802ac1e282172ff08ebdf0096e4913d.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664261768, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (335, 'local', '200.png', 'http://ey.cn/upload/20220927/1492a09f60804f45e2b98673428b7a19.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664261845, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (336, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220927/c1c9378a9816a68503dc98fed18ecc95.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664261855, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (337, 'local', '200.png', 'http://ey.cn/upload/20220927/6653c51acfe26c7c070ee4237f2cf054.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664263834, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (338, 'local', '200.png', 'http://ey.cn/upload/20220927/dc0e511efcda5b64f24883c741450a5a.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664263845, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (339, 'local', '200.png', 'http://ey.cn/upload/20220927/06eb460439e6c80e9291927053caa8eb.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664263858, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (340, 'local', '200.png', 'http://ey.cn/upload/20220927/74ea5197abc10efc430d3e236e54e6f1.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664263875, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (341, 'local', '200.png', 'http://ey.cn/upload/20220927/f5c27388a62d8f00531c878ea279e492.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664264129, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (342, 'local', '200.png', 'http://ey.cn/upload/20220927/529f536c306701e8a09606624192c478.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664265140, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (343, 'local', '200.png', 'http://ey.cn/upload/20220927/782f72d61b494d4eb90f7fedb16f8ef1.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664270052, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (344, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220927/d0069720209444df77f5ef092fadeea0.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664270073, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (345, 'local', '200.png', 'http://ey.cn/upload/20220927/5864e49cb3d196672ffe2f04f6732734.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272792, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (346, 'local', '200.png', 'http://ey.cn/upload/20220927/330acc25b6d983f772e8287fe79a877d.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272829, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (347, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220927/4bb9758d1a27a27d3ae37a71e5632dee.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272829, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (348, 'local', '200.png', 'http://ey.cn/upload/20220927/dc3db2b89c6ba87b26a65bde7d9310ee.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272839, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (349, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220927/e13459902af9fcafdf950baa78ec7dd7.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272839, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (350, 'local', '微信截图_20220916152206.png', 'http://ey.cn/upload/20220927/9d4d582f5ef7295eb5fddf68b6806803.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272856, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (351, 'local', '200.png', 'http://ey.cn/upload/20220927/076710280c377957bcd71b87422fe150.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272856, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (352, 'local', '200.png', 'http://ey.cn/upload/20220927/3e5e81ec2e8d9c64f14bc20d5c2fdcad.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664272868, NULL, NULL);
INSERT INTO `ea_system_uploadfile` VALUES (353, 'local', '200.png', 'http://admin.ey.cn/upload/20220928/b92a03e33296b76ffb0ce324e52c7900.png', '', '', '', 0, 'image/png', 0, 'png', '', 1664345026, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
