<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class AdWechat extends TimeModel
{

    protected $deleteTime = "delete_time";


    public function getSexList()
    {
        return ['1'=>'男','2'=>'女','0'=>'未知',];
    }

    public function getStatusList()
    {
        return ['0'=>'禁用','1'=>'启用',];
    }

    public function adWechatCate()
    {
        return $this->belongsTo('\app\admin\model\AdWechatCate', 'cate_id', 'id');
    }

}