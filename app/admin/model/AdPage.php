<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class AdPage extends TimeModel
{

    protected $name = "ad_page";

    protected $deleteTime = "delete_time";

    
    public function adPageCate()
    {
        return $this->belongsTo('\app\admin\model\AdPageCate', 'cate_id', 'id');
    }

    
    public function getStatusList()
    {
        return ['0'=>'禁用','1'=>'启用',];
    }


}