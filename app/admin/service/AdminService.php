<?php

// +----------------------------------------------------------------------
// | EasyAdmin
// +----------------------------------------------------------------------
// | PHP交流群: 763822524
// +----------------------------------------------------------------------
// | 开源协议  https://mit-license.org 
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zhongshaofa/EasyAdmin
// +----------------------------------------------------------------------

namespace app\admin\service;

use app\common\constants\AdminConstant;

class AdminService
{
    public static function getId()
    {
        return session('admin.id');
    }

    public static function getAdminInfo()
    {
        return session('admin');
    }

    public static function isSuper()
    {
        return session('admin.id') === AdminConstant::SUPER_ADMIN_ID;
    }

}