<?php

namespace app\admin\controller\ad;

use app\admin\model\AdWechat;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="微信分组")
 */
class WechatCate extends AdminController
{

    protected $person = true;

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\AdWechatCate();

    }


    /**
     * @NodeAnotation(title="删除")
     */
    public function delete($id)
    {
        $this->checkPostRequest();
        $row = $this->model->whereIn('id', $id)->select();
        $row->isEmpty() && $this->error('数据不存在');
        //查询是否存在微信号
        $wechat = AdWechat::whereIn('cate_id',$id)->count();
        if(!empty($wechat)){
            $this->error('请先删除此分组下的微信号后再删除分组！');
        }
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        $save ? $this->success('删除成功') : $this->error('删除失败');
    }



}