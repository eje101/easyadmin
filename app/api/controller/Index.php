<?php

namespace app\api\controller;

use app\BaseController;
use think\facade\Db;
use think\facade\Log;

class Index extends BaseController
{
    public function index()
    {
        return '您好！这是一个[api]示例应用';
    }

    public function getInit()
    {
        $qrcode = [
            'id' => 1,
            'number' => '测试微信号',
            'image' => 'https://wf.weimobmj.com/upload/d3/4932ce731cc7c78bd1a4aec206df0f.png',
            'time'=>time()
        ];
        return [
            'code' => 0,
            'msg' => '成功',
            'data' => $qrcode
        ];
    }

    public function sendData()
    {
        $data = $this->request->post();
        $res = [
            'type'=>'js',
            ''
        ];
        Log::info('接收数据:{data}',['data'=>serialize($data)]);
        return $data;
    }
}