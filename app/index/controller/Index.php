<?php
declare (strict_types = 1);

namespace app\index\controller;

use app\BaseController;
use TencentAds\TencentAds;

class Index extends BaseController
{
    public function index()
    {
        return $this->app->view->fetch();
        return '您好！这是一个[index]示例应用';
    }

    public function auth()
    {
        $tads = TencentAds::init([
            'is_debug'=>true
        ]);
        $token = $tads->userActionSets()
            ->add();

        dd($token);
    }
}
