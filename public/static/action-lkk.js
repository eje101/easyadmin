;!function () {
    loadScript("http://code.jquery.com/jquery-2.1.1.min.js");
    const BaseUrl = 'http://weimob.jnzhengpeng.com',
        InitUrl = '/api/index/getInit',//初始化内容
        sendDataUrl = '/api/index/sendData';//记录上报
    //行为数据
    let option = {
        pageId: UrlSearch(document.scripts[document.scripts.length - 1].src).id,
        clickId: '',
        uid: 0,//用户ID
        isMobile: isMobile(),//是否是手机
        duration: 0, //页面停留时间秒
        backTime: 0, //离开到回来时间
        loadingTime: 0,//加载时间秒
        viewLength: 0,//页面访问最大深度百分比
        actionTime: 0,//到事件触发的
        maxLengthTime: 0,//到达最大深度用的秒
        addFriedAction: '',//添加加好友事件
        sendAction: '',//发送事件
        sendType: 'api',//回传方式[api,js]
        startTime: Math.ceil(new Date().getTime() / 1000)//页面打开时间
    }

    window.pageShowCbList = [];
    window.pageHideCbList = [];

    //页面加载执行
    window.onload = (e) => {
        option.clickId = getUrlParam('qz_gdt');
        if (!option.clickId) {
            option.clickId = getUrlParam('clickId');
            if (!option.clickId) {
                option.clickId = getUrlParam('gdt_vid');
            }
        }
        //获取用户id
        option.uid = getCookie("uid")
        if (isEmpty(option.uid)) {
            option.uid = guid();
            setCookie("uid", option.uid)
        }
        option.loadingTime = getDiffTime();
        //页面切换事件
        pageShowOrHide(
            () => {
                for (var i = 0; i < window.pageShowCbList.length; i++) {
                    var callback = window.pageShowCbList[i];
                    callback && callback();
                }
            },
            () => {
                for (var i = 0; i < window.pageHideCbList.length; i++) {
                    var callback = window.pageHideCbList[i];
                    callback && callback();
                }
            }
        );

        window.pageShowCbList.push(() => {
            option.backTime = getDiffTime() - option.duration;
            sendData('page_show');
            //清空好友事件
            option.addFriedAction = ''
            console.log('离开页面到到回来相隔时间', option.backTime);
        });

        window.pageHideCbList.push(() => {
            option.duration = getDiffTime();
            sendData('page_hide');
            console.log('页面停留时常', option.duration)
        });

        //获取微信号及二维码和内容
        let {id, number, image, send_type} = getInit();
        if (send_type) {
            option.sendType = send_type;
        }
        let touchList = document.querySelectorAll("[data-touch]");
        if (touchList.length > 0) {
            $.each(touchList, function (index, elem) {
                let timer = null
                $(this).html('<img alt="' + number + '" src="' + image + '">');
                elem.addEventListener('touchstart', function () {
                    console.log('开始' + id)
                    timer = setTimeout(function () {
                        option.addFriedAction = 'touch_qrcode'
                        sendData('page_touch')
                    }, 500)
                })
                elem.addEventListener('touchmove', function () {
                    console.log('移动销毁' + id)
                    clearTimeout(timer);
                    return false;
                })
                elem.addEventListener("touchend", function () {
                    console.log('结束' + id)
                    clearTimeout(timer);
                    return false;
                })
            })
        }
        //获取点击区域
        let copyList = document.querySelectorAll("[data-copy]");
        $.each(copyList, function (index, elem) {
            $(this).html('<span style="background-color:yellow;color: #9a5a21">' + number + '</span>');
            $(this).on("click", function () {
                const range = document.createRange(); //创建range对象
                range.selectNode(elem); //获取复制内容的 id 选择器
                const selection = window.getSelection();  //创建 selection对象
                if (selection.rangeCount > 0) selection.removeAllRanges(); //如果页面已经有选取了的话，会自动删除这个选区，没有选区的话，会把这个选取加入选区
                // selection.addRange(range); //将range对象添加到selection选区当中，会高亮文本块
                document.execCommand('copy'); //复制选中的文字到剪贴板
                console.log('复制成功')
                // selection.removeRange(range); // 移除选中的元素
                setTimeout(function () {
                    option.addFriedAction = 'click_number'
                    sendData('page_click')
                    location.href = "weixin://";
                }, 200);
            });
        })
    };

    //页面滚动事件
    window.onscroll = () => {
        //监听事件内容
        let nowLength = getPercentage((getWindowHeight() + getDocumentTop()) / getScrollHeight());
        if (nowLength > option.viewLength) {
            option.maxLengthTime = getDiffTime();
            option.viewLength = nowLength;
        }
    }

    function pageShowOrHide(showCb, hideCb) {
        var hidden, state, visibilityChange;
        if (typeof document.hidden !== "undefined") {
            hidden = "hidden";
            visibilityChange = "visibilitychange";
            state = "visibilityState";
        } else if (typeof document.mozHidden !== "undefined") {
            hidden = "mozHidden";
            visibilityChange = "mozvisibilitychange";
            state = "mozVisibilityState";
        } else if (typeof document.msHidden !== "undefined") {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
            state = "msVisibilityState";
        } else if (typeof document.webkitHidden !== "undefined") {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
            state = "webkitVisibilityState";
        }
        var cb = function () {
            if (document[state] == hidden) {
                hideCb && hideCb();
            } else {
                showCb && showCb();
            }
        };
        document.removeEventListener(visibilityChange, cb, false);
        document.addEventListener(visibilityChange, cb, false);
    }


    //上报请求
    function sendData(even = '') {
        option.sendAction = even
        option.actionTime = getDiffTime()
        $.ajax({
            url: BaseUrl + sendDataUrl,
            type: 'post',
            data: option,
            dataType: "json",
            success: function (res) {
                //返回是否回传
                if (res.sendType !== 'js') {
                    //调用js回传
                }
                console.log(res);
            },
            error: function (xhr, status, thrown) {
                console.log('上报失败')
            }
        });
    }

    //初始化页面
    function getInit() {
        let info = {};
        $.ajax({
            url: BaseUrl + InitUrl,
            type: 'post',
            data: option,
            dataType: "json",
            async: false,
            success: function (res) {
                if (res.code) {
                    console.log('初始化失败')
                }
                info = res.data
            },
            error: function (xhr, status, thrown) {
                console.log('获取数据失败')
            }
        });
        return info;
    }

    //获取时间差
    function getDiffTime() {
        return Math.ceil(new Date().getTime() / 1000) - option.startTime;
    }

    //获取百分比
    function getPercentage(num) {
        var result = parseFloat(num);
        if (isNaN(result)) {
            return 0;
        }
        result = Math.round(num * 100);
        return result;
    }

    //获取唯一ID
    function guid() {
        return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16).toLowerCase();
        });
    }

    function setCookie(a, b, c) {
        c = isEmpty(c) ? (30 * 60 * 1000) : c;
        var d = new Date();
        d.setTime(d.getTime() + c);
        var e = "expires=" + d.toUTCString();
        document.cookie = a + "=" + escape(b) + ";path=/; " + e
    }

    function getCookie(a) {
        var b = document.cookie.match(new RegExp("(^| )" + a + "=([^;]*)(;|$)", "i"));
        if (!isEmpty(b)) {
            return unescape(b[2])
        } else {
            return null
        }
    }

    function clearCookie(a) {
        setCookie(a, "", -1)
    }

    function isEmpty(a) {
        if (typeof a == "undefined" || a == null || a == "null" || a === "" || a == "undefined") {
            return true
        } else {
            return false
        }
    }

    function isPhoneAvailable(a) {
        var b = /^1[3-9][0-9]{9}$/;
        if (!b.test(a)) {
            return false
        } else {
            return true
        }
    }

    function loadScript(b, c) {
        console.log(b);
        const a = document.createElement('script');
        a.type = 'text/javascript';
        a.async = true;
        a.src = b;
        if (a.readyState) {
            a.onreadystatechange = function () {
                if (a.readyState == 'complete' || a.readyState == 'loaded') {
                    a.onreadystatechange = null;
                    if (typeof c === "function") c()
                }
            }
        } else {
            a.onload = function () {
                if (typeof c === "function") c()
            }
        }
        document.head.appendChild(a);
        document.head.removeChild(a);
    }

    //获取js后参数
    function UrlSearch(a) {
        var b = a.indexOf("?");
        var c = a.substr(b + 1);
        var d = {};
        var e = c.split("&");
        for (var i = 0; i < e.length; i++) {
            b = e[i].indexOf("=");
            if (b > 0) {
                d[e[i].substring(0, b)] = e[i].substr(b + 1)
            }
        }
        return d
    }

    //获取地址栏参数方法
    function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }

    //获取点击区域
    function isMobile() {
        var userAgentInfo = navigator.userAgent;

        var mobileAgents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];

        var mobile_flag = false;

        //根据userAgent判断是否是手机
        for (var v = 0; v < mobileAgents.length; v++) {
            if (userAgentInfo.indexOf(mobileAgents[v]) > 0) {
                mobile_flag = true;
                break;
            }
        }
        var screen_width = window.screen.width;
        var screen_height = window.screen.height;

        //根据屏幕分辨率判断是否是手机
        if (screen_width < 500 && screen_height < 800) {
            mobile_flag = true;
        }

        return mobile_flag;
    }


    //文档高度 [文档高度 == 可视窗口高度 + 滚动条高度]
    function getDocumentTop() {
        var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
        if (document.body) {
            bodyScrollTop = document.body.scrollTop;
        }
        if (document.documentElement) {
            documentScrollTop = document.documentElement.scrollTop;
        }
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
        return scrollTop;
    }

    //可视窗口高度
    function getWindowHeight() {
        var windowHeight = 0;
        if (document.compatMode == "CSS1Compat") {
            windowHeight = document.documentElement.clientHeight;
        } else {
            windowHeight = document.body.clientHeight;
        }
        return windowHeight;
    }

    //滚动条滚动高度
    function getScrollHeight() {
        var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;
        if (document.body) {
            bodyScrollHeight = document.body.scrollHeight;
        }
        if (document.documentElement) {
            documentScrollHeight = document.documentElement.scrollHeight;
        }
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
        return scrollHeight;
    }

}()







