;!function (t, e) {
    "function" == typeof define && define.amd ? define(function () {
        return e(t)
    }) : e(t)
}(this, function (t) {
    var e = function () {
        function $(t) {
            return null == t ? String(t) : S[C.call(t)] || "object"
        }

        function F(t) {
            return "function" == $(t)
        }

        function k(t) {
            return null != t && t == t.window
        }

        function M(t) {
            return null != t && t.nodeType == t.DOCUMENT_NODE
        }

        function R(t) {
            return "object" == $(t)
        }

        function Z(t) {
            return R(t) && !k(t) && Object.getPrototypeOf(t) == Object.prototype
        }

        function z(t) {
            var e = !!t && "length" in t && t.length, n = r.type(t);
            return "function" != n && !k(t) && ("array" == n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
        }

        function q(t) {
            return a.call(t, function (t) {
                return null != t
            })
        }

        function H(t) {
            return t.length > 0 ? r.fn.concat.apply([], t) : t
        }

        function I(t) {
            return t.replace(/::/g, "/").replace(/([A-Z]+)([A-Z][a-z])/g, "$1_$2").replace(/([a-z\d])([A-Z])/g, "$1_$2").replace(/_/g, "-").toLowerCase()
        }

        function V(t) {
            return t in l ? l[t] : l[t] = new RegExp("(^|\\s)" + t + "(\\s|$)")
        }

        function _(t, e) {
            return "number" != typeof e || h[I(t)] ? e : e + "px"
        }

        function B(t) {
            var e, n;
            return c[t] || (e = f.createElement(t), f.body.appendChild(e), n = getComputedStyle(e, "").getPropertyValue("display"), e.parentNode.removeChild(e), "none" == n && (n = "block"), c[t] = n), c[t]
        }

        function U(t) {
            return "children" in t ? u.call(t.children) : r.map(t.childNodes, function (t) {
                return 1 == t.nodeType ? t : void 0
            })
        }

        function X(t, e) {
            var n, r = t ? t.length : 0;
            for (n = 0; r > n; n++) this[n] = t[n];
            this.length = r, this.selector = e || ""
        }

        function J(t, r, i) {
            for (n in r) i && (Z(r[n]) || L(r[n])) ? (Z(r[n]) && !Z(t[n]) && (t[n] = {}), L(r[n]) && !L(t[n]) && (t[n] = []), J(t[n], r[n], i)) : r[n] !== e && (t[n] = r[n])
        }

        function W(t, e) {
            return null == e ? r(t) : r(t).filter(e)
        }

        function Y(t, e, n, r) {
            return F(e) ? e.call(t, n, r) : e
        }

        function G(t, e, n) {
            null == n ? t.removeAttribute(e) : t.setAttribute(e, n)
        }

        function K(t, n) {
            var r = t.className || "", i = r && r.baseVal !== e;
            return n === e ? i ? r.baseVal : r : void (i ? r.baseVal = n : t.className = n)
        }

        function Q(t) {
            try {
                return t ? "true" == t || ("false" == t ? !1 : "null" == t ? null : +t + "" == t ? +t : /^[\[\{]/.test(t) ? r.parseJSON(t) : t) : t
            } catch (e) {
                return t
            }
        }

        function tt(t, e) {
            e(t);
            for (var n = 0, r = t.childNodes.length; r > n; n++) tt(t.childNodes[n], e)
        }

        var e, n, r, i, O, P, o = [], s = o.concat, a = o.filter, u = o.slice, f = t.document, c = {}, l = {},
            h = {"column-count": 1, columns: 1, "font-weight": 1, "line-height": 1, opacity: 1, "z-index": 1, zoom: 1},
            p = /^\s*<(\w+|!)[^>]*>/, d = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            m = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, g = /^(?:body|html)$/i,
            v = /([A-Z])/g, y = ["val", "css", "html", "text", "data", "width", "height", "offset"],
            x = ["after", "prepend", "before", "append"], b = f.createElement("table"), E = f.createElement("tr"),
            j = {tr: f.createElement("tbody"), tbody: b, thead: b, tfoot: b, td: E, th: E, "*": f.createElement("div")},
            w = /complete|loaded|interactive/, T = /^[\w-]*$/, S = {}, C = S.toString, N = {},
            A = f.createElement("div"), D = {
                tabindex: "tabIndex",
                readonly: "readOnly",
                "for": "htmlFor",
                "class": "className",
                maxlength: "maxLength",
                cellspacing: "cellSpacing",
                cellpadding: "cellPadding",
                rowspan: "rowSpan",
                colspan: "colSpan",
                usemap: "useMap",
                frameborder: "frameBorder",
                contenteditable: "contentEditable"
            }, L = Array.isArray || function (t) {
                return t instanceof Array
            };
        return N.matches = function (t, e) {
            if (!e || !t || 1 !== t.nodeType) return !1;
            var n = t.matches || t.webkitMatchesSelector || t.mozMatchesSelector || t.oMatchesSelector || t.matchesSelector;
            if (n) return n.call(t, e);
            var r, i = t.parentNode, o = !i;
            return o && (i = A).appendChild(t), r = ~N.qsa(i, e).indexOf(t), o && A.removeChild(t), r
        }, O = function (t) {
            return t.replace(/-+(.)?/g, function (t, e) {
                return e ? e.toUpperCase() : ""
            })
        }, P = function (t) {
            return a.call(t, function (e, n) {
                return t.indexOf(e) == n
            })
        }, N.fragment = function (t, n, i) {
            var o, s, a;
            return d.test(t) && (o = r(f.createElement(RegExp.$1))), o || (t.replace && (t = t.replace(m, "<$1></$2>")), n === e && (n = p.test(t) && RegExp.$1), n in j || (n = "*"), a = j[n], a.innerHTML = "" + t, o = r.each(u.call(a.childNodes), function () {
                a.removeChild(this)
            })), Z(i) && (s = r(o), r.each(i, function (t, e) {
                y.indexOf(t) > -1 ? s[t](e) : s.attr(t, e)
            })), o
        }, N.Z = function (t, e) {
            return new X(t, e)
        }, N.isZ = function (t) {
            return t instanceof N.Z
        }, N.init = function (t, n) {
            var i;
            if (!t) return N.Z();
            if ("string" == typeof t) if (t = t.trim(), "<" == t[0] && p.test(t)) i = N.fragment(t, RegExp.$1, n), t = null; else {
                if (n !== e) return r(n).find(t);
                i = N.qsa(f, t)
            } else {
                if (F(t)) return r(f).ready(t);
                if (N.isZ(t)) return t;
                if (L(t)) i = q(t); else if (R(t)) i = [t], t = null; else if (p.test(t)) i = N.fragment(t.trim(), RegExp.$1, n), t = null; else {
                    if (n !== e) return r(n).find(t);
                    i = N.qsa(f, t)
                }
            }
            return N.Z(i, t)
        }, r = function (t, e) {
            return N.init(t, e)
        }, r.extend = function (t) {
            var e, n = u.call(arguments, 1);
            return "boolean" == typeof t && (e = t, t = n.shift()), n.forEach(function (n) {
                J(t, n, e)
            }), t
        }, N.qsa = function (t, e) {
            var n, r = "#" == e[0], i = !r && "." == e[0], o = r || i ? e.slice(1) : e, s = T.test(o);
            return t.getElementById && s && r ? (n = t.getElementById(o)) ? [n] : [] : 1 !== t.nodeType && 9 !== t.nodeType && 11 !== t.nodeType ? [] : u.call(s && !r && t.getElementsByClassName ? i ? t.getElementsByClassName(o) : t.getElementsByTagName(e) : t.querySelectorAll(e))
        }, r.contains = f.documentElement.contains ? function (t, e) {
            return t !== e && t.contains(e)
        } : function (t, e) {
            for (; e && (e = e.parentNode);) if (e === t) return !0;
            return !1
        }, r.type = $, r.isFunction = F, r.isWindow = k, r.isArray = L, r.isPlainObject = Z, r.isEmptyObject = function (t) {
            var e;
            for (e in t) return !1;
            return !0
        }, r.isNumeric = function (t) {
            var e = Number(t), n = typeof t;
            return null != t && "boolean" != n && ("string" != n || t.length) && !isNaN(e) && isFinite(e) || !1
        }, r.inArray = function (t, e, n) {
            return o.indexOf.call(e, t, n)
        }, r.camelCase = O, r.trim = function (t) {
            return null == t ? "" : String.prototype.trim.call(t)
        }, r.uuid = 0, r.support = {}, r.expr = {}, r.noop = function () {
        }, r.map = function (t, e) {
            var n, i, o, r = [];
            if (z(t)) for (i = 0; i < t.length; i++) n = e(t[i], i), null != n && r.push(n); else for (o in t) n = e(t[o], o), null != n && r.push(n);
            return H(r)
        }, r.each = function (t, e) {
            var n, r;
            if (z(t)) {
                for (n = 0; n < t.length; n++) if (e.call(t[n], n, t[n]) === !1) return t
            } else for (r in t) if (e.call(t[r], r, t[r]) === !1) return t;
            return t
        }, r.grep = function (t, e) {
            return a.call(t, e)
        }, t.JSON && (r.parseJSON = JSON.parse), r.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (t, e) {
            S["[object " + e + "]"] = e.toLowerCase()
        }), r.fn = {
            constructor: N.Z,
            length: 0,
            forEach: o.forEach,
            reduce: o.reduce,
            push: o.push,
            sort: o.sort,
            splice: o.splice,
            indexOf: o.indexOf,
            concat: function () {
                var t, e, n = [];
                for (t = 0; t < arguments.length; t++) e = arguments[t], n[t] = N.isZ(e) ? e.toArray() : e;
                return s.apply(N.isZ(this) ? this.toArray() : this, n)
            },
            map: function (t) {
                return r(r.map(this, function (e, n) {
                    return t.call(e, n, e)
                }))
            },
            slice: function () {
                return r(u.apply(this, arguments))
            },
            ready: function (t) {
                return w.test(f.readyState) && f.body ? t(r) : f.addEventListener("DOMContentLoaded", function () {
                    t(r)
                }, !1), this
            },
            get: function (t) {
                return t === e ? u.call(this) : this[t >= 0 ? t : t + this.length]
            },
            toArray: function () {
                return this.get()
            },
            size: function () {
                return this.length
            },
            remove: function () {
                return this.each(function () {
                    null != this.parentNode && this.parentNode.removeChild(this)
                })
            },
            each: function (t) {
                return o.every.call(this, function (e, n) {
                    return t.call(e, n, e) !== !1
                }), this
            },
            filter: function (t) {
                return F(t) ? this.not(this.not(t)) : r(a.call(this, function (e) {
                    return N.matches(e, t)
                }))
            },
            add: function (t, e) {
                return r(P(this.concat(r(t, e))))
            },
            is: function (t) {
                return this.length > 0 && N.matches(this[0], t)
            },
            not: function (t) {
                var n = [];
                if (F(t) && t.call !== e) this.each(function (e) {
                    t.call(this, e) || n.push(this)
                }); else {
                    var i = "string" == typeof t ? this.filter(t) : z(t) && F(t.item) ? u.call(t) : r(t);
                    this.forEach(function (t) {
                        i.indexOf(t) < 0 && n.push(t)
                    })
                }
                return r(n)
            },
            has: function (t) {
                return this.filter(function () {
                    return R(t) ? r.contains(this, t) : r(this).find(t).size()
                })
            },
            eq: function (t) {
                return -1 === t ? this.slice(t) : this.slice(t, +t + 1)
            },
            first: function () {
                var t = this[0];
                return t && !R(t) ? t : r(t)
            },
            last: function () {
                var t = this[this.length - 1];
                return t && !R(t) ? t : r(t)
            },
            find: function (t) {
                var e, n = this;
                return e = t ? "object" == typeof t ? r(t).filter(function () {
                    var t = this;
                    return o.some.call(n, function (e) {
                        return r.contains(e, t)
                    })
                }) : 1 == this.length ? r(N.qsa(this[0], t)) : this.map(function () {
                    return N.qsa(this, t)
                }) : r()
            },
            closest: function (t, e) {
                var n = [], i = "object" == typeof t && r(t);
                return this.each(function (r, o) {
                    for (; o && !(i ? i.indexOf(o) >= 0 : N.matches(o, t));) o = o !== e && !M(o) && o.parentNode;
                    o && n.indexOf(o) < 0 && n.push(o)
                }), r(n)
            },
            parents: function (t) {
                for (var e = [], n = this; n.length > 0;) n = r.map(n, function (t) {
                    return (t = t.parentNode) && !M(t) && e.indexOf(t) < 0 ? (e.push(t), t) : void 0
                });
                return W(e, t)
            },
            parent: function (t) {
                return W(P(this.pluck("parentNode")), t)
            },
            children: function (t) {
                return W(this.map(function () {
                    return U(this)
                }), t)
            },
            contents: function () {
                return this.map(function () {
                    return this.contentDocument || u.call(this.childNodes)
                })
            },
            siblings: function (t) {
                return W(this.map(function (t, e) {
                    return a.call(U(e.parentNode), function (t) {
                        return t !== e
                    })
                }), t)
            },
            empty: function () {
                return this.each(function () {
                    this.innerHTML = ""
                })
            },
            pluck: function (t) {
                return r.map(this, function (e) {
                    return e[t]
                })
            },
            show: function () {
                return this.each(function () {
                    "none" == this.style.display && (this.style.display = ""), "none" == getComputedStyle(this, "").getPropertyValue("display") && (this.style.display = B(this.nodeName))
                })
            },
            replaceWith: function (t) {
                return this.before(t).remove()
            },
            wrap: function (t) {
                var e = F(t);
                if (this[0] && !e) var n = r(t).get(0), i = n.parentNode || this.length > 1;
                return this.each(function (o) {
                    r(this).wrapAll(e ? t.call(this, o) : i ? n.cloneNode(!0) : n)
                })
            },
            wrapAll: function (t) {
                if (this[0]) {
                    r(this[0]).before(t = r(t));
                    for (var e; (e = t.children()).length;) t = e.first();
                    r(t).append(this)
                }
                return this
            },
            wrapInner: function (t) {
                var e = F(t);
                return this.each(function (n) {
                    var i = r(this), o = i.contents(), s = e ? t.call(this, n) : t;
                    o.length ? o.wrapAll(s) : i.append(s)
                })
            },
            unwrap: function () {
                return this.parent().each(function () {
                    r(this).replaceWith(r(this).children())
                }), this
            },
            clone: function () {
                return this.map(function () {
                    return this.cloneNode(!0)
                })
            },
            hide: function () {
                return this.css("display", "none")
            },
            toggle: function (t) {
                return this.each(function () {
                    var n = r(this);
                    (t === e ? "none" == n.css("display") : t) ? n.show() : n.hide()
                })
            },
            prev: function (t) {
                return r(this.pluck("previousElementSibling")).filter(t || "*")
            },
            next: function (t) {
                return r(this.pluck("nextElementSibling")).filter(t || "*")
            },
            html: function (t) {
                return 0 in arguments ? this.each(function (e) {
                    var n = this.innerHTML;
                    r(this).empty().append(Y(this, t, e, n))
                }) : 0 in this ? this[0].innerHTML : null
            },
            text: function (t) {
                return 0 in arguments ? this.each(function (e) {
                    var n = Y(this, t, e, this.textContent);
                    this.textContent = null == n ? "" : "" + n
                }) : 0 in this ? this.pluck("textContent").join("") : null
            },
            attr: function (t, r) {
                var i;
                return "string" != typeof t || 1 in arguments ? this.each(function (e) {
                    if (1 === this.nodeType) if (R(t)) for (n in t) G(this, n, t[n]); else G(this, t, Y(this, r, e, this.getAttribute(t)))
                }) : 0 in this && 1 == this[0].nodeType && null != (i = this[0].getAttribute(t)) ? i : e
            },
            removeAttr: function (t) {
                return this.each(function () {
                    1 === this.nodeType && t.split(" ").forEach(function (t) {
                        G(this, t)
                    }, this)
                })
            },
            prop: function (t, e) {
                return t = D[t] || t, 1 in arguments ? this.each(function (n) {
                    this[t] = Y(this, e, n, this[t])
                }) : this[0] && this[0][t]
            },
            removeProp: function (t) {
                return t = D[t] || t, this.each(function () {
                    delete this[t]
                })
            },
            data: function (t, n) {
                var r = "data-" + t.replace(v, "-$1").toLowerCase(),
                    i = 1 in arguments ? this.attr(r, n) : this.attr(r);
                return null !== i ? Q(i) : e
            },
            val: function (t) {
                return 0 in arguments ? (null == t && (t = ""), this.each(function (e) {
                    this.value = Y(this, t, e, this.value)
                })) : this[0] && (this[0].multiple ? r(this[0]).find("option").filter(function () {
                    return this.selected
                }).pluck("value") : this[0].value)
            },
            offset: function (e) {
                if (e) return this.each(function (t) {
                    var n = r(this), i = Y(this, e, t, n.offset()), o = n.offsetParent().offset(),
                        s = {top: i.top - o.top, left: i.left - o.left};
                    "static" == n.css("position") && (s.position = "relative"), n.css(s)
                });
                if (!this.length) return null;
                if (f.documentElement !== this[0] && !r.contains(f.documentElement, this[0])) return {top: 0, left: 0};
                var n = this[0].getBoundingClientRect();
                return {
                    left: n.left + t.pageXOffset,
                    top: n.top + t.pageYOffset,
                    width: Math.round(n.width),
                    height: Math.round(n.height)
                }
            },
            css: function (t, e) {
                if (arguments.length < 2) {
                    var i = this[0];
                    if ("string" == typeof t) {
                        if (!i) return;
                        return i.style[O(t)] || getComputedStyle(i, "").getPropertyValue(t)
                    }
                    if (L(t)) {
                        if (!i) return;
                        var o = {}, s = getComputedStyle(i, "");
                        return r.each(t, function (t, e) {
                            o[e] = i.style[O(e)] || s.getPropertyValue(e)
                        }), o
                    }
                }
                var a = "";
                if ("string" == $(t)) e || 0 === e ? a = I(t) + ":" + _(t, e) : this.each(function () {
                    this.style.removeProperty(I(t))
                }); else for (n in t) t[n] || 0 === t[n] ? a += I(n) + ":" + _(n, t[n]) + ";" : this.each(function () {
                    this.style.removeProperty(I(n))
                });
                return this.each(function () {
                    this.style.cssText += ";" + a
                })
            },
            index: function (t) {
                return t ? this.indexOf(r(t)[0]) : this.parent().children().indexOf(this[0])
            },
            hasClass: function (t) {
                return t ? o.some.call(this, function (t) {
                    return this.test(K(t))
                }, V(t)) : !1
            },
            addClass: function (t) {
                return t ? this.each(function (e) {
                    if ("className" in this) {
                        i = [];
                        var n = K(this), o = Y(this, t, e, n);
                        o.split(/\s+/g).forEach(function (t) {
                            r(this).hasClass(t) || i.push(t)
                        }, this), i.length && K(this, n + (n ? " " : "") + i.join(" "))
                    }
                }) : this
            },
            removeClass: function (t) {
                return this.each(function (n) {
                    if ("className" in this) {
                        if (t === e) return K(this, "");
                        i = K(this), Y(this, t, n, i).split(/\s+/g).forEach(function (t) {
                            i = i.replace(V(t), " ")
                        }), K(this, i.trim())
                    }
                })
            },
            toggleClass: function (t, n) {
                return t ? this.each(function (i) {
                    var o = r(this), s = Y(this, t, i, K(this));
                    s.split(/\s+/g).forEach(function (t) {
                        (n === e ? !o.hasClass(t) : n) ? o.addClass(t) : o.removeClass(t)
                    })
                }) : this
            },
            scrollTop: function (t) {
                if (this.length) {
                    var n = "scrollTop" in this[0];
                    return t === e ? n ? this[0].scrollTop : this[0].pageYOffset : this.each(n ? function () {
                        this.scrollTop = t
                    } : function () {
                        this.scrollTo(this.scrollX, t)
                    })
                }
            },
            scrollLeft: function (t) {
                if (this.length) {
                    var n = "scrollLeft" in this[0];
                    return t === e ? n ? this[0].scrollLeft : this[0].pageXOffset : this.each(n ? function () {
                        this.scrollLeft = t
                    } : function () {
                        this.scrollTo(t, this.scrollY)
                    })
                }
            },
            position: function () {
                if (this.length) {
                    var t = this[0], e = this.offsetParent(), n = this.offset(),
                        i = g.test(e[0].nodeName) ? {top: 0, left: 0} : e.offset();
                    return n.top -= parseFloat(r(t).css("margin-top")) || 0, n.left -= parseFloat(r(t).css("margin-left")) || 0, i.top += parseFloat(r(e[0]).css("border-top-width")) || 0, i.left += parseFloat(r(e[0]).css("border-left-width")) || 0, {
                        top: n.top - i.top,
                        left: n.left - i.left
                    }
                }
            },
            offsetParent: function () {
                return this.map(function () {
                    for (var t = this.offsetParent || f.body; t && !g.test(t.nodeName) && "static" == r(t).css("position");) t = t.offsetParent;
                    return t
                })
            }
        }, r.fn.detach = r.fn.remove, ["width", "height"].forEach(function (t) {
            var n = t.replace(/./, function (t) {
                return t[0].toUpperCase()
            });
            r.fn[t] = function (i) {
                var o, s = this[0];
                return i === e ? k(s) ? s["inner" + n] : M(s) ? s.documentElement["scroll" + n] : (o = this.offset()) && o[t] : this.each(function (e) {
                    s = r(this), s.css(t, Y(this, i, e, s[t]()))
                })
            }
        }), x.forEach(function (n, i) {
            var o = i % 2;
            r.fn[n] = function () {
                var n, a, s = r.map(arguments, function (t) {
                    var i = [];
                    return n = $(t), "array" == n ? (t.forEach(function (t) {
                        return t.nodeType !== e ? i.push(t) : r.zepto.isZ(t) ? i = i.concat(t.get()) : void (i = i.concat(N.fragment(t)))
                    }), i) : "object" == n || null == t ? t : N.fragment(t)
                }), u = this.length > 1;
                return s.length < 1 ? this : this.each(function (e, n) {
                    a = o ? n : n.parentNode, n = 0 == i ? n.nextSibling : 1 == i ? n.firstChild : 2 == i ? n : null;
                    var c = r.contains(f.documentElement, a);
                    s.forEach(function (e) {
                        if (u) e = e.cloneNode(!0); else if (!a) return r(e).remove();
                        a.insertBefore(e, n), c && tt(e, function (e) {
                            if (!(null == e.nodeName || "SCRIPT" !== e.nodeName.toUpperCase() || e.type && "text/javascript" !== e.type || e.src)) {
                                var n = e.ownerDocument ? e.ownerDocument.defaultView : t;
                                n.eval.call(n, e.innerHTML)
                            }
                        })
                    })
                })
            }, r.fn[o ? n + "To" : "insert" + (i ? "Before" : "After")] = function (t) {
                return r(t)[n](this), this
            }
        }), N.Z.prototype = X.prototype = r.fn, N.uniq = P, N.deserializeValue = Q, r.zepto = N, r
    }();
    return t.Zepto = e, void 0 === t.$ && (t.$ = e), function (e) {
        function h(t) {
            return t._zid || (t._zid = n++)
        }

        function p(t, e, n, r) {
            if (e = d(e), e.ns) var i = m(e.ns);
            return (a[h(t)] || []).filter(function (t) {
                return t && (!e.e || t.e == e.e) && (!e.ns || i.test(t.ns)) && (!n || h(t.fn) === h(n)) && (!r || t.sel == r)
            })
        }

        function d(t) {
            var e = ("" + t).split(".");
            return {e: e[0], ns: e.slice(1).sort().join(" ")}
        }

        function m(t) {
            return new RegExp("(?:^| )" + t.replace(" ", " .* ?") + "(?: |$)")
        }

        function g(t, e) {
            return t.del && !f && t.e in c || !!e
        }

        function v(t) {
            return l[t] || f && c[t] || t
        }

        function y(t, n, i, o, s, u, f) {
            var c = h(t), p = a[c] || (a[c] = []);
            n.split(/\s/).forEach(function (n) {
                if ("ready" == n) return e(document).ready(i);
                var a = d(n);
                a.fn = i, a.sel = s, a.e in l && (i = function (t) {
                    var n = t.relatedTarget;
                    return !n || n !== this && !e.contains(this, n) ? a.fn.apply(this, arguments) : void 0
                }), a.del = u;
                var c = u || i;
                a.proxy = function (e) {
                    if (e = T(e), !e.isImmediatePropagationStopped()) {
                        e.data = o;
                        var n = c.apply(t, e._args == r ? [e] : [e].concat(e._args));
                        return n === !1 && (e.preventDefault(), e.stopPropagation()), n
                    }
                }, a.i = p.length, p.push(a), "addEventListener" in t && t.addEventListener(v(a.e), a.proxy, g(a, f))
            })
        }

        function x(t, e, n, r, i) {
            var o = h(t);
            (e || "").split(/\s/).forEach(function (e) {
                p(t, e, n, r).forEach(function (e) {
                    delete a[o][e.i], "removeEventListener" in t && t.removeEventListener(v(e.e), e.proxy, g(e, i))
                })
            })
        }

        function T(t, n) {
            return (n || !t.isDefaultPrevented) && (n || (n = t), e.each(w, function (e, r) {
                var i = n[e];
                t[e] = function () {
                    return this[r] = b, i && i.apply(n, arguments)
                }, t[r] = E
            }), t.timeStamp || (t.timeStamp = Date.now()), (n.defaultPrevented !== r ? n.defaultPrevented : "returnValue" in n ? n.returnValue === !1 : n.getPreventDefault && n.getPreventDefault()) && (t.isDefaultPrevented = b)), t
        }

        function S(t) {
            var e, n = {originalEvent: t};
            for (e in t) j.test(e) || t[e] === r || (n[e] = t[e]);
            return T(n, t)
        }

        var r, n = 1, i = Array.prototype.slice, o = e.isFunction, s = function (t) {
                return "string" == typeof t
            }, a = {}, u = {}, f = "onfocusin" in t, c = {focus: "focusin", blur: "focusout"},
            l = {mouseenter: "mouseover", mouseleave: "mouseout"};
        u.click = u.mousedown = u.mouseup = u.mousemove = "MouseEvents", e.event = {
            add: y,
            remove: x
        }, e.proxy = function (t, n) {
            var r = 2 in arguments && i.call(arguments, 2);
            if (o(t)) {
                var a = function () {
                    return t.apply(n, r ? r.concat(i.call(arguments)) : arguments)
                };
                return a._zid = h(t), a
            }
            if (s(n)) return r ? (r.unshift(t[n], t), e.proxy.apply(null, r)) : e.proxy(t[n], t);
            throw new TypeError("expected function")
        }, e.fn.bind = function (t, e, n) {
            return this.on(t, e, n)
        }, e.fn.unbind = function (t, e) {
            return this.off(t, e)
        }, e.fn.one = function (t, e, n, r) {
            return this.on(t, e, n, r, 1)
        };
        var b = function () {
            return !0
        }, E = function () {
            return !1
        }, j = /^([A-Z]|returnValue$|layer[XY]$|webkitMovement[XY]$)/, w = {
            preventDefault: "isDefaultPrevented",
            stopImmediatePropagation: "isImmediatePropagationStopped",
            stopPropagation: "isPropagationStopped"
        };
        e.fn.delegate = function (t, e, n) {
            return this.on(e, t, n)
        }, e.fn.undelegate = function (t, e, n) {
            return this.off(e, t, n)
        }, e.fn.live = function (t, n) {
            return e(document.body).delegate(this.selector, t, n), this
        }, e.fn.die = function (t, n) {
            return e(document.body).undelegate(this.selector, t, n), this
        }, e.fn.on = function (t, n, a, u, f) {
            var c, l, h = this;
            return t && !s(t) ? (e.each(t, function (t, e) {
                h.on(t, n, a, e, f)
            }), h) : (s(n) || o(u) || u === !1 || (u = a, a = n, n = r), (u === r || a === !1) && (u = a, a = r), u === !1 && (u = E), h.each(function (r, o) {
                f && (c = function (t) {
                    return x(o, t.type, u), u.apply(this, arguments)
                }), n && (l = function (t) {
                    var r, s = e(t.target).closest(n, o).get(0);
                    return s && s !== o ? (r = e.extend(S(t), {
                        currentTarget: s,
                        liveFired: o
                    }), (c || u).apply(s, [r].concat(i.call(arguments, 1)))) : void 0
                }), y(o, t, u, a, n, l || c)
            }))
        }, e.fn.off = function (t, n, i) {
            var a = this;
            return t && !s(t) ? (e.each(t, function (t, e) {
                a.off(t, n, e)
            }), a) : (s(n) || o(i) || i === !1 || (i = n, n = r), i === !1 && (i = E), a.each(function () {
                x(this, t, i, n)
            }))
        }, e.fn.trigger = function (t, n) {
            return t = s(t) || e.isPlainObject(t) ? e.Event(t) : T(t), t._args = n, this.each(function () {
                t.type in c && "function" == typeof this[t.type] ? this[t.type]() : "dispatchEvent" in this ? this.dispatchEvent(t) : e(this).triggerHandler(t, n)
            })
        }, e.fn.triggerHandler = function (t, n) {
            var r, i;
            return this.each(function (o, a) {
                r = S(s(t) ? e.Event(t) : t), r._args = n, r.target = a, e.each(p(a, t.type || t), function (t, e) {
                    return i = e.proxy(r), r.isImmediatePropagationStopped() ? !1 : void 0
                })
            }), i
        }, "focusin focusout focus blur load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select keydown keypress keyup error".split(" ").forEach(function (t) {
            e.fn[t] = function (e) {
                return 0 in arguments ? this.bind(t, e) : this.trigger(t)
            }
        }), e.Event = function (t, e) {
            s(t) || (e = t, t = e.type);
            var n = document.createEvent(u[t] || "Events"), r = !0;
            if (e) for (var i in e) "bubbles" == i ? r = !!e[i] : n[i] = e[i];
            return n.initEvent(t, r, !0), T(n)
        }
    }(e), function (e) {
        function p(t, n, r) {
            var i = e.Event(n);
            return e(t).trigger(i, r), !i.isDefaultPrevented()
        }

        function d(t, e, n, i) {
            return t.global ? p(e || r, n, i) : void 0
        }

        function m(t) {
            t.global && 0 === e.active++ && d(t, null, "ajaxStart")
        }

        function g(t) {
            t.global && !--e.active && d(t, null, "ajaxStop")
        }

        function v(t, e) {
            var n = e.context;
            return e.beforeSend.call(n, t, e) === !1 || d(e, n, "ajaxBeforeSend", [t, e]) === !1 ? !1 : void d(e, n, "ajaxSend", [t, e])
        }

        function y(t, e, n, r) {
            var i = n.context, o = "success";
            n.success.call(i, t, o, e), r && r.resolveWith(i, [t, o, e]), d(n, i, "ajaxSuccess", [e, n, t]), b(o, e, n)
        }

        function x(t, e, n, r, i) {
            var o = r.context;
            r.error.call(o, n, e, t), i && i.rejectWith(o, [n, e, t]), d(r, o, "ajaxError", [n, r, t || e]), b(e, n, r)
        }

        function b(t, e, n) {
            var r = n.context;
            n.complete.call(r, e, t), d(n, r, "ajaxComplete", [e, n]), g(n)
        }

        function E(t, e, n) {
            if (n.dataFilter == j) return t;
            var r = n.context;
            return n.dataFilter.call(r, t, e)
        }

        function j() {
        }

        function w(t) {
            return t && (t = t.split(";", 2)[0]), t && (t == c ? "html" : t == f ? "json" : a.test(t) ? "script" : u.test(t) && "xml") || "text"
        }

        function T(t, e) {
            return "" == e ? t : (t + "&" + e).replace(/[&?]{1,2}/, "?")
        }

        function S(t) {
            t.processData && t.data && "string" != e.type(t.data) && (t.data = e.param(t.data, t.traditional)), !t.data || t.type && "GET" != t.type.toUpperCase() && "jsonp" != t.dataType || (t.url = T(t.url, t.data), t.data = void 0)
        }

        function C(t, n, r, i) {
            return e.isFunction(n) && (i = r, r = n, n = void 0), e.isFunction(r) || (i = r, r = void 0), {
                url: t,
                data: n,
                success: r,
                dataType: i
            }
        }

        function O(t, n, r, i) {
            var o, s = e.isArray(n), a = e.isPlainObject(n);
            e.each(n, function (n, u) {
                o = e.type(u), i && (n = r ? i : i + "[" + (a || "object" == o || "array" == o ? n : "") + "]"), !i && s ? t.add(u.name, u.value) : "array" == o || !r && "object" == o ? O(t, u, r, n) : t.add(n, u)
            })
        }

        var i, o, n = +new Date, r = t.document, s = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
            a = /^(?:text|application)\/javascript/i, u = /^(?:text|application)\/xml/i, f = "application/json",
            c = "text/html", l = /^\s*$/, h = r.createElement("a");
        h.href = t.location.href, e.active = 0, e.ajaxJSONP = function (i, o) {
            if (!("type" in i)) return e.ajax(i);
            var c, p, s = i.jsonpCallback, a = (e.isFunction(s) ? s() : s) || "Zepto" + n++,
                u = r.createElement("script"), f = t[a], l = function (t) {
                    e(u).triggerHandler("error", t || "abort")
                }, h = {abort: l};
            return o && o.promise(h), e(u).on("load error", function (n, r) {
                clearTimeout(p), e(u).off().remove(), "error" != n.type && c ? y(c[0], h, i, o) : x(null, r || "error", h, i, o), t[a] = f, c && e.isFunction(f) && f(c[0]), f = c = void 0
            }), v(h, i) === !1 ? (l("abort"), h) : (t[a] = function () {
                c = arguments
            }, u.src = i.url.replace(/\?(.+)=\?/, "?$1=" + a), r.head.appendChild(u), i.timeout > 0 && (p = setTimeout(function () {
                l("timeout")
            }, i.timeout)), h)
        }, e.ajaxSettings = {
            type: "GET",
            beforeSend: j,
            success: j,
            error: j,
            complete: j,
            context: null,
            global: !0,
            xhr: function () {
                return new t.XMLHttpRequest
            },
            accepts: {
                script: "text/javascript, application/javascript, application/x-javascript",
                json: f,
                xml: "application/xml, text/xml",
                html: c,
                text: "text/plain"
            },
            crossDomain: !1,
            timeout: 0,
            processData: !0,
            cache: !0,
            dataFilter: j
        }, e.ajax = function (n) {
            var u, f, s = e.extend({}, n || {}), a = e.Deferred && e.Deferred();
            for (i in e.ajaxSettings) void 0 === s[i] && (s[i] = e.ajaxSettings[i]);
            m(s), s.crossDomain || (u = r.createElement("a"), u.href = s.url, u.href = u.href, s.crossDomain = h.protocol + "//" + h.host != u.protocol + "//" + u.host), s.url || (s.url = t.location.toString()), (f = s.url.indexOf("#")) > -1 && (s.url = s.url.slice(0, f)), S(s);
            var c = s.dataType, p = /\?.+=\?/.test(s.url);
            if (p && (c = "jsonp"), s.cache !== !1 && (n && n.cache === !0 || "script" != c && "jsonp" != c) || (s.url = T(s.url, "_=" + Date.now())), "jsonp" == c) return p || (s.url = T(s.url, s.jsonp ? s.jsonp + "=?" : s.jsonp === !1 ? "" : "callback=?")), e.ajaxJSONP(s, a);
            var P, d = s.accepts[c], g = {}, b = function (t, e) {
                g[t.toLowerCase()] = [t, e]
            }, C = /^([\w-]+:)\/\//.test(s.url) ? RegExp.$1 : t.location.protocol, N = s.xhr(), O = N.setRequestHeader;
            if (a && a.promise(N), s.crossDomain || b("X-Requested-With", "XMLHttpRequest"), b("Accept", d || "*/*"), (d = s.mimeType || d) && (d.indexOf(",") > -1 && (d = d.split(",", 2)[0]), N.overrideMimeType && N.overrideMimeType(d)), (s.contentType || s.contentType !== !1 && s.data && "GET" != s.type.toUpperCase()) && b("Content-Type", s.contentType || "application/x-www-form-urlencoded"), s.headers) for (o in s.headers) b(o, s.headers[o]);
            if (N.setRequestHeader = b, N.onreadystatechange = function () {
                if (4 == N.readyState) {
                    N.onreadystatechange = j, clearTimeout(P);
                    var t, n = !1;
                    if (N.status >= 200 && N.status < 300 || 304 == N.status || 0 == N.status && "file:" == C) {
                        if (c = c || w(s.mimeType || N.getResponseHeader("content-type")), "arraybuffer" == N.responseType || "blob" == N.responseType) t = N.response; else {
                            t = N.responseText;
                            try {
                                t = E(t, c, s), "script" == c ? (1, eval)(t) : "xml" == c ? t = N.responseXML : "json" == c && (t = l.test(t) ? null : e.parseJSON(t))
                            } catch (r) {
                                n = r
                            }
                            if (n) return x(n, "parsererror", N, s, a)
                        }
                        y(t, N, s, a)
                    } else x(N.statusText || null, N.status ? "error" : "abort", N, s, a)
                }
            }, v(N, s) === !1) return N.abort(), x(null, "abort", N, s, a), N;
            var A = "async" in s ? s.async : !0;
            if (N.open(s.type, s.url, A, s.username, s.password), s.xhrFields) for (o in s.xhrFields) N[o] = s.xhrFields[o];
            for (o in g) O.apply(N, g[o]);
            return s.timeout > 0 && (P = setTimeout(function () {
                N.onreadystatechange = j, N.abort(), x(null, "timeout", N, s, a)
            }, s.timeout)), N.send(s.data ? s.data : null), N
        }, e.get = function () {
            return e.ajax(C.apply(null, arguments))
        }, e.post = function () {
            var t = C.apply(null, arguments);
            return t.type = "POST", e.ajax(t)
        }, e.getJSON = function () {
            var t = C.apply(null, arguments);
            return t.dataType = "json", e.ajax(t)
        }, e.fn.load = function (t, n, r) {
            if (!this.length) return this;
            var a, i = this, o = t.split(/\s/), u = C(t, n, r), f = u.success;
            return o.length > 1 && (u.url = o[0], a = o[1]), u.success = function (t) {
                i.html(a ? e("<div>").html(t.replace(s, "")).find(a) : t), f && f.apply(i, arguments)
            }, e.ajax(u), this
        };
        var N = encodeURIComponent;
        e.param = function (t, n) {
            var r = [];
            return r.add = function (t, n) {
                e.isFunction(n) && (n = n()), null == n && (n = ""), this.push(N(t) + "=" + N(n))
            }, O(r, t, n), r.join("&").replace(/%20/g, "+")
        }
    }(e), function (t) {
        t.fn.serializeArray = function () {
            var e, n, r = [], i = function (t) {
                return t.forEach ? t.forEach(i) : void r.push({name: e, value: t})
            };
            return this[0] && t.each(this[0].elements, function (r, o) {
                n = o.type, e = o.name, e && "fieldset" != o.nodeName.toLowerCase() && !o.disabled && "submit" != n && "reset" != n && "button" != n && "file" != n && ("radio" != n && "checkbox" != n || o.checked) && i(t(o).val())
            }), r
        }, t.fn.serialize = function () {
            var t = [];
            return this.serializeArray().forEach(function (e) {
                t.push(encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value))
            }), t.join("&")
        }, t.fn.submit = function (e) {
            if (0 in arguments) this.bind("submit", e); else if (this.length) {
                var n = t.Event("submit");
                this.eq(0).trigger(n), n.isDefaultPrevented() || this.get(0).submit()
            }
            return this
        }
    }(e), function () {
        try {
            getComputedStyle(void 0)
        } catch (e) {
            var n = getComputedStyle;
            t.getComputedStyle = function (t, e) {
                try {
                    return n(t, e)
                } catch (r) {
                    return null
                }
            }
        }
    }(), e
});

;!function ($) {
    var touch = {}, touchTimeout, tapTimeout, swipeTimeout, longTapTimeout, longlongTapTimeout, longTapDelay = 320,
        gesture, down, up, move, eventMap, initialized = false;

    function swipeDirection(x1, x2, y1, y2) {
        return Math.abs(x1 - x2) >= Math.abs(y1 - y2) ? (x1 - x2 > 0 ? 'Left' : 'Right') : (y1 - y2 > 0 ? 'Up' : 'Down');
    }

    function longTap() {
        longTapTimeout = null;
        if (touch.last) touch.el.trigger('longTap');
    }

    function longlongTap() {
        longlongTapTimeout = null;
        if (touch.last) {
            touch.el.trigger('longlongTap');
            touch = {};
        }
    }

    function cancelLongTap() {
        if (longTapTimeout) clearTimeout(longTapTimeout);
        if (longlongTapTimeout) clearTimeout(longlongTapTimeout);
        longTapTimeout = longlongTapTimeout = null;
    }

    function cancelAll() {
        if (touchTimeout) clearTimeout(touchTimeout);
        if (tapTimeout) clearTimeout(tapTimeout);
        if (swipeTimeout) clearTimeout(swipeTimeout);
        if (longTapTimeout) clearTimeout(longTapTimeout);
        if (longlongTapTimeout) clearTimeout(longlongTapTimeout);
        touchTimeout = tapTimeout = swipeTimeout = longTapTimeout = longlongTapTimeout = null;
        touch = {};
    }

    function isPrimaryTouch(event) {
        return (event.pointerType == 'touch' || event.pointerType == event.MSPOINTER_TYPE_TOUCH) && event.isPrimary;
    }

    function isPointerEventType(e, type) {
        return (e.type == 'pointer' + type || e.type.toLowerCase() == 'mspointer' + type);
    }

    function unregisterTouchEvents() {
        if (!initialized) return;
        $(document).off(eventMap.down, down).off(eventMap.up, up).off(eventMap.move, move).off(eventMap.cancel, cancelAll);
        $(window).off('scroll', cancelAll);
        cancelAll();
        initialized = false;
    }

    function setup(__eventMap) {
        var now, delta, deltaX = 0, deltaY = 0, firstTouch, _isPointerType;
        unregisterTouchEvents();
        eventMap = (__eventMap && ('down' in __eventMap))
            ? __eventMap
            : ('ontouchstart' in document
                ? {'down': 'touchstart', 'up': 'touchend', 'move': 'touchmove', 'cancel': 'touchcancel'}
                : 'onpointerdown' in document
                    ? {'down': 'pointerdown', 'up': 'pointerup', 'move': 'pointermove', 'cancel': 'pointercancel'}
                    : 'onmspointerdown' in document
                        ? {
                            'down': 'MSPointerDown',
                            'up': 'MSPointerUp',
                            'move': 'MSPointerMove',
                            'cancel': 'MSPointerCancel'
                        }
                        : false);
        if (!eventMap) return;
        if ('MSGesture' in window) {
            gesture = new MSGesture();
            gesture.target = document.body;
            $(document).bind('MSGestureEnd', function (e) {
                var swipeDirectionFromVelocity = e.velocityX > 1 ? 'Right' : e.velocityX < -1 ? 'Left' : e.velocityY > 1 ? 'Down' : e.velocityY < -1 ? 'Up' : null;
                if (swipeDirectionFromVelocity) {
                    touch.el.trigger('swipe');
                    touch.el.trigger('swipe' + swipeDirectionFromVelocity);
                }
            });
        }
        down = function (e) {
            if ((_isPointerType = isPointerEventType(e, 'down')) && !isPrimaryTouch(e)) return;
            firstTouch = (_isPointerType || !e.touches) ? e : e.touches[0];
            if (e.touches && e.touches.length === 1 && touch.x2) {
                touch.x2 = undefined;
                touch.y2 = undefined;
            }
            now = Date.now();
            delta = now - (touch.last || now);
            touch.el = $('tagName' in firstTouch.target ? firstTouch.target : firstTouch.target.parentNode);
            touchTimeout && clearTimeout(touchTimeout);
            touch.x1 = firstTouch.pageX;
            touch.y1 = firstTouch.pageY;
            if (delta > 0 && delta <= 250) touch.isDoubleTap = true;
            touch.last = now;
            longTapTimeout = setTimeout(longTap, longTapDelay);
            longlongTapTimeout = setTimeout(longlongTap, longTapDelay + 280);
            if (gesture && _isPointerType) gesture.addPointer(e.pointerId);
        };
        move = function (e) {
            if ((_isPointerType = isPointerEventType(e, 'move')) && !isPrimaryTouch(e)) return;
            firstTouch = _isPointerType || !e.touches ? e : e.touches[0];
            cancelLongTap();
            touch.x2 = firstTouch.pageX;
            touch.y2 = firstTouch.pageY;
            deltaX += Math.abs(touch.x1 - touch.x2);
            deltaY += Math.abs(touch.y1 - touch.y2);
        };
        up = function (e) {
            if ((_isPointerType = isPointerEventType(e, 'up')) && !isPrimaryTouch(e)) return;
            cancelLongTap();
            if ((touch.x2 && Math.abs(touch.x1 - touch.x2) > 30) || (touch.y2 && Math.abs(touch.y1 - touch.y2) > 30)) {
                swipeTimeout = setTimeout(function () {
                    if (touch.el) {
                        touch.el.trigger('swipe');
                        touch.el.trigger('swipe' + (swipeDirection(touch.x1, touch.x2, touch.y1, touch.y2)));
                    }
                    touch = {};
                }, 0);
            } else if ('last' in touch) {
                if (deltaX < 30 && deltaY < 30) {
                    tapTimeout = setTimeout(function () {
                        var event = $.Event('tap');
                        event.cancelTouch = cancelAll;
                        if (touch.el) touch.el.trigger(event);
                        if (touch.isDoubleTap) {
                            if (touch.el) touch.el.trigger('doubleTap');
                            touch = {};
                        } else {
                            touchTimeout = setTimeout(function () {
                                touchTimeout = null;
                                if (touch.el) touch.el.trigger('singleTap');
                                touch = {};
                            }, 250);
                        }
                    }, 0);
                } else touch = {};
            }
            deltaX = deltaY = 0;
        };
        $(document).on(eventMap.up, up).on(eventMap.down, down).on(eventMap.move, move).on(eventMap.cancel, cancelAll);
        $(window).on('scroll', cancelAll);
        initialized = true;
    };
    ['swipe', 'swipeLeft', 'swipeRight', 'swipeUp', 'swipeDown', 'doubleTap', 'tap', 'singleTap', 'longTap', 'longlongTap'].forEach(function (eventName) {
        $.fn[eventName] = function (callback) {
            return this.on(eventName, callback);
        }
    });
    $.touch = {setup: setup};
    $(document).ready(setup);
}(window.Zepto);


;!function (win, doc, $) {
    if (win.zkserStat) return;

    var func_weixin_spread = 1;
    var func_conversion_statistics = 4;
    var func_page_plug = 8;

    function initAjax() {
        $.ajaxSettings.type = "post";
        $.ajaxSettings.dataType = "json";
        $.ajaxSettings.crossDomain = true;

        $.singleAjax = function (option, sleep, ajaxOff) {
            if (!option.url) return;
            var token = option.url.toLowerCase().replace(/[^\w]/ig, "");
            if (w.requesting[token]) return;
            if (new Date().getTime() / 1000 - w.loadedTime > (ajaxOff || 1800)) return;
            w.requesting[token] = true;
            if (option.complete) option.completeCore = option.complete;
            option.complete = function (a, b) {
                setTimeout(function () {
                    w.requesting[token] = false;
                }, sleep || 1000);
                if (option.completeCore) option.completeCore(a, b);
            };
            $.ajax(option);
        };

        //$.ajaxSettings.xhrFields = { withCredentials: true };
        try {
            win.zkserStat = {
                loadScript: (url, callback) => loadScript(url, callback),
                initClipboard: (selector, text, callback) => initClipboard(selector, text, callback),
                getProtocol: () => getProtocol(),
                getWeixin: () => {
                    var wx = $.extend({}, w.zkserWx);
                    return wx;
                },
                isPhoneAvailable: (phone) => isPhoneAvailable(phone),
                isEmpty: (val) => isEmpty(val),
                getDevices: (userAgent) => getDevices(userAgent),
                setCookie: (key, val, expire) => setCookie(key, val, expire),
                getCookie: (key) => getCookie(key),
                clearCookie: (key) => clearCookie(key),
                tipsAlert: (text, callback) => tipsAlert(text, callback),
                pushCvtData: (typeid) => submitCvt({CvtId: typeid, count_type: 1, stat_type: 0, dim_select: 0}, null),
                jumpToWxapp: (lct) => jumpToWxapp(lct)
            }
        } catch (e) {
        }
    }

    function hasFunction(func) {
        return w.pageConfig && (w.pageConfig.FunctionType & func) > 0;
    }

    function first(array, filter) {
        for (var i = 0; i < array.length; i++) {
            if (filter(array[i])) return array[i];
        }
        return null;
    }

    function loadPageConfig() {
        if (hasFunction(func_weixin_spread | func_conversion_statistics)) submitVisite();
        if (hasFunction(func_page_plug)) getPlugin();
        if (w.isOriginal) $(win).bind("scroll", function () {
            resetOnlineTimer(700);
        });
    }

    function submitVisite() {
        $.singleAjax({
            url: w.apiUrlBase + "/Log/SubmitVisite",
            data: {
                uuid: w.page_uuid,
                session: w.session,
                isOriginal: w.isOriginal,
                url: w.href,
                referrer: w.referrer,
                zkCache: w.zkserWx ? w.zkserWx.Alias : null,
                zkShare: w.zkShare,
                searchEngine: w.searchEngine,
                searchWords: w.searchWords,
                browserName: w.browserName,
                browserVersion: w.browserVersion
            },
            success: function (c) {
                if (c.Ret !== 0) console.error(c.ErrMsg);
                if (c.Ret === -1000) $.singleAjax = function () {
                };
                w.dynamic = c.Data;
                setWeixin();
            },
            complete: function () {
                initCvtStat(true);
            }
        });
    }

    function setWeixin() {
        if (!cfg.WxCfg || !cfg.Weixins || !cfg.Weixins.length || !w.dynamic || !w.dynamic.WeixinId) return;
        var e = first(cfg.Weixins, (x) => x.Id == w.dynamic.WeixinId) || cfg.Weixins[0];
        if (w.dynamic.Weixin) $.extend(e, w.dynamic.Weixin);
        setWeixinCore(e);

        if (cfg.WxCfg.FixedShare && isEmpty(w.zkShare)) setUrlParam("alias", w.zkserWx.Alias);
        else if (!cfg.WxCfg.FixedShare && !isEmpty(w.zkShare)) delUrlParam("alias");

        if (cfg.WxCfg.FixedSession) setCookie("zkserWx_" + cfg.Id, JSON.stringify(w.zkserWx));
        else clearCookie("zkserWx_" + cfg.Id);
    }

    function setWeixinCore(e) {
        if (e.QrcodeUrl && e.QrcodeUrl.indexOf("://") < 0) e.QrcodeUrl = w.resUrlBase + e.QrcodeUrl;
        w.zkserWx = $.extend({}, e);

        $(doc).ready(function () {
            setWeixinHtml(true);
            $(".zkser-wx-icon").bind("click", function () {
                submitCvt(getConvertType(2));
                setTimeout(function () {
                    location.href = "weixin://";
                }, 100);
            });

            if (w.wxTimeHandler) clearInterval(w.wxTimeHandler);
            w.wxTimeHandler = setInterval(setWeixinHtml, 100);
        });

        try {
            if (typeof win.zkserStat.initWeixin === "function") {
                win.zkserStat.initWeixin($.extend({}, e));
            }
        } catch (e) {
        }
    }

    //修改页面上的微信内容
    function setWeixinHtml(first) {
        var count = setWeixinHtmlAttr(w.zkserWx, w.wxCount, "alias", "Alias", false, 1);
        count += setWeixinHtmlAttr(w.zkserWx, w.wxCount, "name", "Name");
        count += setWeixinHtmlAttr(w.zkserWx, w.wxCount, "sex", "Gender");
        count += setWeixinHtmlAttr(w.zkserWx, w.wxCount, "qq", "QQ", false, 1);
        count += setWeixinHtmlAttr(w.zkserWx, w.wxCount, "mobile", "PhoneNumber", false, 1);
        count += setWeixinHtmlAttr(w.zkserWx, w.wxCount, "qr", "QrcodeUrl", true, 3);
        if (count) initCvtStat(first);
    }

    function setWeixinHtmlAttr(cache, domLen, cls, attr, isSrc, cvt) {
        if (isEmpty(cache[attr])) return 0;
        var c = $(".zkser-wx-" + cls);
        if (c.length > domLen[attr]) {
            isSrc ? c.attr("src", cache[attr]) : c.html(cache[attr]);
            if (cvt) {
                c.attr("data-zkser-cvt", cvt);
                if (cvt == 3) {
                    $.each(c, function (a, b) {
                        initParentCvt(b, "img", cvt);
                    });
                }
            }
        }
        domLen[attr] = c.length;
        return 1;
    }

    function initParentCvt(a, t, cvt) {
        var isP = false, $prt = $(a);
        r1 = $prt.offset();
        while (true) {
            var $p = $prt.parent(), po = $p.offset();
            if (po && po.top <= r1.top && po.top + po.height >= r1.top && po.height <= r1.height * 2) isP = $prt = $p;
            else break;
        }
        if (isP) $prt.find(t).attr("data-zkser-cvt", cvt);
        else $(a).attr("data-zkser-cvt", cvt);
    }

    function submitOnline() {
        var isScroll = w.onlineTimeOut < 1000;
        resetOnlineTimer();
        if (w.isOriginal) {
            var depth = ($(win).scrollTop() + $(win).height()) / $(doc).height();
            depth = Math.min(100, parseInt(depth * 100 + 0.5));
            if (depth > w.browseDepth) w.browseDepth = depth;
            else if (isScroll) return;
        }
        $.singleAjax({
            url: w.apiUrlBase + "/Log/SubmitOnline",
            data: {
                uuid: w.page_uuid,
                session: w.session,
                depth: w.browseDepth
            }
        });
    }

    function initCvtStat(first) {
        if (!hasFunction(func_conversion_statistics)) return;
        $.each(w.converts, function (c, d) {
            switch (d.Trigger) {
                case 0:
                    $('[data-zkser-cvt="' + d.CvtId + '"]').unbind("click").on("click", function (e) {
                        debuglog("click cvtid:" + d.CvtId);
                        submitCvt(d, this);
                    });
                    break;
                case 1:
                    $('[data-zkser-cvt="' + d.CvtId + '"]').unbind("longlongTap").unbind("longTap").longlongTap(function (e) {
                        debuglog("longlongTap cvtid:" + d.CvtId);
                        submitCvt(d, this, null, 0);
                    }).longTap(function (e) {
                        debuglog("longTap cvtid:" + d.CvtId);
                        setTimeout(function () {
                            debuglog("longTap cvtid:" + d.CvtId + ", submiting...");
                            submitCvt(d, this, null, -1);
                        }, 500);
                    });

                    $("img:not([data-zkser-cvt])").unbind("longTap").longTap(function (e) {
                        var img = getShownQr();
                        if (!img) return;
                        debuglog("longTap cvtid:" + d.CvtId);
                        setTimeout(function () {
                            debuglog("longTap cvtid:" + d.CvtId + ", submiting...");
                            submitCvt(d, img, null, -1);
                        }, 500);
                    });
                    break;
                case 2:
                    $('[data-zkser-cvt="' + d.CvtId + '"]').unbind("copy").on("copy", function () {
                        submitCvt(d, this);
                        setTimeout(function () {
                            location.href = "weixin://";
                        }, 300);
                    });
                    break;
            }
        });
        if (!first) return;
        resetOnlineTimer();
        $(win).bind("beforeunload", submitOnline);
    }

    function getShownQr() {
        var wT = $(window).scrollTop(), wB = wT + $(window).height(), dom = null;
        $(".zkser-wx-qr").each(function (i, img) {
            var $o = $(img).offset(), qrMid = $o.top + $o.height / 2;
            if (!dom && qrMid > wT && qrMid < wB) dom = img;
        });
        return dom;
    }

    function getPlugin() {
        $.singleAjax({
            url: w.apiUrlBase + "/Log/GetPlugin",
            data: {uuid: w.page_uuid},
            success: function (a) {
                if (a.Ret === 0) {
                    $.each(a.Data, function (a, b) {
                        loadScript(b);
                    });
                }
            }
        })
    }

    function getConvertType(cvtId) {
        return first(w.converts, (c) => c.CvtId === cvtId);
    }

    function resetOnlineTimer(timeout) {
        if (!timeout) timeout = 20000;
        if (w.onlineTimeHandler) clearTimeout(w.onlineTimeHandler);
        w.onlineTimeHandler = setTimeout(submitOnline, timeout);
        w.onlineTimeOut = timeout;
    }

    function submitCvt(b, dom, lct, status) {
        resetOnlineTimer();
        if (!b) return;
        $.singleAjax({
            url: w.apiUrlBase + "/Log/SubmitTrack",
            data: {
                uuid: w.page_uuid,
                session: w.session,
                cvtId: b.CvtId,
                status: parseInt(status) || 0,
                lctId: parseInt(lct) || (dom ? parseInt($(dom).data("zkser-lct")) : null) || null
            },
            success: function (a) {
                debuglog("Track Submited:" + b.CvtId + ", status: " + (status || 0));
                if (a.Ret === 0) {
                    try {
                        if (a.Data && a.Data.WeixinId) {
                            if (!w.dynamic) w.dynamic = {};
                            w.dynamic.WeixinId = a.Data.WeixinId;
                            setWeixin();
                        } else if (a.Data && a.Data.jumpUrl) location.href = a.Data.jumpUrl;
                        if (typeof zkserStat.cvtCallback === "function") zkserStat.cvtCallback(b.CvtId);
                    } catch (e) {
                    }
                }
            }
        }, null, 7200);
    }

    function tipsAlert(a, b) {
        if (w.tipsalertShow) {
            var c = "<div id='tipsAlert' style='box-sizing: content-box;z-index:2147483647;width: 100%;position: fixed;top: 20%;left: 0;text-align: center'><div style='margin: 0;padding: 0;padding: 12px 25px;border: none;background:rgba(0,0,0,.6);min-width: 100px;display:inline-block;border-radius: 5px'>" + "<p style='width:100%;text-indent:0;margin: 0;padding: 0;line-height: 24px;font-size:15px;text-align: center;color: white;'>" + a + "</p></div></div>";
            $("body").append(c);
            w.tipsalertShow = false;
            setTimeout(function () {
                $("#tipsAlert").remove();
                w.tipsalertShow = true;
                if (b != null) {
                    b()
                }
            }, 2000)
        }
    }

    function jumpToWxapp(lct) {
        submitCvt(getConvertType(2), null, lct);
        $.singleAjax({
            url: w.apiUrlBase + "/Log/GetJumpWxapp",
            data: {session: w.session, uuid: w.page_uuid, weixinAlias: w.zkserWx.Alias},
            success: function (a) {
                if (a.Data) location.href = a.Data;
                else console.error(a.ErrMsg);
            }
        }, 1000, 7200);
        return false;
    }

    function initClipboard(a, b, c) {
        var d = new ClipboardJS(a, {
            text: function () {
                return b
            }
        });
        d.on('success', function (e) {
            if (c != null) {
                c(e.text)
            }
        });
        d.on('error', function (e) {
            if (!isEmpty(e.text) && e.text == b) {
                if (c != null) {
                    c(e.text)
                }
            }
            console.log("clipboard error:" + e.text)
        })
    }

    function setUrlParam(arg, val) {
        var strs = w.href.split("#");
        var url = strs[0] + (w.href.indexOf("?") == -1 ? "?" : "&") + arg + "=" + val + (strs[1] || "");
        history.replaceState ? history.replaceState({}, doc.title, url) : location.replace(url)
    }

    function delUrlParam(arg) {
        var a, b = location.search;
        if (b.indexOf(arg) != -1) {
            b = b.substring(1);
            var c = b.split('&');
            var d = [];
            for (var i = 0; i < c.length; i++) {
                var e = c[i].split('=');
                if (arg != e[0]) d.push(c[i]);
            }
            if (d.length == 0) a = location.pathname;
            else a = location.pathname + '?' + d.join('&');
        } else a = location.href;
        history.replaceState ? history.replaceState({}, doc.title, a) : location.replace(a)
    }

    function initBrowseName() {
        try {
            var f = w.userAgent.toLowerCase();
            if (f.match(/msie/) != null || f.match(/trident/) != null) {
                w.browserName = 1;
                w.browserVersion = getIeVersion()
            } else if (f.indexOf('bytedance') > 0) {
                if (f.indexOf('aweme_lite') > 0) w.browserName = 132;
                else if (f.indexOf('aweme') > 0) w.browserName = 131;
                else if (f.indexOf('videoarticle') > 0) w.browserName = 133;
                else w.browserName = 130;
                var v = f.match(/app_version\/([\d\.]+)/);
                w.browserVersion = v == null ? "" : v[1];
            } else if (f.match(/newsarticle.+newslite/ig)) {
                w.browserName = 134;
                var v = f.match(/newsarticle\/([\d\.]+)/);
                w.browserVersion = v == null ? "" : v[1];
            } else if (win.opera || (f.indexOf("opr") > 0)) {
                w.browserName = 10;
                w.browserVersion = getOperaVersion(f)
            } else if (f.match("ba?idubrowser")) {
                w.browserName = 20;
                var g = f.match(/ba?idubrowser\/([\d\.]+)/);
                w.browserVersion = g == null ? "" : g[1]
            } else if (f.indexOf("baiduboxapp") > 0) {
                w.browserName = 21;
                var h = f.match(/baiduboxapp\/([\d\.]+)/);
                w.browserVersion = h == null ? "" : h[1]
            } else if (f.indexOf("ubrowser") > 0) {
                w.browserName = 30;
                var i = f.match(/ubrowser\/([\d\.]+)/);
                w.browserVersion = i == null ? "" : i[1]
            } else if (f.indexOf("ucbrowser") > 0) {
                w.browserName = 31;
                var j = f.match(/ucbrowser\/([\d\.]+)/);
                w.browserVersion = j == null ? "" : j[1]
            } else if (f.indexOf("metasr") > 0 || f.indexOf("se 2.x") > 0) {
                w.browserName = 40;
                var k = f.match(/metasr\s([\d\.]+)/);
                w.browserVersion = k == null ? "" : k[1]
            } else if (f.indexOf("sogoumobilebrowser") > 0) {
                w.browserName = 41;
                var l = f.match(/sogoumobilebrowser\/([\d\.]+)/);
                w.browserVersion = l == null ? "" : l[1]
            } else if (f.indexOf("qqbrowser") > 0 || f.indexOf("micromessenger") > 0) {
                var m = f.match(/micromessenger\/([\d\.]+)/);
                var n = f.match(/qq\/([\d\.]+)/);
                var o = f.match(/tencenttraveler\/([\d\.]+)/);
                var p = f.match(/qqbrowser\/([\d\.]+)/);
                if (m != null) {
                    w.browserName = 53;
                    w.browserVersion = m[1]
                } else if (n != null) {
                    w.browserName = 52;
                    w.browserVersion = n[1]
                } else if (o != null) {
                    w.browserName = 51;
                    w.browserVersion = o
                } else if (p != null) {
                    w.browserName = 51;
                    w.browserVersion = p[1]
                } else {
                    w.browserName = 50
                }
            } else if (f.indexOf("qqnews") > 0) {
                w.browserName = 54;
                var q = f.match(/qqnews\/([\d\.]+)/);
                w.browserVersion = q == null ? "" : q[1]
            } else if (f.indexOf("maxthon") > 0) {
                w.browserName = 60;
                var q = f.match(/maxthon\/([\d\.]+)/);
                w.browserVersion = q == null ? "" : q[1]
            } else if (f.indexOf("firefox") > 0) {
                w.browserName = 70;
                var r = f.match(/firefox\/([\d\.]+)/);
                w.browserVersion = r == null ? "" : r[1]
            } else if (f.indexOf("edge") > 0) {
                w.browserName = 80;
                var s = f.match(/edge\/([\d\.]+)/);
                w.browserVersion = s == null ? "" : s[1]
            } else if (f.indexOf("qihoobrowser") > 0) {
                w.browserName = 90;
                var t = f.match(/qihoobrowser\/([\d\.]+)/);
                w.browserVersion = t == null ? "" : t[1]
            } else if (f.indexOf('weibo') > 0) {
                w.browserName = 120;
                var v = f.match(/weibo__([\d\.]+)/);
                w.browserVersion = v == null ? "" : v[1];
            } else if (f.indexOf('vivobrowser') > 0) {
                w.browserName = 140;
                var v = f.match(/vivobrowser\/?([\d\.]+)/);
                w.browserVersion = v == null ? "" : v[1];
            } else if (f.indexOf('miuibrowser') > 0) {
                w.browserName = 150;
                var v = f.match(/miuibrowser\/?([\d\.]+)/);
                w.browserVersion = v == null ? "" : v[1];
            } else if (f.indexOf('huaweibrowser') > 0) {
                w.browserName = 160;
                var v = f.match(/huaweibrowser\/?([\d\.]+)/);
                w.browserVersion = v == null ? "" : v[1];
            } else if (f.indexOf('heytapbrowser') > 0 || f.indexOf('oppo') > 0) {
                w.browserName = 170;
                var v = f.match(/heytapbrowser\/?([\d\.]+)/);
                w.browserVersion = v == null ? "" : v[1];
            } else if (f.indexOf("chrome") > 0) {
                if (f.indexOf("mobile") > 0) w.browserName = 101;
                else w.browserName = 100;
                var u = f.match(/chrome\/([\d\.]+)/);
                w.browserVersion = u == null ? "" : u[1]
            } else if (f.indexOf("safari") > 0) {
                w.browserName = 110;
                var v = f.match(/version\/([\d\.]+)/);
                w.browserVersion = v == null ? "" : v
            }
            w.browserVersion = w.browserVersion.length > 25 ? w.browserVersion.substring(0, 25) : w.browserVersion
        } catch (e) {
        }

        function getIeVersion() {
            var a = doc.documentMode;
            var b = /(msie\s|trident.*rv:)([\w.]+)/;
            var c = win.navigator.userAgent.toLowerCase();
            var d = b.exec(c);
            try {
                return d[2]
            } catch (e) {
                return a
            }
        }

        function getOperaVersion(a) {
            try {
                if (win.opera) {
                    return a.match(/opera.([\d.]+)/)[1]
                } else if (a.indexOf("opr") > 0) {
                    return a.match(/opr\/([\d.]+)/)[1]
                }
            } catch (e) {
                return 0
            }
        }
    }

    function partReferrer() {
        try {
            w.referrer = getCookie("zkRef_" + cfg.Id);
            if (isEmpty(w.referrer)) {
                if (!doc.referrer) return;
                w.referrer = doc.referrer;
                setCookie("zkRef_" + cfg.Id, encodeURI(w.referrer));
            }

            var a = w.referrer.split("?")[0];
            if (a.indexOf("google.com") != -1) {
                w.searchEngine = 1;
                w.searchWords = getQueryString(w.referrer, "q");
            } else if (a.indexOf("m.baidu.com") != -1) {
                w.searchEngine = 2;
                w.searchWords = getQueryString(w.referrer, "word");
            } else if (a.indexOf("baidu.com") != -1) {
                w.searchEngine = 2;
                w.searchWords = getQueryString(w.referrer, "wd") || getQueryString(w.referrer, "word");
            } else if (a.indexOf("so.com") != -1) {
                w.searchEngine = 3;
                w.searchWords = getQueryString(w.referrer, "q");
            } else if (a.indexOf("m.sogou") != -1 || a.indexOf("wap.sogou") != -1 || a.indexOf("zhishi.sogou") != -1 || a.indexOf("3g.sogou") != -1) {
                w.searchEngine = 4;
                w.searchWords = getQueryString(w.referrer, "keyword");
            } else if (a.indexOf("www.sogou") != -1) {
                w.searchEngine = 4;
                w.searchWords = getQueryString(w.referrer, "query");
            } else if (a.indexOf("sm.cn") != -1) {
                w.searchEngine = 5;
                w.searchWords = getQueryString(w.referrer, "q");
            } else if (a.indexOf("ifeng.com") != -1) {
                w.searchEngine = 6;
                w.searchWords = getQueryString(w.referrer, "q");
            } else if (a.indexOf("toutiao.eastday.com") != -1) {
                w.searchEngine = 7;
                w.searchWords = getQueryString(w.referrer, "kw");
            } else if (a.indexOf("toutiao.com") != -1) {
                w.searchEngine = 7;
                w.searchWords = getQueryString(w.referrer, "keyword");
            } else if (a.indexOf("bing.com") != -1) {
                w.searchEngine = 8;
                w.searchWords = getQueryString(w.referrer, "q");
            }
            if (!isEmpty(getQueryString(w.href, "se"))) {
                w.searchEngine = getQueryString(w.href, "se").split("#")[0]
            }
        } catch (e) {
        }
    }

    function debuglog(log, err) {
        if (!w.debug) return;
        if (!$(".debuglog").length) $(doc.body).append("<ul class='debuglog' style='position:fixed;top:10px;left:10px;right:10px;padding:10px;list-style:none;background-color:rgba(0,0,0,.8);color:white;word-break:break-all;white-space:pre-line;max-height:100px;overflow-y:auto;font-size:10pt;font-weight:normal;font-family:新宋体;line-height:20px;'></ul>");
        $(".debuglog").append("<li><span style='color:#FFB800;margin-right:10px;'>" + new Date().toTimeString().split(' ')[0] + "</span><span style='color:" + (err ? "#FF5722" : "#1E9FFF") + "'>" + log + "</span></li>").scrollTop(1000);
    }

    function getDevices(e) {
        if (!e) e = navigator.userAgent;
        var oss = [["HarmonyOS", "hos", 4], ["Windows NT 5.1", "winXP", 2], ["Windows NT 6.1", "win7", 2], ["Windows NT 6.0", "winVista", 2], ["Windows NT 6.2", "win8", 2], ["Windows NT 10.0", "win10", 2], ["iPad", "ios", 1], ["iPhone;", "ios", 1], ["iPod", "ios", 1], ["Macintosh", "mac", 2], ["Android", "Android", 1], ["Ubuntu", "ubuntu", 2], ["Linux", "linux", 1], ["Windows NT 5.2", "win2003", 2], ["Windows NT 5.0", "win2000", 2], ["Windows", "winOther", 2], ["rhino", "rhino", 3]];
        for (var i = 0, n = oss.length; i < n; ++i) if (e.indexOf(oss[i][0]) !== -1) return oss[i];
        return ["other", "other", 3]
    }

    function getQueryString(a, b) {
        try {
            var c = new RegExp("(^|&)" + b + "=([^&]*)(&|$)", "i");
            var d = a.split("?")[1];
            if (!isEmpty(d)) {
                return decodeURIComponent(d.match(c)[2])
            } else {
                return ""
            }
        } catch (err) {
        }
        try {
            var f = location.search.substring(1);
            var g = f.split("&");
            for (var i = 0; i < g.length; i++) {
                var h = g[i].split("=");
                if (h[0].toLowerCase() == b.toLowerCase()) {
                    return decodeURIComponent(h[1])
                }
            }
        } catch (e) {
        }
    }

    function guid() {
        return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16).toLowerCase();
        });
    }

    function setCookie(a, b, c) {
        c = isEmpty(c) ? (30 * 60 * 1000) : c;
        var d = new Date();
        d.setTime(d.getTime() + c);
        var e = "expires=" + d.toUTCString();
        doc.cookie = a + "=" + escape(b) + ";path=/; " + e
    }

    function getCookie(a) {
        var b = doc.cookie.match(new RegExp("(^| )" + a + "=([^;]*)(;|$)", "i"));
        if (!isEmpty(b)) {
            return unescape(b[2])
        } else {
            return null
        }
    }

    function clearCookie(a) {
        setCookie(a, "", -1)
    }

    function isEmpty(a) {
        if (typeof a == "undefined" || a == null || a == "null" || a === "") {
            return true
        } else {
            return false
        }
    }

    function isPhoneAvailable(a) {
        var b = /^1[3-9][0-9]{9}$/;
        if (!b.test(a)) {
            return false
        } else {
            return true
        }
    }

    function getProtocol() {
        try {
            if (location.protocol.indexOf("https") != -1) {
                return "https://"
            }
        } catch (e) {
        }
        return "http://"
    }

    function zformUrl(a) {
        a = a.toLowerCase().split('?')[0].split('#')[0];
        return a + (/\.([sp]?html?|php|jsp|aspx?|do|\/)$/ig.test(a) ? '' : '/');
    }

    function loadScript(b, c) {
        var a = doc.createElement('script');
        a.type = 'text/javascript';
        a.async = true;
        a.src = b;
        if (a.readyState) {
            a.onreadystatechange = function () {
                if (a.readyState == 'complete' || a.readyState == 'loaded') {
                    a.onreadystatechange = null;
                    if (typeof c === "function") c()
                }
            }
        } else {
            a.onload = function () {
                if (typeof c === "function") c()
            }
        }
        doc.head.appendChild(a);
        doc.head.removeChild(a);
    }

    var cfg = win.zkserPageCfg, w = {
        zkser_version: "v1.20.6.8",
        apiUrlBase: cfg.ApiDomain || (getProtocol() + "fans.jvit.top"),
        resUrlBase: cfg.ResDomain || (getProtocol() + "fans.jvit.top"),
        protocol: getProtocol(),
        href: location.href,
        referrer: doc.referrer,
        pageConfig: cfg,
        page_uuid: cfg.Token,
        devices_type: getDevices()[2],
        devices: getDevices()[1],
        userAgent: navigator.userAgent,
        browserName: 0,
        browserVersion: "",
        searchWords: "",
        searchEngine: 0,
        session: "",
        wxCount: {Alias: 0, Name: 0, Gender: 0, PhoneNumber: 0, QQ: 0, QrcodeUrl: 0},
        zkserWx: {Alias: "", Name: "", Gender: "", PhoneNumber: "", QQ: "", QrcodeUrl: "", RedirectUrl: ""},
        converts: [{CvtId: 1, Trigger: 2}, {CvtId: 2, Trigger: 0}, {CvtId: 3, Trigger: 1}],
        requesting: {},
        requestCount: {},
        zkShare: "",
        tipsalertShow: true,
        alertIsShow: true,
        loadedTime: new Date().getTime() / 1000,
        browseDepth: 0
    };
    console.log("%c欢迎使用《" + (cfg.Software || "智客云量") + "》推广系统%c" + (cfg.HomePage || "https://www.zkser.com") + "%c\r\n专业服务竞价推广客户，支持腾讯、头条、百度、快手、VIVO、OPPO、360、微博等主流平台，提供企微及个微真实加粉自动精准回传归因、开单深度回传、二维码自动轮播、精准控制单个微信加粉量，以及转化行为统计、条件跳转等更多专业功能。", "background-color:#0094ff;color:white;font-weight:bolder;font-size:14pt;padding:5px 10px;border-radius:5px", "font-size:14pt;margin-left:10px", "font-size:12pt;line-height:28px");
    if (isEmpty(w.page_uuid)) return;

    w.debug = getQueryString(w.href, "debug") == "true"
    w.plugsUrl = w.resUrlBase + "/js/plugs.js";
    w.alertUrl = w.resUrlBase + "/js/alerts.js";
    w.clipboardUrl = w.resUrlBase + "/js/clipboard.min.js";
    w.historyUrl = w.resUrlBase + "/js/history.min.js";
    w.simpleUrl = zformUrl(w.href);
    try {
        var cacheWx = getCookie("zkserWx_" + cfg.Id);
        setWeixinCore(JSON.parse(cacheWx));
    } catch (e) {
    }
    w.zkShare = getQueryString(w.href, "alias");
    w.session = getQueryString(w.href, "visitor");
    w.isOriginal = isEmpty(w.session);
    if (isEmpty(w.session)) w.session = getQueryString("gdt_vid") || getQueryString("qz_gdt");
    if (isEmpty(w.session)) w.session = getCookie("session" + cfg.Id);
    if (isEmpty(w.session)) {
        w.session = guid();
        setCookie("session" + cfg.Id, w.session)
    }

    initBrowseName();
    partReferrer();
    initAjax();
    loadPageConfig();
}(window, document, window.Zepto);
