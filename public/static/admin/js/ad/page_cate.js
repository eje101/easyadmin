define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'ad.page_cate/index',
        add_url: 'ad.page_cate/add',
        edit_url: 'ad.page_cate/edit',
        delete_url: 'ad.page_cate/delete',
        export_url: 'ad.page_cate/export',
        modify_url: 'ad.page_cate/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'ID',search:false},
                    {field: 'title', title: '分组名称'},
                    {field: 'remark', title: '备注信息',search:false, templet: ea.table.text},
                    {field: 'create_time', title: '添加时间',search: 'range'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});