define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'ad.page/index',
        add_url: 'ad.page/add',
        edit_url: 'ad.page/edit',
        delete_url: 'ad.page/delete',
        export_url: 'ad.page/export',
        modify_url: 'ad.page/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'ID',search: false},
                    {field: 'adPageCate.title', title: '分组名称',search: false},
                    {field: 'cate_id', title: '所属分组',hide:true,search: 'select',selectUrl:'ad.page_cate/index'},
                    {field: 'name', title: '页面名称'},
                    {field: 'url', title: '访问地址'},
                    {field: 'status', search: 'select', selectList: ["禁用","启用"], title: '状态', templet: ea.table.switch},
                    {field: 'create_time', title: '添加时间',search: 'range'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});