define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'ad.wechat/index',
        add_url: 'ad.wechat/add',
        edit_url: 'ad.wechat/edit',
        delete_url: 'ad.wechat/delete',
        export_url: 'ad.wechat/export',
        modify_url: 'ad.wechat/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'ID',search: false},
                    {field: 'adWechatCate.title', title: '分组名称',search: false},
                    {field: 'cate_id', title: '所属分组',hide:true,search: 'select',selectUrl:'ad.wechat_cate/index'},
                    {field: 'nickname', title: '微信昵称'},
                    {field: 'wechat_code', title: '微信号'},
                    {field: 'head_image', title: '微信头像', templet: ea.table.image,search: false},
                    {field: 'qrcode_image', title: '微信二维码', templet: ea.table.image,search: false},
                    {field: 'sex', search: 'select', selectList: {"1":"男","2":"女","0":"未知"}, title: '性别'},
                    {field: 'sort', title: '权重', edit: 'text',search: false},
                    {field: 'remark', title: '备注说明', templet: ea.table.text,search: false},
                    {field: 'status', search: 'select', selectList: ["禁用","启用"], title: '状态', templet: ea.table.switch},
                    {field: 'create_time', title: '创建时间',search: 'range'},
                    {width: 250, title: '操作',templet: ea.table.tool},
                ]]
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});