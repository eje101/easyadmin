!function (t, e) {
    "function" == typeof define && define.amd ? define(function () {
        return e(t)
    }) : e(t)
}(window, function (t) {
    var e = function () {
        function D(t) {
            return null == t ? String(t) : S[j.call(t)] || "object"
        }

        function A(t) {
            return "function" == D(t)
        }

        function $(t) {
            return null != t && t == t.window
        }

        function F(t) {
            return null != t && t.nodeType == t.DOCUMENT_NODE
        }

        function k(t) {
            return "object" == D(t)
        }

        function R(t) {
            return k(t) && !$(t) && Object.getPrototypeOf(t) == Object.prototype
        }

        function Z(t) {
            var e = !!t && "length" in t && t.length, n = i.type(t);
            return "function" != n && !$(t) && ("array" == n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
        }

        function _(t) {
            return a.call(t, function (t) {
                return null != t
            })
        }

        function z(t) {
            return t.length > 0 ? i.fn.concat.apply([], t) : t
        }

        function X(t) {
            return t.replace(/::/g, "/").replace(/([A-Z]+)([A-Z][a-z])/g, "$1_$2").replace(/([a-z\d])([A-Z])/g, "$1_$2").replace(/_/g, "-").toLowerCase()
        }

        function q(t) {
            return t in l ? l[t] : l[t] = new RegExp("(^|\\s)" + t + "(\\s|$)")
        }

        function H(t, e) {
            return "number" != typeof e || h[X(t)] ? e : e + "px"
        }

        function I(t) {
            var e, n;
            return c[t] || (e = f.createElement(t), f.body.appendChild(e), n = getComputedStyle(e, "").getPropertyValue("display"), e.parentNode.removeChild(e), "none" == n && (n = "block"), c[t] = n), c[t]
        }

        function U(t) {
            return "children" in t ? u.call(t.children) : i.map(t.childNodes, function (t) {
                return 1 == t.nodeType ? t : void 0
            })
        }

        function V(t, e) {
            var n, i = t ? t.length : 0;
            for (n = 0; i > n; n++) this[n] = t[n];
            this.length = i, this.selector = e || ""
        }

        function Y(t, i, r) {
            for (n in i) r && (R(i[n]) || L(i[n])) ? (R(i[n]) && !R(t[n]) && (t[n] = {}), L(i[n]) && !L(t[n]) && (t[n] = []), Y(t[n], i[n], r)) : i[n] !== e && (t[n] = i[n])
        }

        function B(t, e) {
            return null == e ? i(t) : i(t).filter(e)
        }

        function J(t, e, n, i) {
            return A(e) ? e.call(t, n, i) : e
        }

        function W(t, e, n) {
            null == n ? t.removeAttribute(e) : t.setAttribute(e, n)
        }

        function G(t, n) {
            var i = t.className || "", r = i && i.baseVal !== e;
            return n === e ? r ? i.baseVal : i : void (r ? i.baseVal = n : t.className = n)
        }

        function K(t) {
            try {
                return t ? "true" == t || ("false" == t ? !1 : "null" == t ? null : +t + "" == t ? +t : /^[\[\{]/.test(t) ? i.parseJSON(t) : t) : t
            } catch (e) {
                return t
            }
        }

        function Q(t, e) {
            e(t);
            for (var n = 0, i = t.childNodes.length; i > n; n++) Q(t.childNodes[n], e)
        }

        var e, n, i, r, N, P, o = [], s = o.concat, a = o.filter, u = o.slice, f = t.document, c = {}, l = {},
            h = {"column-count": 1, columns: 1, "font-weight": 1, "line-height": 1, opacity: 1, "z-index": 1, zoom: 1},
            p = /^\s*<(\w+|!)[^>]*>/, d = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            m = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, g = /^(?:body|html)$/i,
            y = /([A-Z])/g, v = ["val", "css", "html", "text", "data", "width", "height", "offset"],
            x = ["after", "prepend", "before", "append"], b = f.createElement("table"), w = f.createElement("tr"),
            E = {tr: f.createElement("tbody"), tbody: b, thead: b, tfoot: b, td: w, th: w, "*": f.createElement("div")},
            T = /^[\w-]*$/, S = {}, j = S.toString, C = {}, O = f.createElement("div"), M = {
                tabindex: "tabIndex",
                readonly: "readOnly",
                "for": "htmlFor",
                "class": "className",
                maxlength: "maxLength",
                cellspacing: "cellSpacing",
                cellpadding: "cellPadding",
                rowspan: "rowSpan",
                colspan: "colSpan",
                usemap: "useMap",
                frameborder: "frameBorder",
                contenteditable: "contentEditable"
            }, L = Array.isArray || function (t) {
                return t instanceof Array
            };
        return C.matches = function (t, e) {
            if (!e || !t || 1 !== t.nodeType) return !1;
            var n = t.matches || t.webkitMatchesSelector || t.mozMatchesSelector || t.oMatchesSelector || t.matchesSelector;
            if (n) return n.call(t, e);
            var i, r = t.parentNode, o = !r;
            return o && (r = O).appendChild(t), i = ~C.qsa(r, e).indexOf(t), o && O.removeChild(t), i
        }, N = function (t) {
            return t.replace(/-+(.)?/g, function (t, e) {
                return e ? e.toUpperCase() : ""
            })
        }, P = function (t) {
            return a.call(t, function (e, n) {
                return t.indexOf(e) == n
            })
        }, C.fragment = function (t, n, r) {
            var o, s, a;
            return d.test(t) && (o = i(f.createElement(RegExp.$1))), o || (t.replace && (t = t.replace(m, "<$1></$2>")), n === e && (n = p.test(t) && RegExp.$1), n in E || (n = "*"), a = E[n], a.innerHTML = "" + t, o = i.each(u.call(a.childNodes), function () {
                a.removeChild(this)
            })), R(r) && (s = i(o), i.each(r, function (t, e) {
                v.indexOf(t) > -1 ? s[t](e) : s.attr(t, e)
            })), o
        }, C.Z = function (t, e) {
            return new V(t, e)
        }, C.isZ = function (t) {
            return t instanceof C.Z
        }, C.init = function (t, n) {
            var r;
            if (!t) return C.Z();
            if ("string" == typeof t) if (t = t.trim(), "<" == t[0] && p.test(t)) r = C.fragment(t, RegExp.$1, n), t = null; else {
                if (n !== e) return i(n).find(t);
                r = C.qsa(f, t)
            } else {
                if (A(t)) return i(f).ready(t);
                if (C.isZ(t)) return t;
                if (L(t)) r = _(t); else if (k(t)) r = [t], t = null; else if (p.test(t)) r = C.fragment(t.trim(), RegExp.$1, n), t = null; else {
                    if (n !== e) return i(n).find(t);
                    r = C.qsa(f, t)
                }
            }
            return C.Z(r, t)
        }, i = function (t, e) {
            return C.init(t, e)
        }, i.extend = function (t) {
            var e, n = u.call(arguments, 1);
            return "boolean" == typeof t && (e = t, t = n.shift()), n.forEach(function (n) {
                Y(t, n, e)
            }), t
        }, C.qsa = function (t, e) {
            var n, i = "#" == e[0], r = !i && "." == e[0], o = i || r ? e.slice(1) : e, s = T.test(o);
            return t.getElementById && s && i ? (n = t.getElementById(o)) ? [n] : [] : 1 !== t.nodeType && 9 !== t.nodeType && 11 !== t.nodeType ? [] : u.call(s && !i && t.getElementsByClassName ? r ? t.getElementsByClassName(o) : t.getElementsByTagName(e) : t.querySelectorAll(e))
        }, i.contains = f.documentElement.contains ? function (t, e) {
            return t !== e && t.contains(e)
        } : function (t, e) {
            for (; e && (e = e.parentNode);) if (e === t) return !0;
            return !1
        }, i.type = D, i.isFunction = A, i.isWindow = $, i.isArray = L, i.isPlainObject = R, i.isEmptyObject = function (t) {
            var e;
            for (e in t) return !1;
            return !0
        }, i.isNumeric = function (t) {
            var e = Number(t), n = typeof t;
            return null != t && "boolean" != n && ("string" != n || t.length) && !isNaN(e) && isFinite(e) || !1
        }, i.inArray = function (t, e, n) {
            return o.indexOf.call(e, t, n)
        }, i.camelCase = N, i.trim = function (t) {
            return null == t ? "" : String.prototype.trim.call(t)
        }, i.uuid = 0, i.support = {}, i.expr = {}, i.noop = function () {
        }, i.map = function (t, e) {
            var n, r, o, i = [];
            if (Z(t)) for (r = 0; r < t.length; r++) n = e(t[r], r), null != n && i.push(n); else for (o in t) n = e(t[o], o), null != n && i.push(n);
            return z(i)
        }, i.each = function (t, e) {
            var n, i;
            if (Z(t)) {
                for (n = 0; n < t.length; n++) if (e.call(t[n], n, t[n]) === !1) return t
            } else for (i in t) if (e.call(t[i], i, t[i]) === !1) return t;
            return t
        }, i.grep = function (t, e) {
            return a.call(t, e)
        }, t.JSON && (i.parseJSON = JSON.parse), i.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (t, e) {
            S["[object " + e + "]"] = e.toLowerCase()
        }), i.fn = {
            constructor: C.Z,
            length: 0,
            forEach: o.forEach,
            reduce: o.reduce,
            push: o.push,
            sort: o.sort,
            splice: o.splice,
            indexOf: o.indexOf,
            concat: function () {
                var t, e, n = [];
                for (t = 0; t < arguments.length; t++) e = arguments[t], n[t] = C.isZ(e) ? e.toArray() : e;
                return s.apply(C.isZ(this) ? this.toArray() : this, n)
            },
            map: function (t) {
                return i(i.map(this, function (e, n) {
                    return t.call(e, n, e)
                }))
            },
            slice: function () {
                return i(u.apply(this, arguments))
            },
            ready: function (e) {
                if ("complete" === f.readyState || "loading" !== f.readyState && !f.documentElement.doScroll) setTimeout(function () {
                    e(i)
                }, 0); else {
                    var n = function () {
                        f.removeEventListener("DOMContentLoaded", n, !1), t.removeEventListener("load", n, !1), e(i)
                    };
                    f.addEventListener("DOMContentLoaded", n, !1), t.addEventListener("load", n, !1)
                }
                return this
            },
            get: function (t) {
                return t === e ? u.call(this) : this[t >= 0 ? t : t + this.length]
            },
            toArray: function () {
                return this.get()
            },
            size: function () {
                return this.length
            },
            remove: function () {
                return this.each(function () {
                    null != this.parentNode && this.parentNode.removeChild(this)
                })
            },
            each: function (t) {
                return o.every.call(this, function (e, n) {
                    return t.call(e, n, e) !== !1
                }), this
            },
            filter: function (t) {
                return A(t) ? this.not(this.not(t)) : i(a.call(this, function (e) {
                    return C.matches(e, t)
                }))
            },
            add: function (t, e) {
                return i(P(this.concat(i(t, e))))
            },
            is: function (t) {
                return "string" == typeof t ? this.length > 0 && C.matches(this[0], t) : t && this.selector == t.selector
            },
            not: function (t) {
                var n = [];
                if (A(t) && t.call !== e) this.each(function (e) {
                    t.call(this, e) || n.push(this)
                }); else {
                    var r = "string" == typeof t ? this.filter(t) : Z(t) && A(t.item) ? u.call(t) : i(t);
                    this.forEach(function (t) {
                        r.indexOf(t) < 0 && n.push(t)
                    })
                }
                return i(n)
            },
            has: function (t) {
                return this.filter(function () {
                    return k(t) ? i.contains(this, t) : i(this).find(t).size()
                })
            },
            eq: function (t) {
                return -1 === t ? this.slice(t) : this.slice(t, +t + 1)
            },
            first: function () {
                var t = this[0];
                return t && !k(t) ? t : i(t)
            },
            last: function () {
                var t = this[this.length - 1];
                return t && !k(t) ? t : i(t)
            },
            find: function (t) {
                var e, n = this;
                return e = t ? "object" == typeof t ? i(t).filter(function () {
                    var t = this;
                    return o.some.call(n, function (e) {
                        return i.contains(e, t)
                    })
                }) : 1 == this.length ? i(C.qsa(this[0], t)) : this.map(function () {
                    return C.qsa(this, t)
                }) : i()
            },
            closest: function (t, e) {
                var n = [], r = "object" == typeof t && i(t);
                return this.each(function (i, o) {
                    for (; o && !(r ? r.indexOf(o) >= 0 : C.matches(o, t));) o = o !== e && !F(o) && o.parentNode;
                    o && n.indexOf(o) < 0 && n.push(o)
                }), i(n)
            },
            parents: function (t) {
                for (var e = [], n = this; n.length > 0;) n = i.map(n, function (t) {
                    return (t = t.parentNode) && !F(t) && e.indexOf(t) < 0 ? (e.push(t), t) : void 0
                });
                return B(e, t)
            },
            parent: function (t) {
                return B(P(this.pluck("parentNode")), t)
            },
            children: function (t) {
                return B(this.map(function () {
                    return U(this)
                }), t)
            },
            contents: function () {
                return this.map(function () {
                    return this.contentDocument || u.call(this.childNodes)
                })
            },
            siblings: function (t) {
                return B(this.map(function (t, e) {
                    return a.call(U(e.parentNode), function (t) {
                        return t !== e
                    })
                }), t)
            },
            empty: function () {
                return this.each(function () {
                    this.innerHTML = ""
                })
            },
            pluck: function (t) {
                return i.map(this, function (e) {
                    return e[t]
                })
            },
            show: function () {
                return this.each(function () {
                    "none" == this.style.display && (this.style.display = ""), "none" == getComputedStyle(this, "").getPropertyValue("display") && (this.style.display = I(this.nodeName))
                })
            },
            replaceWith: function (t) {
                return this.before(t).remove()
            },
            wrap: function (t) {
                var e = A(t);
                if (this[0] && !e) var n = i(t).get(0), r = n.parentNode || this.length > 1;
                return this.each(function (o) {
                    i(this).wrapAll(e ? t.call(this, o) : r ? n.cloneNode(!0) : n)
                })
            },
            wrapAll: function (t) {
                if (this[0]) {
                    i(this[0]).before(t = i(t));
                    for (var e; (e = t.children()).length;) t = e.first();
                    i(t).append(this)
                }
                return this
            },
            wrapInner: function (t) {
                var e = A(t);
                return this.each(function (n) {
                    var r = i(this), o = r.contents(), s = e ? t.call(this, n) : t;
                    o.length ? o.wrapAll(s) : r.append(s)
                })
            },
            unwrap: function () {
                return this.parent().each(function () {
                    i(this).replaceWith(i(this).children())
                }), this
            },
            clone: function () {
                return this.map(function () {
                    return this.cloneNode(!0)
                })
            },
            hide: function () {
                return this.css("display", "none")
            },
            toggle: function (t) {
                return this.each(function () {
                    var n = i(this);
                    (t === e ? "none" == n.css("display") : t) ? n.show() : n.hide()
                })
            },
            prev: function (t) {
                return i(this.pluck("previousElementSibling")).filter(t || "*")
            },
            next: function (t) {
                return i(this.pluck("nextElementSibling")).filter(t || "*")
            },
            html: function (t) {
                return 0 in arguments ? this.each(function (e) {
                    var n = this.innerHTML;
                    i(this).empty().append(J(this, t, e, n))
                }) : 0 in this ? this[0].innerHTML : null
            },
            text: function (t) {
                return 0 in arguments ? this.each(function (e) {
                    var n = J(this, t, e, this.textContent);
                    this.textContent = null == n ? "" : "" + n
                }) : 0 in this ? this.pluck("textContent").join("") : null
            },
            attr: function (t, i) {
                var r;
                return "string" != typeof t || 1 in arguments ? this.each(function (e) {
                    if (1 === this.nodeType) if (k(t)) for (n in t) W(this, n, t[n]); else W(this, t, J(this, i, e, this.getAttribute(t)))
                }) : 0 in this && 1 == this[0].nodeType && null != (r = this[0].getAttribute(t)) ? r : e
            },
            removeAttr: function (t) {
                return this.each(function () {
                    1 === this.nodeType && t.split(" ").forEach(function (t) {
                        W(this, t)
                    }, this)
                })
            },
            prop: function (t, e) {
                return t = M[t] || t, "string" != typeof t || 1 in arguments ? this.each(function (i) {
                    if (k(t)) for (n in t) this[M[n] || n] = t[n]; else this[t] = J(this, e, i, this[t])
                }) : this[0] && this[0][t]
            },
            removeProp: function (t) {
                return t = M[t] || t, this.each(function () {
                    delete this[t]
                })
            },
            data: function (t, n) {
                var i = "data-" + t.replace(y, "-$1").toLowerCase(),
                    r = 1 in arguments ? this.attr(i, n) : this.attr(i);
                return null !== r ? K(r) : e
            },
            val: function (t) {
                return 0 in arguments ? (null == t && (t = ""), this.each(function (e) {
                    this.value = J(this, t, e, this.value)
                })) : this[0] && (this[0].multiple ? i(this[0]).find("option").filter(function () {
                    return this.selected
                }).pluck("value") : this[0].value)
            },
            offset: function (e) {
                if (e) return this.each(function (t) {
                    var n = i(this), r = J(this, e, t, n.offset()), o = n.offsetParent().offset(),
                        s = {top: r.top - o.top, left: r.left - o.left};
                    "static" == n.css("position") && (s.position = "relative"), n.css(s)
                });
                if (!this.length) return null;
                if (f.documentElement !== this[0] && !i.contains(f.documentElement, this[0])) return {top: 0, left: 0};
                var n = this[0].getBoundingClientRect();
                return {
                    left: n.left + t.pageXOffset,
                    top: n.top + t.pageYOffset,
                    width: Math.round(n.width),
                    height: Math.round(n.height)
                }
            },
            css: function (t, e) {
                if (arguments.length < 2) {
                    var r = this[0];
                    if ("string" == typeof t) {
                        if (!r) return;
                        return r.style[N(t)] || getComputedStyle(r, "").getPropertyValue(t)
                    }
                    if (L(t)) {
                        if (!r) return;
                        var o = {}, s = getComputedStyle(r, "");
                        return i.each(t, function (t, e) {
                            o[e] = r.style[N(e)] || s.getPropertyValue(e)
                        }), o
                    }
                }
                var a = "";
                if ("string" == D(t)) e || 0 === e ? a = X(t) + ":" + H(t, e) : this.each(function () {
                    this.style.removeProperty(X(t))
                }); else for (n in t) t[n] || 0 === t[n] ? a += X(n) + ":" + H(n, t[n]) + ";" : this.each(function () {
                    this.style.removeProperty(X(n))
                });
                return this.each(function () {
                    this.style.cssText += ";" + a
                })
            },
            index: function (t) {
                return t ? this.indexOf(i(t)[0]) : this.parent().children().indexOf(this[0])
            },
            hasClass: function (t) {
                return t ? o.some.call(this, function (t) {
                    return this.test(G(t))
                }, q(t)) : !1
            },
            addClass: function (t) {
                return t ? this.each(function (e) {
                    if ("className" in this) {
                        r = [];
                        var n = G(this), o = J(this, t, e, n);
                        o.split(/\s+/g).forEach(function (t) {
                            i(this).hasClass(t) || r.push(t)
                        }, this), r.length && G(this, n + (n ? " " : "") + r.join(" "))
                    }
                }) : this
            },
            removeClass: function (t) {
                return this.each(function (n) {
                    if ("className" in this) {
                        if (t === e) return G(this, "");
                        r = G(this), J(this, t, n, r).split(/\s+/g).forEach(function (t) {
                            r = r.replace(q(t), " ")
                        }), G(this, r.trim())
                    }
                })
            },
            toggleClass: function (t, n) {
                return t ? this.each(function (r) {
                    var o = i(this), s = J(this, t, r, G(this));
                    s.split(/\s+/g).forEach(function (t) {
                        (n === e ? !o.hasClass(t) : n) ? o.addClass(t) : o.removeClass(t)
                    })
                }) : this
            },
            scrollTop: function (t) {
                if (this.length) {
                    var n = "scrollTop" in this[0];
                    return t === e ? n ? this[0].scrollTop : this[0].pageYOffset : this.each(n ? function () {
                        this.scrollTop = t
                    } : function () {
                        this.scrollTo(this.scrollX, t)
                    })
                }
            },
            scrollLeft: function (t) {
                if (this.length) {
                    var n = "scrollLeft" in this[0];
                    return t === e ? n ? this[0].scrollLeft : this[0].pageXOffset : this.each(n ? function () {
                        this.scrollLeft = t
                    } : function () {
                        this.scrollTo(t, this.scrollY)
                    })
                }
            },
            position: function () {
                if (this.length) {
                    var t = this[0], e = this.offsetParent(), n = this.offset(),
                        r = g.test(e[0].nodeName) ? {top: 0, left: 0} : e.offset();
                    return n.top -= parseFloat(i(t).css("margin-top")) || 0, n.left -= parseFloat(i(t).css("margin-left")) || 0, r.top += parseFloat(i(e[0]).css("border-top-width")) || 0, r.left += parseFloat(i(e[0]).css("border-left-width")) || 0, {
                        top: n.top - r.top,
                        left: n.left - r.left
                    }
                }
            },
            offsetParent: function () {
                return this.map(function () {
                    for (var t = this.offsetParent || f.body; t && !g.test(t.nodeName) && "static" == i(t).css("position");) t = t.offsetParent;
                    return t
                })
            }
        }, i.fn.detach = i.fn.remove, ["width", "height"].forEach(function (t) {
            var n = t.replace(/./, function (t) {
                return t[0].toUpperCase()
            });
            i.fn[t] = function (r) {
                var o, s = this[0];
                return r === e ? $(s) ? s["inner" + n] : F(s) ? s.documentElement["scroll" + n] : (o = this.offset()) && o[t] : this.each(function (e) {
                    s = i(this), s.css(t, J(this, r, e, s[t]()))
                })
            }
        }), x.forEach(function (n, r) {
            var o = r % 2;
            i.fn[n] = function () {
                var n, a, s = i.map(arguments, function (t) {
                    var r = [];
                    return n = D(t), "array" == n ? (t.forEach(function (t) {
                        return t.nodeType !== e ? r.push(t) : i.zepto.isZ(t) ? r = r.concat(t.get()) : void (r = r.concat(C.fragment(t)))
                    }), r) : "object" == n || null == t ? t : C.fragment(t)
                }), u = this.length > 1;
                return s.length < 1 ? this : this.each(function (e, n) {
                    a = o ? n : n.parentNode, n = 0 == r ? n.nextSibling : 1 == r ? n.firstChild : 2 == r ? n : null;
                    var c = i.contains(f.documentElement, a);
                    s.forEach(function (e) {
                        if (u) e = e.cloneNode(!0); else if (!a) return i(e).remove();
                        a.insertBefore(e, n), c && Q(e, function (e) {
                            if (!(null == e.nodeName || "SCRIPT" !== e.nodeName.toUpperCase() || e.type && "text/javascript" !== e.type || e.src)) {
                                var n = e.ownerDocument ? e.ownerDocument.defaultView : t;
                                n.eval.call(n, e.innerHTML)
                            }
                        })
                    })
                })
            }, i.fn[o ? n + "To" : "insert" + (r ? "Before" : "After")] = function (t) {
                return i(t)[n](this), this
            }
        }), C.Z.prototype = V.prototype = i.fn, C.uniq = P, C.deserializeValue = K, i.zepto = C, i
    }();
    return t.Zepto = e, void 0 === t.$ && (t.$ = e), function (e) {
        function h(t) {
            return t._zid || (t._zid = n++)
        }

        function p(t, e, n, i) {
            if (e = d(e), e.ns) var r = m(e.ns);
            return (a[h(t)] || []).filter(function (t) {
                return t && (!e.e || t.e == e.e) && (!e.ns || r.test(t.ns)) && (!n || h(t.fn) === h(n)) && (!i || t.sel == i)
            })
        }

        function d(t) {
            var e = ("" + t).split(".");
            return {e: e[0], ns: e.slice(1).sort().join(" ")}
        }

        function m(t) {
            return new RegExp("(?:^| )" + t.replace(" ", " .* ?") + "(?: |$)")
        }

        function g(t, e) {
            return t.del && !f && t.e in c || !!e
        }

        function y(t) {
            return l[t] || f && c[t] || t
        }

        function v(t, n, r, o, s, u, f) {
            var c = h(t), p = a[c] || (a[c] = []);
            n.split(/\s/).forEach(function (n) {
                if ("ready" == n) return e(document).ready(r);
                var a = d(n);
                a.fn = r, a.sel = s, a.e in l && (r = function (t) {
                    var n = t.relatedTarget;
                    return !n || n !== this && !e.contains(this, n) ? a.fn.apply(this, arguments) : void 0
                }), a.del = u;
                var c = u || r;
                a.proxy = function (e) {
                    if (e = S(e), !e.isImmediatePropagationStopped()) {
                        e.data = o;
                        var n = c.apply(t, e._args == i ? [e] : [e].concat(e._args));
                        return n === !1 && (e.preventDefault(), e.stopPropagation()), n
                    }
                }, a.i = p.length, p.push(a), "addEventListener" in t && t.addEventListener(y(a.e), a.proxy, g(a, f))
            })
        }

        function x(t, e, n, i, r) {
            var o = h(t);
            (e || "").split(/\s/).forEach(function (e) {
                p(t, e, n, i).forEach(function (e) {
                    delete a[o][e.i], "removeEventListener" in t && t.removeEventListener(y(e.e), e.proxy, g(e, r))
                })
            })
        }

        function S(t, n) {
            if (n || !t.isDefaultPrevented) {
                n || (n = t), e.each(T, function (e, i) {
                    var r = n[e];
                    t[e] = function () {
                        return this[i] = b, r && r.apply(n, arguments)
                    }, t[i] = w
                });
                try {
                    t.timeStamp || (t.timeStamp = Date.now())
                } catch (r) {
                }
                (n.defaultPrevented !== i ? n.defaultPrevented : "returnValue" in n ? n.returnValue === !1 : n.getPreventDefault && n.getPreventDefault()) && (t.isDefaultPrevented = b)
            }
            return t
        }

        function j(t) {
            var e, n = {originalEvent: t};
            for (e in t) E.test(e) || t[e] === i || (n[e] = t[e]);
            return S(n, t)
        }

        var i, n = 1, r = Array.prototype.slice, o = e.isFunction, s = function (t) {
                return "string" == typeof t
            }, a = {}, u = {}, f = "onfocusin" in t, c = {focus: "focusin", blur: "focusout"},
            l = {mouseenter: "mouseover", mouseleave: "mouseout"};
        u.click = u.mousedown = u.mouseup = u.mousemove = "MouseEvents", e.event = {
            add: v,
            remove: x
        }, e.proxy = function (t, n) {
            var i = 2 in arguments && r.call(arguments, 2);
            if (o(t)) {
                var a = function () {
                    return t.apply(n, i ? i.concat(r.call(arguments)) : arguments)
                };
                return a._zid = h(t), a
            }
            if (s(n)) return i ? (i.unshift(t[n], t), e.proxy.apply(null, i)) : e.proxy(t[n], t);
            throw new TypeError("expected function")
        }, e.fn.bind = function (t, e, n) {
            return this.on(t, e, n)
        }, e.fn.unbind = function (t, e) {
            return this.off(t, e)
        }, e.fn.one = function (t, e, n, i) {
            return this.on(t, e, n, i, 1)
        };
        var b = function () {
            return !0
        }, w = function () {
            return !1
        }, E = /^([A-Z]|returnValue$|layer[XY]$|webkitMovement[XY]$)/, T = {
            preventDefault: "isDefaultPrevented",
            stopImmediatePropagation: "isImmediatePropagationStopped",
            stopPropagation: "isPropagationStopped"
        };
        e.fn.delegate = function (t, e, n) {
            return this.on(e, t, n)
        }, e.fn.undelegate = function (t, e, n) {
            return this.off(e, t, n)
        }, e.fn.live = function (t, n) {
            return e(document.body).delegate(this.selector, t, n), this
        }, e.fn.die = function (t, n) {
            return e(document.body).undelegate(this.selector, t, n), this
        }, e.fn.on = function (t, n, a, u, f) {
            var c, l, h = this;
            return t && !s(t) ? (e.each(t, function (t, e) {
                h.on(t, n, a, e, f)
            }), h) : (s(n) || o(u) || u === !1 || (u = a, a = n, n = i), (u === i || a === !1) && (u = a, a = i), u === !1 && (u = w), h.each(function (i, o) {
                f && (c = function (t) {
                    return x(o, t.type, u), u.apply(this, arguments)
                }), n && (l = function (t) {
                    var i, s = e(t.target).closest(n, o).get(0);
                    return s && s !== o ? (i = e.extend(j(t), {
                        currentTarget: s,
                        liveFired: o
                    }), (c || u).apply(s, [i].concat(r.call(arguments, 1)))) : void 0
                }), v(o, t, u, a, n, l || c)
            }))
        }, e.fn.off = function (t, n, r) {
            var a = this;
            return t && !s(t) ? (e.each(t, function (t, e) {
                a.off(t, n, e)
            }), a) : (s(n) || o(r) || r === !1 || (r = n, n = i), r === !1 && (r = w), a.each(function () {
                x(this, t, r, n)
            }))
        }, e.fn.trigger = function (t, n) {
            return t = s(t) || e.isPlainObject(t) ? e.Event(t) : S(t), t._args = n, this.each(function () {
                t.type in c && "function" == typeof this[t.type] ? this[t.type]() : "dispatchEvent" in this ? this.dispatchEvent(t) : e(this).triggerHandler(t, n)
            })
        }, e.fn.triggerHandler = function (t, n) {
            var i, r;
            return this.each(function (o, a) {
                i = j(s(t) ? e.Event(t) : t), i._args = n, i.target = a, e.each(p(a, t.type || t), function (t, e) {
                    return r = e.proxy(i), i.isImmediatePropagationStopped() ? !1 : void 0
                })
            }), r
        }, "focusin focusout focus blur load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select keydown keypress keyup error".split(" ").forEach(function (t) {
            e.fn[t] = function (e) {
                return 0 in arguments ? this.bind(t, e) : this.trigger(t)
            }
        }), e.Event = function (t, e) {
            s(t) || (e = t, t = e.type);
            var n = document.createEvent(u[t] || "Events"), i = !0;
            if (e) for (var r in e) "bubbles" == r ? i = !!e[r] : n[r] = e[r];
            return n.initEvent(t, i, !0), S(n)
        }
    }(e), function (e) {
        function p(t, n, i) {
            var r = e.Event(n);
            return e(t).trigger(r, i), !r.isDefaultPrevented()
        }

        function d(t, e, n, r) {
            return t.global ? p(e || i, n, r) : void 0
        }

        function m(t) {
            t.global && 0 === e.active++ && d(t, null, "ajaxStart")
        }

        function g(t) {
            t.global && !--e.active && d(t, null, "ajaxStop")
        }

        function y(t, e) {
            var n = e.context;
            return e.beforeSend.call(n, t, e) === !1 || d(e, n, "ajaxBeforeSend", [t, e]) === !1 ? !1 : void d(e, n, "ajaxSend", [t, e])
        }

        function v(t, e, n, i) {
            var r = n.context, o = "success";
            n.success.call(r, t, o, e), i && i.resolveWith(r, [t, o, e]), d(n, r, "ajaxSuccess", [e, n, t]), b(o, e, n)
        }

        function x(t, e, n, i, r) {
            var o = i.context;
            i.error.call(o, n, e, t), r && r.rejectWith(o, [n, e, t]), d(i, o, "ajaxError", [n, i, t || e]), b(e, n, i)
        }

        function b(t, e, n) {
            var i = n.context;
            n.complete.call(i, e, t), d(n, i, "ajaxComplete", [e, n]), g(n)
        }

        function w(t, e, n) {
            if (n.dataFilter == E) return t;
            var i = n.context;
            return n.dataFilter.call(i, t, e)
        }

        function E() {
        }

        function T(t) {
            return t && (t = t.split(";", 2)[0]), t && (t == c ? "html" : t == f ? "json" : a.test(t) ? "script" : u.test(t) && "xml") || "text"
        }

        function S(t, e) {
            return "" == e ? t : (t + "&" + e).replace(/[&?]{1,2}/, "?")
        }

        function j(t) {
            t.processData && t.data && "string" != e.type(t.data) && (t.data = e.param(t.data, t.traditional)), !t.data || t.type && "GET" != t.type.toUpperCase() && "jsonp" != t.dataType || (t.url = S(t.url, t.data), t.data = void 0)
        }

        function C(t, n, i, r) {
            return e.isFunction(n) && (r = i, i = n, n = void 0), e.isFunction(i) || (r = i, i = void 0), {
                url: t,
                data: n,
                success: i,
                dataType: r
            }
        }

        function P(t, n, i, r) {
            var o, s = e.isArray(n), a = e.isPlainObject(n);
            e.each(n, function (n, u) {
                o = e.type(u), r && (n = i ? r : r + "[" + (a || "object" == o || "array" == o ? n : "") + "]"), !r && s ? t.add(u.name, u.value) : "array" == o || !i && "object" == o ? P(t, u, i, n) : t.add(n, u)
            })
        }

        var r, o, n = +new Date, i = t.document, s = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
            a = /^(?:text|application)\/javascript/i, u = /^(?:text|application)\/xml/i, f = "application/json",
            c = "text/html", l = /^\s*$/, h = i.createElement("a");
        h.href = t.location.href, e.active = 0, e.ajaxJSONP = function (r, o) {
            if (!("type" in r)) return e.ajax(r);
            var c, p, s = r.jsonpCallback, a = (e.isFunction(s) ? s() : s) || "Zepto" + n++,
                u = i.createElement("script"), f = t[a], l = function (t) {
                    e(u).triggerHandler("error", t || "abort")
                }, h = {abort: l};
            return o && o.promise(h), e(u).on("load error", function (n, i) {
                clearTimeout(p), e(u).off().remove(), "error" != n.type && c ? v(c[0], h, r, o) : x(null, i || "error", h, r, o), t[a] = f, c && e.isFunction(f) && f(c[0]), f = c = void 0
            }), y(h, r) === !1 ? (l("abort"), h) : (t[a] = function () {
                c = arguments
            }, u.src = r.url.replace(/\?(.+)=\?/, "?$1=" + a), i.head.appendChild(u), r.timeout > 0 && (p = setTimeout(function () {
                l("timeout")
            }, r.timeout)), h)
        }, e.ajaxSettings = {
            type: "GET",
            beforeSend: E,
            success: E,
            error: E,
            complete: E,
            context: null,
            global: !0,
            xhr: function () {
                return new t.XMLHttpRequest
            },
            accepts: {
                script: "text/javascript, application/javascript, application/x-javascript",
                json: f,
                xml: "application/xml, text/xml",
                html: c,
                text: "text/plain"
            },
            crossDomain: !1,
            timeout: 0,
            processData: !0,
            cache: !0,
            dataFilter: E
        }, e.ajax = function (n) {
            var u, f, s = e.extend({}, n || {}), a = e.Deferred && e.Deferred();
            for (r in e.ajaxSettings) void 0 === s[r] && (s[r] = e.ajaxSettings[r]);
            m(s), s.crossDomain || (u = i.createElement("a"), u.href = s.url, u.href = u.href, s.crossDomain = h.protocol + "//" + h.host != u.protocol + "//" + u.host), s.url || (s.url = t.location.toString()), (f = s.url.indexOf("#")) > -1 && (s.url = s.url.slice(0, f)), j(s);
            var c = s.dataType, p = /\?.+=\?/.test(s.url);
            if (p && (c = "jsonp"), s.cache !== !1 && (n && n.cache === !0 || "script" != c && "jsonp" != c) || (s.url = S(s.url, "_=" + Date.now())), "jsonp" == c) return p || (s.url = S(s.url, s.jsonp ? s.jsonp + "=?" : s.jsonp === !1 ? "" : "callback=?")), e.ajaxJSONP(s, a);
            var O, d = s.accepts[c], g = {}, b = function (t, e) {
                g[t.toLowerCase()] = [t, e]
            }, C = /^([\w-]+:)\/\//.test(s.url) ? RegExp.$1 : t.location.protocol, N = s.xhr(), P = N.setRequestHeader;
            if (a && a.promise(N), s.crossDomain || b("X-Requested-With", "XMLHttpRequest"), b("Accept", d || "*/*"), (d = s.mimeType || d) && (d.indexOf(",") > -1 && (d = d.split(",", 2)[0]), N.overrideMimeType && N.overrideMimeType(d)), (s.contentType || s.contentType !== !1 && s.data && "GET" != s.type.toUpperCase()) && b("Content-Type", s.contentType || "application/x-www-form-urlencoded"), s.headers) for (o in s.headers) b(o, s.headers[o]);
            if (N.setRequestHeader = b, N.onreadystatechange = function () {
                if (4 == N.readyState) {
                    N.onreadystatechange = E, clearTimeout(O);
                    var t, n = !1;
                    if (N.status >= 200 && N.status < 300 || 304 == N.status || 0 == N.status && "file:" == C) {
                        if (c = c || T(s.mimeType || N.getResponseHeader("content-type")), "arraybuffer" == N.responseType || "blob" == N.responseType) t = N.response; else {
                            t = N.responseText;
                            try {
                                t = w(t, c, s), "script" == c ? (1, eval)(t) : "xml" == c ? t = N.responseXML : "json" == c && (t = l.test(t) ? null : e.parseJSON(t))
                            } catch (i) {
                                n = i
                            }
                            if (n) return x(n, "parsererror", N, s, a)
                        }
                        v(t, N, s, a)
                    } else x(N.statusText || null, N.status ? "error" : "abort", N, s, a)
                }
            }, y(N, s) === !1) return N.abort(), x(null, "abort", N, s, a), N;
            var M = "async" in s ? s.async : !0;
            if (N.open(s.type, s.url, M, s.username, s.password), s.xhrFields) for (o in s.xhrFields) N[o] = s.xhrFields[o];
            for (o in g) P.apply(N, g[o]);
            return s.timeout > 0 && (O = setTimeout(function () {
                N.onreadystatechange = E, N.abort(), x(null, "timeout", N, s, a)
            }, s.timeout)), N.send(s.data ? s.data : null), N
        }, e.get = function () {
            return e.ajax(C.apply(null, arguments))
        }, e.post = function () {
            var t = C.apply(null, arguments);
            return t.type = "POST", e.ajax(t)
        }, e.getJSON = function () {
            var t = C.apply(null, arguments);
            return t.dataType = "json", e.ajax(t)
        }, e.fn.load = function (t, n, i) {
            if (!this.length) return this;
            var a, r = this, o = t.split(/\s/), u = C(t, n, i), f = u.success;
            return o.length > 1 && (u.url = o[0], a = o[1]), u.success = function (t) {
                r.html(a ? e("<div>").html(t.replace(s, "")).find(a) : t), f && f.apply(r, arguments)
            }, e.ajax(u), this
        };
        var N = encodeURIComponent;
        e.param = function (t, n) {
            var i = [];
            return i.add = function (t, n) {
                e.isFunction(n) && (n = n()), null == n && (n = ""), this.push(N(t) + "=" + N(n))
            }, P(i, t, n), i.join("&").replace(/%20/g, "+")
        }
    }(e), function () {
        try {
            getComputedStyle(void 0)
        } catch (e) {
            var n = getComputedStyle;
            t.getComputedStyle = function (t, e) {
                try {
                    return n(t, e)
                } catch (i) {
                    return null
                }
            }
        }
    }(), function (t, e) {
        function y(t) {
            return t.replace(/([A-Z])/g, "-$1").toLowerCase()
        }

        function v(t) {
            return i ? i + t : t.toLowerCase()
        }

        var i, a, u, f, c, l, h, p, d, m, n = "", r = {Webkit: "webkit", Moz: "", O: "o"},
            o = document.createElement("div"),
            s = /^((translate|rotate|scale)(X|Y|Z|3d)?|matrix(3d)?|perspective|skew(X|Y)?)$/i, g = {};
        o.style.transform === e && t.each(r, function (t, r) {
            return o.style[t + "TransitionProperty"] !== e ? (n = "-" + t.toLowerCase() + "-", i = r, !1) : void 0
        }), a = n + "transform", g[u = n + "transition-property"] = g[f = n + "transition-duration"] = g[l = n + "transition-delay"] = g[c = n + "transition-timing-function"] = g[h = n + "animation-name"] = g[p = n + "animation-duration"] = g[m = n + "animation-delay"] = g[d = n + "animation-timing-function"] = "", t.fx = {
            off: i === e && o.style.transitionProperty === e,
            speeds: {_default: 400, fast: 200, slow: 600},
            cssPrefix: n,
            transitionEnd: v("TransitionEnd"),
            animationEnd: v("AnimationEnd")
        }, t.fn.animate = function (n, i, r, o, s) {
            return t.isFunction(i) && (o = i, r = e, i = e), t.isFunction(r) && (o = r, r = e), t.isPlainObject(i) && (r = i.easing, o = i.complete, s = i.delay, i = i.duration), i && (i = ("number" == typeof i ? i : t.fx.speeds[i] || t.fx.speeds._default) / 1e3), s && (s = parseFloat(s) / 1e3), this.anim(n, i, r, o, s)
        }, t.fn.anim = function (n, i, r, o, v) {
            var x, w, S, b = {}, E = "", T = this, j = t.fx.transitionEnd, C = !1;
            if (i === e && (i = t.fx.speeds._default / 1e3), v === e && (v = 0), t.fx.off && (i = 0), "string" == typeof n) b[h] = n, b[p] = i + "s", b[m] = v + "s", b[d] = r || "linear", j = t.fx.animationEnd; else {
                w = [];
                for (x in n) s.test(x) ? E += x + "(" + n[x] + ") " : (b[x] = n[x], w.push(y(x)));
                E && (b[a] = E, w.push(a)), i > 0 && "object" == typeof n && (b[u] = w.join(", "), b[f] = i + "s", b[l] = v + "s", b[c] = r || "linear")
            }
            return S = function (e) {
                if ("undefined" != typeof e) {
                    if (e.target !== e.currentTarget) return;
                    t(e.target).unbind(j, S)
                } else t(this).unbind(j, S);
                C = !0, t(this).css(g), o && o.call(this)
            }, i > 0 && (this.bind(j, S), setTimeout(function () {
                C || S.call(T)
            }, 1e3 * (i + v) + 25)), this.size() && this.get(0).clientLeft, this.css(b), 0 >= i && setTimeout(function () {
                T.each(function () {
                    S.call(this)
                })
            }, 0), this
        }, o = null
    }(e), function (e, n) {
        function a(t, i, r, o, s) {
            "function" != typeof i || s || (s = i, i = n);
            var a = {opacity: r};
            return o && (a.scale = o, t.css(e.fx.cssPrefix + "transform-origin", "0 0")), t.animate(a, i, null, s)
        }

        function u(t, n, i, r) {
            return a(t, n, 0, i, function () {
                o.call(e(this)), r && r.call(this)
            })
        }

        var r = (t.document, e.fn.show), o = e.fn.hide, s = e.fn.toggle;
        e.fn.show = function (t, e) {
            return r.call(this), t === n ? t = 0 : this.css("opacity", 0), a(this, t, 1, "1,1", e)
        }, e.fn.hide = function (t, e) {
            return t === n ? o.call(this) : u(this, t, "0,0", e)
        }, e.fn.toggle = function (t, i) {
            return t === n || "boolean" == typeof t ? s.call(this, t) : this.each(function () {
                var n = e(this);
                n["none" == n.css("display") ? "show" : "hide"](t, i)
            })
        }, e.fn.fadeTo = function (t, e, n) {
            return a(this, t, e, null, n)
        }, e.fn.fadeIn = function (t, e) {
            var n = this.css("opacity");
            return n > 0 ? this.css("opacity", 0) : n = 1, r.call(this).fadeTo(t, n, e)
        }, e.fn.fadeOut = function (t, e) {
            return u(this, t, null, e)
        }, e.fn.fadeToggle = function (t, n) {
            return this.each(function () {
                var i = e(this);
                i[0 == i.css("opacity") || "none" == i.css("display") ? "fadeIn" : "fadeOut"](t, n)
            })
        }
    }(e), function (e) {
        function d(t, e, n, i) {
            return Math.abs(t - e) >= Math.abs(n - i) ? t - e > 0 ? "Left" : "Right" : n - i > 0 ? "Up" : "Down"
        }

        function m() {
            s = null, n.last && (n.el.trigger("longTap"), n = {})
        }

        function g() {
            s && clearTimeout(s), s = null
        }

        function y() {
            i && clearTimeout(i), r && clearTimeout(r), o && clearTimeout(o), s && clearTimeout(s), i = r = o = s = null, n = {}
        }

        function v(t) {
            return ("touch" == t.pointerType || t.pointerType == t.MSPOINTER_TYPE_TOUCH) && t.isPrimary
        }

        function x(t, e) {
            return t.type == "pointer" + e || t.type.toLowerCase() == "mspointer" + e
        }

        function b() {
            p && (e(document).off(h.down, f).off(h.up, c).off(h.move, l).off(h.cancel, y), e(t).off("scroll", y), y(), p = !1)
        }

        function w(w) {
            var E, T, C, N, S = 0, j = 0;
            b(), h = w && "down" in w ? w : "ontouchstart" in document ? {
                down: "touchstart",
                up: "touchend",
                move: "touchmove",
                cancel: "touchcancel"
            } : "onpointerdown" in document ? {
                down: "pointerdown",
                up: "pointerup",
                move: "pointermove",
                cancel: "pointercancel"
            } : "onmspointerdown" in document ? {
                down: "MSPointerDown",
                up: "MSPointerUp",
                move: "MSPointerMove",
                cancel: "MSPointerCancel"
            } : !1, h && ("MSGesture" in t && (u = new MSGesture, u.target = document.body, e(document).bind("MSGestureEnd", function (t) {
                var e = t.velocityX > 1 ? "Right" : t.velocityX < -1 ? "Left" : t.velocityY > 1 ? "Down" : t.velocityY < -1 ? "Up" : null;
                e && (n.el.trigger("swipe"), n.el.trigger("swipe" + e))
            })), f = function (t) {
                (!(N = x(t, "down")) || v(t)) && (C = N ? t : t.touches[0], t.touches && 1 === t.touches.length && n.x2 && (n.x2 = void 0, n.y2 = void 0), E = Date.now(), T = E - (n.last || E), n.el = e("tagName" in C.target ? C.target : C.target.parentNode), i && clearTimeout(i), n.x1 = C.pageX, n.y1 = C.pageY, T > 0 && 250 >= T && (n.isDoubleTap = !0), n.last = E, s = setTimeout(m, a), u && N && u.addPointer(t.pointerId))
            }, l = function (t) {
                (!(N = x(t, "move")) || v(t)) && (C = N ? t : t.touches[0], g(), n.x2 = C.pageX, n.y2 = C.pageY, S += Math.abs(n.x1 - n.x2), j += Math.abs(n.y1 - n.y2))
            }, c = function (t) {
                (!(N = x(t, "up")) || v(t)) && (g(), n.x2 && Math.abs(n.x1 - n.x2) > 30 || n.y2 && Math.abs(n.y1 - n.y2) > 30 ? o = setTimeout(function () {
                    n.el && (n.el.trigger("swipe"), n.el.trigger("swipe" + d(n.x1, n.x2, n.y1, n.y2))), n = {}
                }, 0) : "last" in n && (30 > S && 30 > j ? r = setTimeout(function () {
                    var t = e.Event("tap");
                    t.cancelTouch = y, n.el && n.el.trigger(t), n.isDoubleTap ? (n.el && n.el.trigger("doubleTap"), n = {}) : i = setTimeout(function () {
                        i = null, n.el && n.el.trigger("singleTap"), n = {}
                    }, 250)
                }, 0) : n = {}), S = j = 0)
            }, e(document).on(h.up, c).on(h.down, f).on(h.move, l), e(document).on(h.cancel, y), e(t).on("scroll", y), p = !0)
        }

        var i, r, o, s, u, f, c, l, h, n = {}, a = 750, p = !1;
        ["swipe", "swipeLeft", "swipeRight", "swipeUp", "swipeDown", "doubleTap", "tap", "singleTap", "longTap"].forEach(function (t) {
            e.fn[t] = function (e) {
                return this.on(t, e)
            }
        }), e.touch = {setup: w}, e(document).ready(w)
    }(e), e
});
;
(function init() {
    if (window.zaaxstat != null || window.zaaxstat) {
        return
    }
    window.$zq = window.Zepto = Zepto;
    var D = {
        zaaxstat_version: "v5.5.6",
        baseurl: getProtocol() + "api.hduofen.cn/sem/",
        plugsdUrl: getProtocol() + "res.hduofen.cn/js/plugs.js",
        alertUrl: getProtocol() + "res.hduofen.cn/js/alerts.js",
        zaaxgobackUrl: getProtocol() + "res.hduofen.cn/js/zaaxgoback.js",
        clipboardUrl: getProtocol() + "res.hduofen.cn/js/tools/clipboard.js",
        jqueryUrl: getProtocol() + "apps.bdimg.com/libs/jquery/1.9.1/jquery.min.js",
        fileurl: getProtocol() + "file.hduofen.cn",
        protocol: getProtocol(),
        href: window.location.href,
        path: window.location.pathname,
        user_uuid: (isEmpty(UrlSearch(document.scripts[document.scripts.length - 1].src).id)) ? getUuid() : (UrlSearch(document.scripts[document.scripts.length - 1].src).id),
        devices_type: getDevices()[2],
        devices: getDevices()[1],
        userAgent: navigator.userAgent,
        platform: navigator.platform,
        browserName: 0,
        browserVersion: "",
        modify: 0,
        wxhClassName: ".zaax-wxh",
        copy_wxh: "",
        last_copy_wxh: "",
        serach_text: "",
        seo: 0,
        source_url: "",
        referrer: document.referrer,
        last_wx: "",
        wxh_name: "",
        wxh_sex: "",
        city: "",
        browse_start: 1,
        return_stutas: 0,
        return_url: "",
        return_msg: "",
        sessions: "",
        muserWxhManager: null,
        mcopyStatiConf: null,
        zaaxwx: "",
        zaaxname: "",
        zaaxsex: "",
        zaaxqr: "",
        zaaxcity: "",
        access_ratio: 0,
        is_cache: 0,
        share_change: 0,
        is_wxscheme: 0,
        is_wxkf: 0,
        hdfshare: "",
        fast_cache: false,
        hduofenCache: {wxh: "", name: "", sex: "", qr: ""},
        tipsalertShow: true,
        alertIsShow: false,
        isOpen: true,
        isSubmitBrowserTime: true,
        isCall: true,
        is_hdf_getWxh: true,
        hdf_is_open_copy: true,
        hdf_is_browse: true,
        hdf_is_cvt: true,
        hdf_initJscode: true,
        hdf_copy_btn: false
    };
    if (isEmpty(D.user_uuid)) {
        return
    }
    console.log("%c\u597d%c\u591a%c\u7c89 \n%c\u597d\u591a\u7c89\u52a0\u7c89\u7edf\u8ba1\u7cfb\u7edf\uff0c\u4e13\u4e1a\u670d\u52a1\u7ade\u4ef7\u52a0\u7c89\u5ba2\u6237\uff0c\u53ef\u63d0\u4f9b\u5fae\u4fe1\u52a0\u7c89\u7edf\u8ba1\uff0c\u8bbf\u5ba2\u771f\u5b9e\u5230\u7c89\u7edf\u8ba1\uff0c\u590d\u5236\u7edf\u8ba1\uff0c\u5fae\u4fe1\u53f7\u7ba1\u7406\uff0c\u4e8c\u7ef4\u7801\u8f6e\u64ad\uff0c\u6d4f\u89c8\u5668\u8df3\u8f6c\u5fae\u4fe1\u52a0\u7c89\uff0curl\u8df3\u8f6c\u529f\u80fd\uff0c\u8bbf\u5ba2\u884c\u4e3a\u8f6c\u5316\u7edf\u8ba1\uff0c\u843d\u5730\u9875\u63d2\u4ef6\uff0c\u7b49\u591a\u79cd\u63a8\u5e7f\u76f8\u5173\u4e13\u4e1a\u529f\u80fd \n%c\u5b98\u7f51:https://hduofen.com", 'font-size:18px;color:#4285f4;text-shadow:0 0 10px #4285f4;font-family:Microsoft YaHei', 'font-size:18px;color:#34a853;text-shadow:0 0 10px #34a853;font-family:Microsoft YaHei', 'font-size:18px;color:#ea4335;text-shadow:0 0 10px #ea4335;font-family:Microsoft YaHei', 'font-size:18px;color:#56aff5;text-shadow:0 0 10px #279bf5;font-family:Microsoft YaHei', 'font-size:14px;color:#279bf5;font-family:Microsoft YaHei');
    D.url = D.href;
    D.simpleUrl = zformUrl(D.href.split("?")[0].split("#")[0]);
    D.hdfshare = getQueryString(D.href, "hdfshare");
    D.sessions = getCookie("sessions");
    if (isEmpty(D.sessions)) {
        D.sessions = guid();
        setCookie("sessions", D.sessions)
    }
    D.zaaxwx = $zq(".zaax-wxh");
    D.zaaxname = $zq(".zaax-wxname");
    D.zaaxsex = $zq(".zaax-wxsex");
    D.zaaxqr = $zq(".zaax-qr");
    if (!isEmpty(D.zaaxwx) && D.zaaxwx[0] != null) {
        D.modify = D.zaaxwx.length;
        D.last_wx = D.zaaxwx[0].innerText == null ? "" : $zq(".zaax-wxh").eq(0).text()
    }
    initBrowseName();
    partReferrer();
    initJump();
    initAjax();
    getWxh();

    function initJump() {
        try {
            window.parent.postMessage({zaaxcode: 100}, '*');
            addEventListener('message', function (e) {
                if (!isEmpty(e.data) && e.data.zaaxcode === 101) {
                    if (e.data.zaaxIsJump && !isEmpty(e.data.zaaxJumpReferrer)) {
                        D.referrer = e.data.zaaxJumpReferrer;
                        partReferrer()
                    }
                }
            })
        } catch (e) {
            console.log(e)
        }
    }

    function initAjax() {
        $zq.ajaxSettings.type = "post";
        $zq.ajaxSettings.dataType = "json";
        $zq.ajaxSettings.crossDomain = true;
        $zq.ajaxSettings.xhrFields = {withCredentials: true};
        try {
            window._hdf = {pushCopyData: subCopyData, pushCvtData: subCvtData, pushOpenWxData: subOpenWxData};
            window.zaaxstat = {
                loadScript: loadScript,
                getProtocol: getProtocol,
                isPoneAvailable: isPoneAvailable,
                isEmpty: isEmpty,
                getDevices: getDevices,
                setCookie: setCookie,
                getCookie: getCookie,
                clearCookie: clearCookie,
                tipsalert: tipsalert,
                initClipboard: initClipboard,
                getAccessRatio: getAccessRatio,
                callFun: callFun
            }
        } catch (e) {
            console.log(e)
        }
    }

    function callFun(a) {
        if (a === 'submitCallPhone') {
            submitCallPhone()
        } else if (a === 'submitOpenCount') {
            submitOpenCount()
        }
    }

    function getWxh() {
        if (D.is_hdf_getWxh) {
            D.is_hdf_getWxh = false;
            $zq.ajax({
                url: D.baseurl + "webstatic/findByUserIdWxh",
                data: {uuid: D.user_uuid, url: D.simpleUrl, sessions: D.sessions, hdfshare: D.hdfshare},
                success: function (a) {
                    try {
                        if (a.success) {
                            if (a.data.userWxhConf) {
                                var b = a.data.userWxhConf;
                                D.muserWxhManager = a.data.userWxhManager;
                                D.is_cache = b.is_cache;
                                if (D.is_cache == 1) {
                                    if (!isEmpty(getCookie("hduofenCache_" + D.path))) {
                                        D.hduofenCache = JSON.parse(getCookie("hduofenCache_" + D.path))
                                    }
                                } else {
                                    clearCookie("hduofenCache_" + D.path)
                                }
                                if (b.wxh_methods == 0) {
                                    if (!isEmpty(a.data.userWxhManager.wxh)) {
                                        D.last_wx = a.data.userWxhManager.wxh;
                                        setWxh(b);
                                        if (!isEmpty(a.data.userWxhManager.wxh_name) || D.is_cache == 1) {
                                            D.wxh_name = a.data.userWxhManager.wxh_name;
                                            setWxName()
                                        }
                                        if (!isEmpty(a.data.userWxhManager.wxh_sex) || D.is_cache == 1) {
                                            D.wxh_sex = a.data.userWxhManager.wxh_sex;
                                            setWxSex()
                                        }
                                        if (!isEmpty(a.data.userWxhManager.city) || D.is_cache == 1) {
                                            D.city = a.data.userWxhManager.city;
                                            setCity()
                                        }
                                    }
                                } else {
                                    switch (b.display_type) {
                                        case 1:
                                            displayType1(b);
                                            break;
                                        case 2:
                                            displayType2(b);
                                            break
                                    }
                                }
                                getQr(b.wxh_group, b.qr_setting, b.qr_def, D.last_wx);
                                if (b.share_change == 1) {
                                    if (isEmpty(D.hdfshare) && !isEmpty(D.last_wx)) {
                                        clearCookie("hduofenCache_" + D.path);
                                        setHdfshare(D.last_wx);
                                        if (location.hash.indexOf("#") == -1) {
                                            return
                                        }
                                    }
                                } else if (D.href.indexOf("hdfshare") != -1) {
                                    window.location.href = delParame("hdfshare");
                                    if (location.hash.indexOf("#") == -1) {
                                        return
                                    }
                                }
                            }
                            if (a.data.copyStatiConf) {
                                var c = a.data.copyStatiConf;
                                D.mcopyStatiConf = a.data.copyStatiConf;
                                if (c.stati_methods == 0) {
                                    if (!isEmpty(D.last_wx)) {
                                        if ((D.devices_type == 1 && b.is_long_wx == 0) || (D.devices_type != 1 && b.pc_is_open_wx == 0 && b.pc_copy_wx == 0)) {
                                            copyListen(D.last_wx)
                                        }
                                    }
                                } else {
                                    manualCopyWxh(c)
                                }
                            }
                            if (a.data.return == 1) {
                                getReturnStatus()
                            }
                            var d = a.data.cvt_stat;
                            if (d == 1) {
                                getCvtStat()
                            }
                            if (a.data.plug == 1) {
                                getPlug()
                            }
                            if (a.data.is_wxscheme == 1) {
                                D.is_wxscheme = 1;
                                getWxProgramUid()
                            } else if (a.data.is_wxkf == 1) {
                                getWxKfUrl()
                            }
                        }
                    } catch (e) {
                        console.log(e)
                    }
                    browse();
                    initJsCode();
                    try {
                        $zq(document).ready(function () {
                            if (window._hdf.callback) {
                                window._hdf.callback({
                                    wxh: window._hdf.wxh,
                                    wxh_name: window._hdf.wxname,
                                    wxh_sex: window._hdf.wxsex
                                })
                            }
                            if (window._hdfCallback) {
                                window._hdfCallback({
                                    wxh: window._hdf.wxh,
                                    wxh_name: window._hdf.wxname,
                                    wxh_sex: window._hdf.wxsex
                                })
                            }
                        })
                    } catch (e) {
                        console.log(e)
                    }
                }
            })
        }
    }

    function manualCopyWxh(k) {
        var l = Math.round(Math.random() * 10000);
        var m = k.is_open_wx;
        var n = k.pc_is_open_wx;
        var o = k.is_long_wx;
        var p = k.pc_copy_wx;
        var q = k.alert_data;
        if (isEmpty(k.wxh)) {
            return
        }
        var r = k.wxh.split("|");
        $zq.each(r, function (a, b) {
            if (!isEmpty(b)) {
                if (m > 0 || n > 0 || o > 0 || p > 0) {
                    forCopyWx(b);
                    $zq(document).ready(function () {
                        setInterval(function () {
                            forCopyWx(b)
                        }, 3000);
                        if (D.zaaxwx.length == 0) {
                            $zq("body").append("<div style='display: none!important;'><span class='zaax-wxh'>" + b + "</span></div>")
                        }
                    });
                    $zq(document).on("click", ".zaax-click", function () {
                        $zq(".zaax-wxh").click()
                    })
                }
                if ((D.devices_type == 1 && o == 0) || (D.devices_type != 1 && n == 0 && p == 0)) {
                    setTimeout(function () {
                        copyListen(b)
                    }, a * 55)
                }
            }
        });
        if (m > 0 || n > 0 || o > 0 || p > 0) {
            loadScript(D.alertUrl, function () {
                hdfAlerts(D, 'cp', {
                    copy_openWx: m,
                    copy_pc_openWx: n,
                    copy_long_copy: o,
                    copy_pc_long_copy: p,
                    alert_data: q
                })
            })
        }

        function forCopyWx(b) {
            $zq(".zaax-wxh:not([data-wxh-round='" + l + "'])").removeClass("zaax-wxh");
            var c = "<span class='zaax-wxh' data-wxh-round='" + l + "' style='cursor:pointer'>" + b + "</span>";
            var d = new RegExp(b, "g");
            if (!isEmpty($zq("body").text()) && $zq("body").text().indexOf(b) != -1) {
                var e = $zq("body *:not(img):not(script):not(br):not(style)");
                for (var i = 0; i < e.length; i++) {
                    var f = $zq(e[i]).contents().filter(function (a) {
                        return this.nodeType === 3
                    }).text();
                    var g = $zq(e[i]).hasClass("zaax-wxh");
                    var h = $zq(e[i]).data("wxh-round");
                    var j = $zq(e[i]).data("status-wx");
                    if (!isEmpty(f) && f.indexOf(b) != -1 && !g && h != l && j != 0) {
                        if (D.devices_type == 1 && o != 0) {
                            c = "<span class='zaax-wxh' data-wxh-round='" + l + "' style='cursor:pointer;position:relative' ontouchstart='return false'>" + b + copyHtml() + "</span>"
                        }
                        if (D.devices_type != 1 && p != 0) {
                            if (n != 0) {
                                c = "<span class='zaax-wxh' data-wxh-round='" + l + "' style='cursor:pointer;position:relative' oncopy='window.hdfPcCopy(this)'>" + b + "</span>"
                            } else {
                                c = "<span class='zaax-wxh' data-wxh-round='" + l + "' style='position:relative' oncopy='window.hdfPcCopy(this)'>" + b + "</span>"
                            }
                        }
                        if ($zq(e[i]).find("script").length > 0 && $zq(e[i]).find("script").text().replace(/[ ]/g, "").indexOf("document.write") != -1) {
                            $zq(e[i]).find("script").remove()
                        }
                        $zq(e[i]).html($zq(e[i]).html().replace(d, c))
                    }
                }
            }
        }
    }

    function getQr(b, c, d, f) {
        if (D.is_cache == 1 && !isEmpty(D.hduofenCache.qr)) {
            try {
                var g = D.hduofenCache.qr;
                setQr(g)
            } catch (e) {
                console.log(e)
            }
        } else {
            if (c == 0) {
                return
            }
            $zq.ajax({
                url: D.baseurl + "webstatic/getQr",
                data: {user_uuid: D.user_uuid, wxh: f, qr_def: d, qrsetting: c, wxh_group: b},
                success: function (a) {
                    if (a.success) {
                        setCookie("zaax-qr_" + D.path, a.data);
                        try {
                            if (D.is_cache == 1 && D.fast_cache) {
                                D.hduofenCache.qr = a.data;
                                setCookie("hduofenCache_" + D.path, JSON.stringify(D.hduofenCache))
                            }
                            setQr(a.data)
                        } catch (e) {
                            console.log(e)
                        }
                    }
                }
            })
        }
    }

    function setQr(c) {
        $zq(document).ready(function () {
            if (!isEmpty(D.zaaxqr)) {
                $zq(".zaax-qr").attr("src", D.fileurl + c)
            }
            window._hdf.qrpath = D.fileurl + c;
            var b = D.zaaxqr.length;
            setInterval(function () {
                var a = $zq(".zaax-qr");
                if (a.length > b) {
                    b = a.length;
                    $zq(".zaax-qr").attr("src", D.fileurl + c)
                }
            }, 80)
        })
    }

    function getReturnStatus() {
        if (D.seo == 6) {
            return
        }
        $zq.ajax({
            url: D.baseurl + "webstatic/getReturnStatus",
            data: {
                user_uuid: D.user_uuid,
                url: D.simpleUrl,
                devices_type: D.devices_type,
                serach_text: isEmpty(D.serach_text) ? "" : D.serach_text,
                seo: D.seo
            },
            success: function (a) {
                if (a.success) {
                    startReturn(a.data)
                }
            }
        })
    }

    function startReturn(a) {
        var b = {title: "title", url: ""};
        window.history.pushState(b, null, "");
        window.addEventListener("popstate", function (e) {
            setTimeout(function () {
                window.location.href = a
            }, 500)
        }, false)
    }

    function getCvtStat() {
        $zq.ajax({
            url: D.baseurl + "webstatic/getCvt",
            data: {user_uuid: D.user_uuid, url: D.simpleUrl},
            success: function (a) {
                if (a != null && a.success) {
                    initCvtStat(a.data)
                }
            }
        })
    }

    function initCvtStat(k) {
        $zq(document).ready(function () {
            var c = false;
            $zq.each(k, function (a, b) {
                if (b.stat_type == 0) {
                    if (b.touch_type == 0) {
                        if (D.devices_type == 1) {
                            longTouch('[data-hdf-cvt="' + b.conversion_type + '"]', null, function (e) {
                                submitCvt(b);
                                return
                            })
                        } else {
                            $zq(document).on("click", '[data-hdf-cvt="' + b.conversion_type + '"]', function () {
                                submitCvt(b);
                                return
                            })
                        }
                    } else if (b.touch_type == 1) {
                        longTouch('[data-hdf-cvt="' + b.conversion_type + '"]', function (e) {
                            submitCvt(b);
                            return
                        })
                    }
                } else {
                    c = true
                }
            });
            if (c) {
                if (getDevices()[1] == 'ios') {
                    $zq("body").css("cursor", "pointer")
                }
                if (D.devices_type == 1) {
                    longTouch("body", null, function (e) {
                        var a = e.srcElement ? e.srcElement : e.target;
                        queryElement(0, a);
                        return
                    })
                } else {
                    $zq(document).on("click", function (e) {
                        var a = e.srcElement ? e.srcElement : e.target;
                        queryElement(0, a)
                    })
                }
                longTouch("body", function (e) {
                    var a = e.srcElement ? e.srcElement : e.target;
                    queryElement(1, a)
                })
            }
        });

        function queryElement(i, j) {
            $zq.each(k, function (f, g) {
                if (g.stat_type == 1) {
                    var h = g.dim_content.split("|");
                    $zq.each(h, function (a, b) {
                        if (!isEmpty(b)) {
                            if (g.dim_select == 1) {
                                var c = j.innerText;
                                if (!isEmpty(c) && c.indexOf(b) != -1) {
                                    if (g.touch_type == i) {
                                        submitCvt(g, b)
                                    }
                                    return
                                }
                            } else if (g.dim_select == 2) {
                                var d = j.id;
                                if (!isEmpty(d) && d.indexOf(b) != -1) {
                                    if (g.touch_type == i) {
                                        submitCvt(g, b)
                                    }
                                    return
                                }
                            } else if (g.dim_select == 3) {
                                var e = j.className;
                                if (!isEmpty(e) && e.indexOf(b) != -1) {
                                    if (g.touch_type == i) {
                                        submitCvt(g, b)
                                    }
                                    return
                                }
                            }
                        }
                    })
                }
            })
        }
    }

    function getPlug() {
        $zq.ajax({
            url: D.baseurl + "webstatic/getPlug",
            data: {user_uuid: D.user_uuid, url: D.simpleUrl},
            success: function (a) {
                if (a != null && a.success) {
                    initPlgu(a.data)
                }
            }
        })
    }

    function initPlgu(c) {
        loadScript(D.plugsdUrl, function () {
            $zq.each(c, function (a, b) {
                hdfPlug(D, b)
            })
        })
    }

    function subCvtData(a) {
        var b = {conversion_type: a, count_type: 1, stat_type: 0, dim_select: 0};
        submitCvt(b)
    }

    function submitCvt(b, c) {
        $zq.ajax({
            url: D.baseurl + "webstatic/sumbitCvt",
            data: {
                user_uuid: D.user_uuid,
                sessions: D.sessions,
                url: D.simpleUrl,
                count_type: b.count_type,
                touch_type: b.touch_type,
                stat_type: b.stat_type,
                dim_select: b.dim_select,
                current_wxh: D.last_wx,
                dim_content: isEmpty(c) ? "" : c,
                complete_url: D.url.replace(/&/g, '||'),
                conversion_type: b.conversion_type,
                devices_type: D.devices_type,
                device: D.devices,
                serach_text: isEmpty(D.serach_text) ? "" : D.serach_text,
                seo: D.seo,
                browserName: D.browserName,
                browserVersion: D.browserVersion,
                source_url: D.source_url,
                min_cvttime: b.min_cvttime ? b.min_cvttime : 0
            },
            success: function (a) {
                try {
                    if (a.success) {
                        if (!isEmpty(hdfCvtCallback)) {
                            hdfCvtCallback(b.conversion_type)
                        }
                    }
                } catch (e) {
                }
            }
        })
    }

    function displayType1(a) {
        var b = a.wxh.split("|");
        var c = [];
        var d = 0;
        for (var i = 0; i < 10; i++) {
            if (d >= b.length) {
                d = 0
            }
            c.push(b[d]);
            d++
        }
        var e = Math.floor(Math.random() * 10);
        D.last_wx = c[e];
        setWxh(a)
    }

    function displayType2(a) {
        var b = a.wxh.split("|");
        var c = new Date();
        var d = c.getDay();
        var e = c.getHours();
        var f = c.getMinutes();
        switch (a.for_time) {
            case 1:
                D.last_wx = b[f % b.length];
                break;
            case 2:
                var g = Math.floor(f / 5);
                D.last_wx = b[g % b.length];
                break;
            case 3:
                var h = Math.floor(e * 6 + f / 10);
                D.last_wx = b[h % b.length];
                break;
            case 4:
                var i = Math.floor(e * 2 + f / 30);
                D.last_wx = b[i % b.length];
                break;
            case 5:
                D.last_wx = b[e % b.length];
                break;
            case 6:
                var j = Math.floor((d * 24 + e) / 3);
                D.last_wx = b[j % b.length];
                break;
            case 7:
                var k = Math.floor((d * 24 + e) / 6);
                D.last_wx = b[k % b.length];
                break;
            case 8:
                var l = Math.floor((d * 24 + e) / 12);
                D.last_wx = b[l % b.length];
                break
        }
        setWxh(a)
    }

    function setWxh(f) {
        if (D.is_cache == 1) {
            try {
                if (!isEmpty(D.hduofenCache.wxh)) {
                    D.last_wx = D.hduofenCache.wxh;
                    console.log("hduofen cache锛�" + D.hduofenCache.wxh)
                } else {
                    if (!isEmpty(D.last_wx)) {
                        D.hduofenCache.wxh = D.last_wx;
                        D.fast_cache = true;
                        setCookie("hduofenCache_" + D.path, JSON.stringify(D.hduofenCache))
                    }
                }
            } catch (e) {
                console.log(e)
            }
        }
        window._hdf.wxh = D.last_wx;
        var g = f.is_open_wx;
        var h = f.pc_is_open_wx;
        var i = f.is_long_wx;
        var j = f.pc_copy_wx;
        var k = JSON.parse(f.wx_style);
        var l = f.wx_display_type;
        var m = f.pc_wx_display_type;
        var n = f.alert_data;
        var o = D.last_wx;
        var p;
        if (D.devices_type == 1) {
            if (l == 1) {
                p = "<span class='zaax-wxh-suffix'>\u0020\u0028\u70b9\u51fb\u590d\u5236\u0029</span>"
            } else if (l == 2) {
            } else if (l == 3) {
                p = "<span class='zaax-wxh-suffix'>\u0028\u957F\u6309\u590D\u5236\u0029</span>"
            } else if (l == 4) {
                p = " <span class='zaax-wxh-suffix' style='color: #333;'>\u0028\u5FAE\u4FE1\u540C\u53F7\u0029</span>"
            } else if (l == 5) {
                p = " <span class='zaax-wxh-suffix' style='color: #333;'>\u0028\u53D1\u9001\u77ED\u4FE1\u0029</span>"
            } else if (l == 6) {
                p = " <span class='zaax-wxh-suffix' style='color: #333;'>\u0028\u67E5\u770B\u4E8C\u7EF4\u7801\u0029</span>"
            } else if (l == 7) {
                p = " <img class='zaax-wxh-suffix' style='vertical-align: middle!important;display: inline;padding: 0;margin: 0;width:100px!important;height:auto!important;float: none!important;border-radius: 0' src='" + D.protocol + "res.hduofen.cn/images/lookqr.png'>"
            } else if (l == 8) {
                p = " <img class='zaax-wxh-suffix' style='vertical-align: middle!important;display: inline;padding: 0;margin: 0;width:100px!important;height:auto!important;float: none!important;border-radius: 0' src='" + D.protocol + "res.hduofen.cn/images/icon_clickcopy.png'>"
            } else if (l == 9) {
                p = " <img class='zaax-wxh-suffix' style='vertical-align: middle!important;display: inline;padding: 0;margin: 0;width:100px!important;height:auto!important;float: none!important;border-radius: 0' src='" + D.protocol + "res.hduofen.cn/images/btn_addwx.png'>"
            }
        } else {
            if (m == 1) {
            } else if (m == 2) {
                p = " <span class='zaax-wxh-suffix' style='color: #333;'>\u0020\u0028\u70b9\u51fb\u590d\u5236\u0029</span>"
            } else if (m == 3) {
                p = " <span class='zaax-wxh-suffix' style='color: #333;'>\u0028\u67E5\u770B\u4E8C\u7EF4\u7801\u0029</span>"
            } else if (m == 4) {
                p = " <img class='zaax-wxh-suffix' style='vertical-align: middle!important;display: inline;padding: 0;margin: 0;width:100px!important;height:auto!important;float: none!important;border-radius: 0' src='" + D.protocol + "res.hduofen.cn/images/lookqr.png'>"
            } else if (m == 5) {
                p = " <img class='zaax-wxh-suffix' style='vertical-align: middle!important;display: inline;padding: 0;margin: 0;width:100px!important;height:auto!important;float: none!important;border-radius: 0' src='" + D.protocol + "res.hduofen.cn/images/icon_clickcopy.png'>"
            } else if (m == 6) {
                p = " <img class='zaax-wxh-suffix' style='vertical-align: middle!important;display: inline;padding: 0;margin: 0;width:100px!important;height:auto!important;float: none!important;border-radius: 0' src='" + D.protocol + "res.hduofen.cn/images/lookqr.png'>"
            }
        }
        forwx();
        $zq(document).ready(function () {
            D.zaaxwx = $zq(D.wxhClassName);
            forwx();
            if (!isEmpty(D.wxh_name)) {
                $zq(".zaax-wxname").text(D.wxh_name)
            }
            if (!isEmpty(D.wxh_sex)) {
                $zq(".zaax-wxsex").text(D.wxh_sex)
            }
            if (!isEmpty(D.city)) {
                $zq(".zaax-city").text(D.city)
            }
            if (D.zaaxwx.length == 0) {
                $zq("body").append("<div style='display: none!important;'><span class='zaax-wxh'>" + D.last_wx + "</span></div>")
            }
        });
        var q = D.zaaxwx.length;
        var r = D.zaaxsex.length;
        var s = D.zaaxname.length;
        var t = D.zaaxcity.length;
        setInterval(function () {
            var a = $zq(D.wxhClassName);
            if (a.length > q) {
                console.log("page updata wxh:" + a.length);
                q = a.length;
                forwx()
            }
            if (!isEmpty(D.wxh_name)) {
                var b = $zq(".zaax-wxname");
                if (b.length > s) {
                    s = b.length;
                    $zq(".zaax-wxname").text(D.wxh_name)
                }
            }
            if (!isEmpty(D.wxh_sex)) {
                var c = $zq(".zaax-wxsex");
                if (c.length > r) {
                    r = c.length;
                    $zq(".zaax-wxsex").text(D.wxh_sex)
                }
            }
            if (!isEmpty(D.city)) {
                var d = $zq(".zaax-city");
                if (d.length > t) {
                    t = d.length;
                    $zq(".zaax-city").text(D.city)
                }
            }
        }, 80);

        function forwx() {
            $zq(D.wxhClassName).html(o).css(k);
            if (!isEmpty(p)) {
                $zq(".zaax-wxh-suffix").remove();
                $zq(D.wxhClassName).after(p);
                if ((D.devices_type == 1 && l != 7 && l != 8 && l != 9) || (D.devices_type != 1 && m != 4 && m != 5 && m != 6)) {
                    $zq(".zaax-wxh-suffix").css(k)
                }
                if ((D.devices_type != 1 && m == 6) || (D.devices_type == 1 && l == 9)) {
                    $zq(D.wxhClassName).hide()
                }
            }
            if (D.devices_type == 1 && g != 0 || D.devices_type != 1 && h != 0) {
                $zq(D.wxhClassName).css("cursor", "pointer");
                $zq(".zaax-wxh-suffix").css("cursor", "pointer");
                $zq(document).on("click", ".zaax-click", function () {
                    $zq(".zaax-wxh").click()
                })
            }
            if (D.devices_type == 1 && i != 0) {
                $zq(".hdf_copy_box").remove();
                $zq(D.wxhClassName).append(copyHtml());
                $zq(D.wxhClassName).attr("ontouchstart", "return false");
                $zq(D.wxhClassName).css("position", "relative")
            }
            if (D.devices_type != 1 && j != 0) {
                $zq(D.wxhClassName).attr("oncopy", "window.hdfPcCopy(this)")
            }
        }

        if (g != 0 || h != 0 || i != 0 || j != 0) {
            loadScript(D.alertUrl, function () {
                hdfAlerts(D, 'wxm', {
                    openWx: g,
                    pc_openWx: h,
                    long_copy: i,
                    pc_long_copy: j,
                    wx_style: k,
                    alert_data: n
                })
            })
        }
    }

    function setWxName() {
        if (D.is_cache == 1) {
            try {
                if (!isEmpty(D.hduofenCache.name) || isEmpty(D.hduofenCache.name) && !D.fast_cache) {
                    D.wxh_name = D.hduofenCache.name
                } else if (D.fast_cache) {
                    D.hduofenCache.name = D.wxh_name;
                    setCookie("hduofenCache_" + D.path, JSON.stringify(D.hduofenCache))
                }
            } catch (e) {
                console.log(e)
            }
        }
        window._hdf.wxname = D.wxh_name
    }

    function setWxSex() {
        if (D.is_cache == 1) {
            try {
                if (!isEmpty(D.hduofenCache.sex) || isEmpty(D.hduofenCache.sex) && !D.fast_cache) {
                    D.wxh_sex = D.hduofenCache.sex
                } else if (D.fast_cache) {
                    D.hduofenCache.sex = D.wxh_sex;
                    setCookie("hduofenCache_" + D.path, JSON.stringify(D.hduofenCache))
                }
            } catch (e) {
                console.log(e)
            }
        }
        window._hdf.wxsex = D.wxh_sex
    }

    function setCity() {
        if (D.is_cache == 1) {
            try {
                if (!isEmpty(D.hduofenCache.city) || isEmpty(D.hduofenCache.city) && !D.fast_cache) {
                    D.city = D.hduofenCache.city
                } else if (D.fast_cache) {
                    D.hduofenCache.city = D.city;
                    setCookie("hduofenCache_" + D.path, JSON.stringify(D.hduofenCache))
                }
            } catch (e) {
                console.log(e)
            }
        }
        window._hdf.city = D.city
    }

    function tipsalert(a, b) {
        if (D.tipsalertShow) {
            var c = "<div id='tipsalert' style='box-sizing: content-box;z-index:2147483647;padding: 0 5%!important;margin: 0!important;width: 90%!important;position: fixed;top: 20%;left: 0;text-align: center'>" + "<div style='height: auto;margin: 0!important;padding: 12px 25px!important;border: none;background:rgba(0,0,0,.6)!important;min-width: 100px;max-width: 90%;display:inline-block;border-radius: 5px;line-height: 24px!important;font-size:15px!important;text-align: center;color: white;'>" + a + "</div></div>";
            $zq("body").append(c);
            D.tipsalertShow = false;
            setTimeout(function () {
                $zq("#tipsalert").remove();
                D.tipsalertShow = true;
                if (b != null) {
                    b()
                }
            }, 2000)
        }
    }

    function copyListen(a) {
        var b = true;
        if (window.getSelection) {
            if (window.getSelection().toString().length > 0 && window.getSelection().toString() == a) {
                D.copy_wxh = a;
                if (b) {
                    submitCopyCount();
                    b = false
                }
            }
        } else if (window.document.getSelection) {
            if (window.getSelection().toString().length > 0 && window.document.getSelection().toString() == a) {
                D.copy_wxh = a;
                if (b) {
                    submitCopyCount();
                    b = false
                }
            }
        } else if (window.document.selection) {
            if (window.getSelection().toString().length > 0 && window.document.selection.createRange().text == a) {
                D.copy_wxh = a;
                if (b) {
                    submitCopyCount();
                    b = false
                }
            }
        }
        if (b) {
            setTimeout(function () {
                copyListen(a)
            }, 50)
        }
    }

    function subOpenWxData() {
        submitOpenCount()
    }

    function submitOpenCount() {
        if (D.isOpen) {
            D.isOpen = false;
            $zq.ajax({
                url: D.baseurl + "webstatic/upOpenWxBrowseRecord",
                data: {sessions: D.sessions, copy_wxh: D.copy_wxh, user_uuid: D.user_uuid, url: D.simpleUrl},
                success: function (a) {
                    if (!a.success) {
                        D.isOpen = true
                    }
                }
            })
        }
    }

    function submitCallPhone() {
        if (D.isCall) {
            $zq.ajax({
                url: D.baseurl + "webstatic/upCallPhoenRecord",
                data: {sessions: D.sessions, url: D.simpleUrl, user_uuid: D.user_uuid},
                success: function (a) {
                    if (a.success) {
                        D.isCall = false
                    }
                }
            })
        }
    }

    function subCopyData(a) {
        submitCopyCount(a)
    }

    function submitCopyCount(b) {
        if (D.hdf_is_open_copy || D.last_copy_wxh.indexOf(D.copy_wxh) == -1) {
            D.hdf_is_open_copy = false;
            D.last_copy_wxh = D.copy_wxh;
            $zq.ajax({
                url: D.baseurl + "webstatic/upCopyBrowseRecord",
                data: {
                    sessions: D.sessions,
                    copy_wxh: isEmpty(b) ? D.copy_wxh : b,
                    url: D.simpleUrl,
                    complete_url: D.url.replace(/&/g, '||'),
                    user_uuid: D.user_uuid,
                    modify: D.modify,
                    current_wxh: D.last_wx,
                    devices_type: D.devices_type,
                    device: D.devices,
                    serach_text: isEmpty(D.serach_text) ? "" : D.serach_text,
                    seo: D.seo,
                    source_url: D.source_url,
                    return_stutas: D.return_stutas,
                    return_url: D.return_url,
                    return_msg: D.return_msg,
                    wxh_name: D.wxh_name,
                    browserName: D.browserName,
                    browserVersion: D.browserVersion,
                    wxh_methods: isEmpty(D.muserWxhManager) ? "" : isEmpty(D.muserWxhManager.wxh_methods) ? "" : D.muserWxhManager.wxh_methods,
                    groups: isEmpty(D.muserWxhManager) ? "" : isEmpty(D.muserWxhManager.groups) ? "" : D.muserWxhManager.groups,
                    min_copytime: D.mcopyStatiConf ? D.mcopyStatiConf.min_copytime : 0
                },
                success: function (a) {
                    try {
                        if (a.success) {
                            if (!isEmpty(hdfCallback)) {
                                hdfCallback(D.copy_wxh)
                            }
                        }
                    } catch (e) {
                    }
                }
            })
        }
    }

    function browse() {
        if (D.hdf_is_browse) {
            D.hdf_is_browse = false;
            $zq.ajax({
                url: D.baseurl + "webstatic/addbrowseRecord",
                data: {
                    sessions: D.sessions,
                    user_uuid: D.user_uuid,
                    url: D.simpleUrl,
                    complete_url: D.url.replace(/&/g, '||'),
                    modify: D.modify,
                    current_wxh: D.last_wx,
                    devices_type: D.devices_type,
                    device: D.devices,
                    serach_text: isEmpty(D.serach_text) ? "" : D.serach_text,
                    seo: D.seo,
                    source_url: D.source_url,
                    return_stutas: D.return_stutas,
                    return_url: D.return_url,
                    return_msg: D.return_msg,
                    zaaxstat_version: D.zaaxstat_version,
                    browserName: D.browserName,
                    browserVersion: D.browserVersion,
                    userAgent: isEmpty(D.browserName) ? D.userAgent : ""
                },
                success: function (a) {
                    if (a != null && !a.success) {
                        D.isSubmitBrowserTime = false
                    }
                },
                complete: function () {
                    browseTime()
                }
            })
        }
    }

    function browseTime() {
        var k = new Date().getTime();
        var l = getCookie("browseSecond_" + window.location.pathname);
        if (l != null) {
            k = l
        } else {
        }
        $zq(document).ready(function () {
            var d = $zq(document).height();
            var f = document.body.scrollHeight;
            d = d < f ? f : d;
            var g = $zq(window).height();
            if ((d - g) <= 100) {
                D.access_ratio = 100;
                calculateTime()
            }
            window.addEventListener("scroll", function (e) {
                var a = $zq(window).scrollTop();
                var b = (a / (d - g)).toFixed(2) * 100;
                var c = Math.ceil(b) > 100 ? 100 : Math.ceil(b);
                D.access_ratio = c > D.access_ratio ? c : D.access_ratio
            });
            $zq(document.body).on("click", function (e) {
                calculateTime()
            });
            document.addEventListener('touchstart', function () {
                calculateTime()
            }, false);
            var h;
            var i = function (e) {
                if (h == null) {
                    h = setTimeout(function () {
                        h = null;
                        calculateTime()
                    }, 1000)
                }
            };
            if (document.addEventListener) {
                document.addEventListener('DOMMouseScroll', i, false)
            }
            window.onmousewheel = document.onmousewheel = i;
            window.addEventListener('scroll', i);
            addOnbeforeunload(function () {
                D.browse_start = 5;
                calculateTime()
            });
            var j;
            if (typeof document.hidden !== "undefined") {
                j = "visibilitychange"
            } else if (typeof document.mozHidden !== "undefined") {
                j = "mozvisibilitychange"
            } else if (typeof document.msHidden !== "undefined") {
                j = "msvisibilitychange"
            } else if (typeof document.webkitHidden !== "undefined") {
                j = "webkitvisibilitychange"
            }
            document.addEventListener(j, function () {
                calculateTime()
            }, false)
        });

        function calculateTime() {
            var a = (new Date().getTime() - k) / 1000;
            if (a >= 8 || D.browse_start == 5) {
                submitTime()
            }
        }

        function submitTime() {
            if (D.isSubmitBrowserTime) {
                D.isSubmitBrowserTime = false;
                k = new Date().getTime();
                setCookie("browseSecond_" + window.location.pathname, k);
                $zq.ajax({
                    url: D.baseurl + "webstatic/statBrowseTime",
                    data: {
                        user_uuid: D.user_uuid,
                        url: D.simpleUrl,
                        sessions: D.sessions,
                        browse_start: D.browse_start,
                        access_ratio: D.access_ratio
                    },
                    success: function (a) {
                        D.isSubmitBrowserTime = true;
                        D.browse_start = 1;
                        console.log(a)
                    }
                })
            }
        }

        function addOnbeforeunload(a) {
            var b = window.onbeforeunload;
            if (typeof window.onbeforeunload != 'function') {
                window.onbeforeunload = a
            } else {
                window.onbeforeunload = function () {
                    b();
                    a()
                }
            }
        }
    }

    function initJsCode() {
        if (D.hdf_initJscode) {
            D.hdf_initJscode = false;
            $zq.ajax({
                url: D.baseurl + "webstatic/getJsCode",
                data: {url: D.simpleUrl, uuid: D.user_uuid},
                success: function (d) {
                    if (d != null && d.success) {
                        if (!isEmpty(d.data)) {
                            $zq(document).ready(function () {
                                loadScript(D.jqueryUrl, function () {
                                    var c = $.noConflict(true);
                                    $zq.each(d.data, function (a, b) {
                                        if (!isEmpty(b)) {
                                            c('body').append("<div style='display: none'>" + b.js_code + "</div>")
                                        }
                                    })
                                })
                            })
                        }
                    }
                }
            })
        }
    }

    function setHdfshare(a) {
        if (D.href.indexOf("?") == -1) {
            window.location.href = D.href + "?hdfshare=" + a
        } else {
            window.location.href = D.href + "&hdfshare=" + a
        }
    }

    function getWxProgramUid() {
        $zq.ajax({
            url: D.baseurl + "webstatic/getWxProgramUid",
            data: {user_uuid: D.user_uuid, url: D.simpleUrl, last_wx: D.last_wx},
            success: function (a) {
                if (a && a.success) {
                    D.wxProgramData = a.data
                }
            }
        })
    }

    D.getWxUrlScheme = function (b) {
        if (D.wxProgramData) {
            var c = getCookie("hdf_UrlScheme_" + D.wxProgramData.uid);
            if (c) {
                if (b) {
                    b(c)
                }
            } else {
                $zq.ajax({
                    url: D.baseurl + "webstatic/getUrlScheme",
                    data: {user_uuid: D.user_uuid, uid: D.wxProgramData.uid, type: D.wxProgramData.wxprogram_type},
                    success: function (a) {
                        if (a && a.success && a.data) {
                            setCookie("hdf_UrlScheme_" + D.wxProgramData.uid, a.data);
                            if (b) {
                                b(a.data)
                            }
                        }
                    }
                })
            }
        }
    };

    function getWxKfUrl() {
        $zq.ajax({
            url: D.baseurl + "webstatic/getWxKfUrl",
            data: {user_uuid: D.user_uuid, url: D.simpleUrl, last_wx: D.last_wx},
            success: function (a) {
                if (a && a.success) {
                    D.is_wxkf = 1;
                    D.wxkf_url = a.data
                } else {
                    D.is_wxkf = 0
                }
            }
        })
    }

    function initBrowseName() {
        try {
            var f = D.userAgent.toLowerCase();
            if (f.match(/msie/) != null || f.match(/trident/) != null) {
                D.browserName = 1;
                D.browserVersion = getIeVersion()
            } else if (window.opera || (f.indexOf("opr") > 0)) {
                D.browserName = 10;
                D.browserVersion = getOperaVersion(f)
            } else if (f.indexOf("bidubrowser") > 0) {
                D.browserName = 20;
                var g = f.match(/bidubrowser\/([\d.]+)/);
                D.browserVersion = g == null ? "" : g[1]
            } else if (f.indexOf("baiduboxapp") > 0) {
                if (f.indexOf("lite baiduboxapp") > 0) {
                    D.browserName = 22
                } else {
                    D.browserName = 21
                }
                var h = f.match(/baiduboxapp\/([\d.]+)/);
                D.browserVersion = h == null ? "" : h[1]
            } else if (f.indexOf("ubrowser") > 0) {
                D.browserName = 30;
                var i = f.match(/ubrowser\/([\d.]+)/);
                D.browserVersion = i == null ? "" : i[1]
            } else if (f.indexOf("ucbrowser") > 0) {
                D.browserName = 31;
                var j = f.match(/ucbrowser\/([\d.]+)/);
                D.browserVersion = j == null ? "" : j[1]
            } else if (f.indexOf("metasr") > 0 || f.indexOf("se 2.x") > 0) {
                D.browserName = 40;
                var k = f.match(/metasr\s([\d.]+)/);
                D.browserVersion = k == null ? "" : k[1]
            } else if (f.indexOf("sogoumobilebrowser") > 0) {
                D.browserName = 41;
                var l = f.match(/sogoumobilebrowser\/([\d.]+)/);
                D.browserVersion = l == null ? "" : l[1]
            } else if (f.indexOf("sogoumse") > 0) {
                D.browserName = 41;
                var m = f.match(/sogoumse\/([\d.]+)/);
                D.browserVersion = m == null ? "" : m[1]
            } else if (f.indexOf("qqbrowser") > 0 || f.indexOf("micromessenger") > 0) {
                var n = f.match(/micromessenger\/([\d.]+)/);
                var o = f.match(/qq\/([\d.]+)/);
                var p = f.match(/tencenttraveler\/([\d.]+)/);
                var q = f.match(/qqbrowser\/([\d.]+)/);
                if (n != null) {
                    D.browserName = 53;
                    D.browserVersion = n[1]
                } else if (o != null) {
                    D.browserName = 52;
                    D.browserVersion = o[1]
                } else if (p != null) {
                    D.browserName = 51;
                    D.browserVersion = p
                } else if (q != null) {
                    D.browserName = 51;
                    D.browserVersion = q[1]
                } else {
                    D.browserName = 50
                }
            } else if (f.indexOf("maxthon") > 0) {
                D.browserName = 60;
                var r = f.match(/maxthon\/([\d.]+)/);
                D.browserVersion = r == null ? "" : r[1]
            } else if (f.indexOf("firefox") > 0) {
                D.browserName = 70;
                var s = f.match(/firefox\/([\d.]+)/);
                D.browserVersion = s == null ? "" : s[1]
            } else if (f.indexOf("edge") > 0) {
                D.browserName = 80;
                var t = f.match(/edge\/([\d.]+)/);
                D.browserVersion = t == null ? "" : t[1]
            } else if (f.indexOf("qihoobrowser") > 0) {
                D.browserName = 85;
                var u = f.match(/qihoobrowser\/([\d.]+)/);
                D.browserVersion = u == null ? "" : u[1]
            } else if (f.indexOf("newsarticle") > 0) {
                if (f.indexOf("newslite") > 0) {
                    D.browserName = 111
                } else {
                    D.browserName = 110
                }
                var v = f.match(/newsarticle\/([\d.]+)/);
                D.browserVersion = v == null ? "" : v[1]
            } else if (f.indexOf("aweme") > 0) {
                if (f.indexOf("aweme_lite") > 0) {
                    D.browserName = 121
                } else {
                    D.browserName = 120
                }
                var v = f.match(/app_version\/([\d.]+)/);
                D.browserVersion = v == null ? "" : v[1]
            } else if (f.indexOf("ksnebula") > 0) {
                D.browserName = 131;
                var v = f.match(/ksnebula\/([\d.]+)/);
                D.browserVersion = v == null ? "" : v[1]
            } else if (f.indexOf("kwai") > 0) {
                D.browserName = 130;
                var v = f.match(/kwai\/([\d.]+)/);
                D.browserVersion = v == null ? "" : v[1]
            } else if (f.indexOf("live_stream") > 0) {
                D.browserName = 140;
                var v = f.match(/live_stream_([\d.]+)/);
                D.browserVersion = (v == null ? "" : v[1])
            } else if (f.indexOf("videoarticle") > 0) {
                D.browserName = 150;
                var v = f.match(/videoarticle\/([\d.]+)/);
                D.browserVersion = (v == null ? "" : v[1])
            } else if (f.indexOf("samsungbrowser") > 0) {
                D.browserName = 160;
                var w = f.match(/samsungbrowser\/([\d.]+)/);
                D.browserVersion = (w == null ? "" : w[1])
            } else if (f.indexOf("miuibrowser") > 0) {
                D.browserName = 170;
                var x = f.match(/miuibrowser\/([\d.]+)/);
                D.browserVersion = (x == null ? "" : x[1])
            } else if (f.indexOf("heytapbrowser") > 0) {
                D.browserName = 180;
                var y = f.match(/heytapbrowser\/([\d.]+)/);
                D.browserVersion = (y == null ? "" : y[1])
            } else if (f.indexOf("vivobrowser") > 0) {
                D.browserName = 190;
                var z = f.match(/vivobrowser\/([\d.]+)/);
                D.browserVersion = (z == null ? "" : z[1])
            } else if (f.indexOf("huaweibrowser") > 0) {
                D.browserName = 200;
                var A = f.match(/huaweibrowser\/([\d.]+)/);
                D.browserVersion = (A == null ? "" : A[1])
            } else if (f.indexOf("chrome") > 0) {
                D.browserName = 90;
                if (f.indexOf("mobile")) {
                    D.browserName = 91
                }
                var B = f.match(/chrome\/([\d.]+)/);
                D.browserVersion = B == null ? "" : B[1]
            } else if (f.indexOf("safari") > -1) {
                D.browserName = 100;
                var C = f.match(/version\/([\d.]+)/);
                D.browserVersion = C == null ? "" : C[1]
            }
            D.browserVersion = D.browserVersion.length > 25 ? D.browserVersion.substring(0, 25) : D.browserVersion
        } catch (e) {
            console.log(e)
        }

        function getIeVersion() {
            var a = document.documentMode;
            var b = /(msie\s|trident.*rv:)([\w.]+)/;
            var c = window.navigator.userAgent.toLowerCase();
            var d = b.exec(c);
            try {
                return d[2]
            } catch (e) {
                console.log(e);
                return a
            }
        }

        function getOperaVersion(a) {
            try {
                if (window.opera) {
                    return a.match(/opera.([\d.]+)/)[1]
                } else if (a.indexOf("opr") > 0) {
                    return a.match(/opr\/([\d.]+)/)[1]
                }
            } catch (e) {
                console.log(e);
                return 0
            }
        }
    }

    function partReferrer() {
        try {
            if (isEmpty(D.referrer) || (!isEmpty(getQueryString(D.url, "hdfshare")) && D.referrer.indexOf(D.simpleUrl) != -1)) {
                D.referrer = isEmpty(getCookie("zaaxRef")) ? "" : getCookie("zaaxRef")
            } else {
                setCookie("zaaxRef", encodeURI(D.referrer))
            }
            var a = D.referrer.split("?")[0];
            if (a.indexOf("m.baidu.com") != -1) {
                D.seo = 2;
                D.serach_text = getQueryString(D.referrer, "word")
            } else if (a.indexOf("baidu.com") != -1) {
                D.seo = 2;
                D.serach_text = getQueryString(D.referrer, "wd");
                if (isEmpty(D.serach_text)) {
                    D.serach_text = getQueryString(D.referrer, "word")
                }
            } else if (a.indexOf("so.com") != -1) {
                D.seo = 3;
                D.serach_text = getQueryString(D.referrer, "q")
            } else if (a.indexOf("m.sogou") != -1 || a.indexOf("wap.sogou") != -1 || a.indexOf("zhishi.sogou") != -1 || a.indexOf("3g.sogou") != -1 || a.indexOf("sogou.com") != -1) {
                D.seo = 4;
                D.serach_text = getQueryString(D.referrer, "keyword")
            } else if (a.indexOf("www.sogou") != -1) {
                D.seo = 4;
                D.serach_text = getQueryString(D.referrer, "query")
            } else if (a.indexOf(".sm.cn") != -1 || a.indexOf("//sm.cn") != -1) {
                D.seo = 5;
                D.serach_text = getQueryString(D.referrer, "q")
            } else if (a.indexOf("zaax") != -1) {
                D.seo = 6;
                if (a.indexOf("baidu.zaax.top") != -1) {
                    D.serach_text = getQueryString(D.referrer, "word")
                } else if (a.indexOf("so.zaax.top") != -1) {
                    D.serach_text = getQueryString(D.referrer, "q")
                } else if (a.indexOf("sogou.zaax.top") != -1) {
                    D.serach_text = getQueryString(D.referrer, "keyword")
                } else if (a.indexOf("sm.zaax.top") != -1) {
                    D.serach_text = getQueryString(D.referrer, "q")
                }
                D.referrer = ""
            } else if (a.indexOf("google.com") != -1) {
                D.seo = 7;
                D.serach_text = getQueryString(D.referrer, "q")
            } else if (a.indexOf("ifeng.com") != -1) {
                D.seo = 8;
                D.serach_text = getQueryString(D.referrer, "q")
            } else if (a.indexOf("toutiao.eastday.com") != -1) {
                D.seo = 9;
                D.serach_text = getQueryString(D.referrer, "kw")
            } else if (a.indexOf("toutiao.com") != -1) {
                D.seo = 9;
                D.serach_text = getQueryString(D.referrer, "keyword")
            }
            if (!isEmpty(getQueryString(D.url, "seo"))) {
                D.seo = getQueryString(D.url, "seo").split("#")[0]
            }
            D.source_url = D.referrer.length > 1000 ? D.referrer.substring(0, 1000) : D.referrer;
            if (D.serach_text == 1) {
                D.serach_text = ''
            }
        } catch (e) {
            console.log(e)
        }
    }

    function getAccessRatio() {
        return D.access_ratio
    }

    function getDevices() {
        var e = navigator.userAgent;
        for (var i = [["Windows NT 5.1", "winXP", 2], ["Windows NT 6.1", "win7", 2], ["Windows NT 6.0", "winVista", 2], ["Windows NT 6.2", "win8", 2], ["Windows NT 10.0", "win10", 2], ["iPad", "ios", 1], ["iPhone;", "ios", 1], ["iPod", "ios", 1], ["Macintosh", "mac", 2], ["Android", "Android", 1], ["Ubuntu", "ubuntu", 2], ["Linux", "linux", 1], ["Windows NT 5.2", "win2003", 2], ["Windows NT 5.0", "win2000", 2], ["Windows", "winOther", 2], ["rhino", "rhino", 3]], o = 0, n = i.length; o < n; ++o) if (e.indexOf(i[o][0]) !== -1) return i[o];
        return ["other", "other", 3]
    }

    function getUuid() {
        var a = document.getElementsByTagName("script");
        for (var i = 0; i < a.length; i++) {
            if (a[i].src.indexOf("zaaxstat.js") >= 0 || a[i].src.indexOf("zaaxstat_test.js") >= 0) {
                return a[i].src.split('?')[1].split('=')[1]
            }
        }
    }

    function getQueryString(a, b) {
        try {
            var c = new RegExp("(^|&)" + b + "=([^&]*)(&|$)", "i");
            var d = a.split("?")[1];
            if (!isEmpty(d)) {
                var f = d.match(c);
                if (!isEmpty(f) && f.length > 1) {
                    return decodeURIComponent(f[2])
                }
            }
            return ""
        } catch (e) {
            console.log(e)
        }
        try {
            var g = window.location.search.substring(1);
            var h = g.split("&");
            for (var i = 0; i < h.length; i++) {
                var j = h[i].split("=");
                if (j[0] == b) {
                    return decodeURIComponent(j[1])
                }
            }
            return ""
        } catch (e) {
            console.log(e)
        }
    }

    function UrlSearch(a) {
        var b = a.indexOf("?");
        var c = a.substr(b + 1);
        var d = {};
        var e = c.split("&");
        for (var i = 0; i < e.length; i++) {
            b = e[i].indexOf("=");
            if (b > 0) {
                d[e[i].substring(0, b)] = e[i].substr(b + 1)
            }
        }
        return d
    }

    function delParame(a) {
        var b = window.location.search;
        if (b.indexOf(a) != -1) {
            b = b.substring(1);
            var c = b.split('&');
            var d = [];
            for (var i = 0; i < c.length; i++) {
                var e = c[i].split('=');
                if (a != e[0]) {
                    d.push(c[i])
                }
            }
            if (d.length == 0) {
                return window.location.pathname
            } else {
                return window.location.pathname + '?' + d.join('&')
            }
        } else {
            return window.location.href
        }
    }

    function guid() {
        return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16).toUpperCase()
        })
    }

    function setCookie(a, b, c) {
        var d = new Date();
        c = isEmpty(c) ? (30 * 60 * 1000) : c;
        window.localStorage.setItem(a, JSON.stringify({data: b, expires: (d.getTime() + c)}))
    }

    function getCookie(a) {
        var b = window.localStorage.getItem(a);
        var d = new Date();
        if (!isEmpty(b)) {
            var c = JSON.parse(b);
            if (d.getTime() <= c.expires) {
                return c.data
            } else {
                window.localStorage.removeItem(a)
            }
        }
        return ""
    }

    function clearCookie(a) {
        window.localStorage.removeItem(a)
    }

    function isEmpty(a) {
        if (typeof a == "undefined" || a == null || a == "null" || a === "") {
            return true
        } else {
            return false
        }
    }

    function isPoneAvailable(a) {
        var b = /^[1][3,4,5,6,7,8,9][0-9]{9}$/;
        var c = /^400[0-9]/;
        var d = /^0\d{2}\d{7,8}/;
        if (!b.test(a) && !c.test(a) && !d.test(a)) {
            return false
        } else {
            return true
        }
    }

    function getProtocol() {
        try {
            if (document.location.protocol.indexOf("https") != -1) {
                return "https://"
            }
        } catch (e) {
            console.log(e)
        }
        return "http://"
    }

    function zformUrl(a) {
        var b = a.split("?")[0].split("#")[0];
        if (a.indexOf("html") != -1 || a.indexOf("htm") != -1 || a.indexOf("php") != -1 || a.indexOf("jsp") != -1 || a.indexOf("asp") != -1) {
            return b
        }
        if (b.substr(b.length - 1, 1) == "/") {
            return b
        } else {
            return b + "/"
        }
    }

    function initClipboard(a, b, c) {
        var d = new ClipboardJS(a, {
            text: function () {
                return b
            }
        });
        d.on('success', function (e) {
            if (c != null) {
                c(e.text)
            }
        });
        d.on('error', function (e) {
            if (!isEmpty(e.text) && e.text == b) {
                if (c != null) {
                    c(e.text)
                }
            }
            console.log("clipboard error:" + e.text)
        })
    }

    function loadScript(b, c) {
        $zq(document).ready(function () {
            var a = document.createElement('script');
            a.type = 'text/javascript';
            a.async = true;
            a.src = b;
            document.body.appendChild(a);
            if (a.readyState) {
                a.onreadystatechange = function () {
                    if (a.readyState == 'complete' || a.readyState == 'loaded') {
                        a.onreadystatechange = null;
                        c()
                    }
                }
            } else {
                a.onload = function () {
                    c()
                }
            }
        })
    }

    function longTouch(b, c, d, f) {
        var g;
        var h = 0;
        var i = 0;
        $zq(document).on({
            touchstart: function (e) {
                g = setTimeout(function () {
                    g = 0;
                    if (c != null) {
                        c(e)
                    }
                }, (f == null ? 600 : f));
                var a = e.originalEvent.targetTouches[0];
                if (a) {
                    h = a.pageX;
                    i = a.pageY
                }
            }, touchmove: function (e) {
                var a = e.originalEvent.targetTouches[0];
                if (a) {
                    if (Math.abs((h - a.pageX)) > 5 || Math.abs((h - a.pageX)) > 5) {
                        clearTimeout(g);
                        g = 0
                    }
                }
            }, touchend: function (e) {
                clearTimeout(g);
                if (g != 0) {
                    if (d != null) {
                        d(e)
                    }
                }
            }
        }, b)
    }

    function copyHtml() {
        return "<div class='hdf_copy_box' style='z-index: 5!important;width: 120%!important;background: rgba(56,56,56,0)!important;position: absolute!important;text-align: center!important;margin:0!important;padding: 0!important;" + "box-sizing: content-box!important;top: -50%!important;left: -10%!important;font-weight: 400!important;line-height: 200%!important;height: 200%!important;font-size: 0.8em!important;border-radius: 4px!important;color: white!important'>" + "<button class='hdf_copy_btn' style='display: none;padding: 0!important;z-index: 2147483646!important;outline: none!important;border: none!important;width: 60px!important;position: relative!important;text-align: center!important;" + "box-sizing: content-box!important;margin: -30px auto 0;font-weight: 400!important;line-height: 32px!important;height: 32px!important;font-size: 16px!important;border-radius: 4px!important;color: white!important;background: #777!important;'>" + "<div style='position: absolute!important;background: none!important;left: 50%!important;top: 31px!important;;width: 0!important;height: 0!important;margin:0 0 0 -10px!important;padding: 0!important;border: 10px solid transparent!important;border-radius: 0!important;border-top-color: #777!important;'></div>\u590D\u5236</button></div>"
    }
})();